// To parse this JSON data, do
//
//     final kitBagResponse = kitBagResponseFromJson(jsonString);

import 'dart:convert';

List<KitBagResponse> kitBagResponseFromJson(String str) => List<KitBagResponse>.from(json.decode(str).map((x) => KitBagResponse.fromJson(x)));

String kitBagResponseToJson(List<KitBagResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class KitBagResponse {
  KitBagResponse({
    this.url,
    this.fileName,
    this.extension,
  });

  String url;
  String fileName;
  String extension;

  factory KitBagResponse.fromJson(Map<String, dynamic> json) => KitBagResponse(
    url: json["URL"],
    fileName: json["FileName"],
    extension: json["Extension"],
  );

  Map<String, dynamic> toJson() => {
    "URL": url,
    "FileName": fileName,
    "Extension": extension,
  };
}
