// To parse this JSON data, do
//
//     final userResponse = userResponseFromJson(jsonString);

import 'dart:convert';

UserResponse userResponseFromJson(String str) => UserResponse.fromJson(json.decode(str));

String userResponseToJson(UserResponse data) => json.encode(data.toJson());

class UserResponse {
    UserResponse({
        this.authToken,
        this.userId,
        this.forename,
        this.surname,
        this.email,
        this.username,
        this.errorCode,
        this.errorMessage,
        this.roleId,
        this.roleName,
        this.businessUnitId,
        this.costCentreId,
        this.regionId,
        this.userTypeId,
        this.jobTitleId,
        this.location,
        this.employeeRef,
        this.token,
        this.tokenExpires,
        this.contractorId,
    });

    String authToken;
    int userId;
    String forename;
    String surname;
    dynamic email;
    String username;
    dynamic errorCode;
    dynamic errorMessage;
    int roleId;
    String roleName;
    dynamic businessUnitId;
    dynamic costCentreId;
    dynamic regionId;
    dynamic userTypeId;
    dynamic jobTitleId;
    dynamic location;
    dynamic employeeRef;
    String token;
    DateTime tokenExpires;
    int contractorId;

    factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
        authToken: json["AuthToken"],
        userId: json["UserID"],
        forename: json["Forename"],
        surname: json["Surname"],
        email: json["Email"],
        username: json["Username"],
        errorCode: json["ErrorCode"],
        errorMessage: json["ErrorMessage"],
        roleId: json["RoleID"],
        roleName: json["RoleName"],
        businessUnitId: json["BusinessUnitID"],
        costCentreId: json["CostCentreID"],
        regionId: json["RegionID"],
        userTypeId: json["UserTypeID"],
        jobTitleId: json["JobTitleID"],
        location: json["Location"],
        employeeRef: json["EmployeeRef"],
        token: json["Token"],
        tokenExpires: DateTime.parse(json["TokenExpires"]),
        contractorId: json["ContractorID"],
    );

    Map<String, dynamic> toJson() => {
        "AuthToken": authToken,
        "UserID": userId,
        "Forename": forename,
        "Surname": surname,
        "Email": email,
        "Username": username,
        "ErrorCode": errorCode,
        "ErrorMessage": errorMessage,
        "RoleID": roleId,
        "RoleName": roleName,
        "BusinessUnitID": businessUnitId,
        "CostCentreID": costCentreId,
        "RegionID": regionId,
        "UserTypeID": userTypeId,
        "JobTitleID": jobTitleId,
        "Location": location,
        "EmployeeRef": employeeRef,
        "Token": token,
        "TokenExpires": tokenExpires.toIso8601String(),
        "ContractorID": contractorId,
    };
}
