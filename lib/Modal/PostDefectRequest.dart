// To parse this JSON data, do
//
//     final postDefectRequest = postDefectRequestFromJson(jsonString);

import 'dart:convert';

PostDefectRequest postDefectRequestFromJson(String str) => PostDefectRequest.fromJson(json.decode(str));

String postDefectRequestToJson(PostDefectRequest data) => json.encode(data.toJson());

class PostDefectRequest {
  PostDefectRequest({
    this.defectId,
    this.jobId,
    this.defectTypeId,
    this.cableId,
    this.workPoint,
    this.urgent,
    this.buildImpacting,
    this.location,
    this.latitude,
    this.longitude,
    this.section,
    this.whatIsDamaged,
    this.howItGotDamaged,
    this.comments,
    this.loggedDateTime,
    this.defectPhotos,
  });

  String defectId;
  String jobId;
  int defectTypeId;
  String cableId;
  String workPoint;
  bool urgent;
  bool buildImpacting;
  String location;
  double latitude;
  double longitude;
  String section;
  String whatIsDamaged;
  String howItGotDamaged;
  String comments;
  DateTime loggedDateTime;
  List<DefectPhoto> defectPhotos;

  factory PostDefectRequest.fromJson(Map<String, dynamic> json) => PostDefectRequest(
    defectId: json["defectId"],
    jobId: json["jobId"],
    defectTypeId: json["defectTypeId"],
    cableId: json["cableId"],
    workPoint: json["workPoint"],
    urgent: json["urgent"],
    buildImpacting: json["buildImpacting"],
    location: json["location"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    section: json["section"],
    whatIsDamaged: json["whatIsDamaged"],
    howItGotDamaged: json["howItGotDamaged"],
    comments: json["comments"],
    loggedDateTime: DateTime.parse(json["loggedDateTime"]),
    defectPhotos: List<DefectPhoto>.from(json["defectPhotos"].map((x) => DefectPhoto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "defectId": defectId,
    "jobId": jobId,
    "defectTypeId": defectTypeId,
    "cableId": cableId,
    "workPoint": workPoint,
    "urgent": urgent,
    "buildImpacting": buildImpacting,
    "location": location,
    "latitude": latitude,
    "longitude": longitude,
    "section": section,
    "whatIsDamaged": whatIsDamaged,
    "howItGotDamaged": howItGotDamaged,
    "comments": comments,
    "loggedDateTime": loggedDateTime.toIso8601String(),
    "defectPhotos": List<dynamic>.from(defectPhotos.map((x) => x.toJson())),
  };
}

class DefectPhoto {
  DefectPhoto({
    this.fileBytes,
  });

  String fileBytes;

  factory DefectPhoto.fromJson(Map<String, dynamic> json) => DefectPhoto(
    fileBytes: json["fileBytes"],
  );

  Map<String, dynamic> toJson() => {
    "fileBytes": fileBytes,
  };
}
