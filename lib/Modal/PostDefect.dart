// To parse this JSON data, do
//
//     final postDefect = postDefectFromJson(jsonString);

import 'dart:convert';

PostDefect postDefectFromJson(String str) => PostDefect.fromJson(json.decode(str));

String postDefectToJson(PostDefect data) => json.encode(data.toJson());

class PostDefect {
  PostDefect({
    this.authToken,
    this.submissionId,
    this.jobId,
    this.jobWorkPointId,
    this.defectTypeId,
    this.buildImpacting,
    this.urgent,
    this.location,
    this.lat,
    this.lng,
    this.whatDamaged,
    this.howDamaged,
    this.addComments,
    this.defectRef,
  });

  String authToken;
  String submissionId;
  int jobId;
  int jobWorkPointId;
  int defectTypeId;
  bool buildImpacting;
  bool urgent;
  String location;
  double lat;
  double lng;
  String whatDamaged;
  String howDamaged;
  String addComments;
  String defectRef;

  factory PostDefect.fromJson(Map<String, dynamic> json) => PostDefect(
    authToken: json["AuthToken"],
    submissionId: json["SubmissionID"],
    jobId: json["JobID"],
    jobWorkPointId: json["JobWorkPointID"],
    defectTypeId: json["DefectTypeID"],
    buildImpacting: json["BuildImpacting"],
    urgent: json["Urgent"],
    location: json["Location"],
    lat: json["Lat"],
    lng: json["Lng"],
    whatDamaged: json["WhatDamaged"],
    howDamaged: json["HowDamaged"],
    addComments: json["AddComments"],
    defectRef: json["DefectRef"],
  );

  Map<String, dynamic> toJson() => {
    "AuthToken": authToken,
    "SubmissionID": submissionId,
    "JobID": jobId,
    "JobWorkPointID": jobWorkPointId,
    "DefectTypeID": defectTypeId,
    "BuildImpacting": buildImpacting,
    "Urgent": urgent,
    "Location": location,
    "Lat": lat,
    "Lng": lng,
    "WhatDamaged": whatDamaged,
    "HowDamaged": howDamaged,
    "AddComments": addComments,
    "DefectRef": defectRef,
  };
}
