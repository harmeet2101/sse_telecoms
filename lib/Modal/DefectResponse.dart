// To parse this JSON data, do
//
//     final defectResponse = defectResponseFromJson(jsonString);

import 'dart:convert';

DefectResponse defectResponseFromJson(String str) => DefectResponse.fromJson(json.decode(str));

String defectResponseToJson(DefectResponse data) => json.encode(data.toJson());

class DefectResponse {
  DefectResponse({
    this.defectId,
  });

  int defectId;

  factory DefectResponse.fromJson(Map<String, dynamic> json) => DefectResponse(
    defectId: json["DefectID"],
  );

  Map<String, dynamic> toJson() => {
    "DefectID": defectId,
  };
}
