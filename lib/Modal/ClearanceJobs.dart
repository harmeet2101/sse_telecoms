// To parse this JSON data, do
//
//     final clearanceJobs = clearanceJobsFromJson(jsonString);

import 'dart:convert';

ClearanceJobs clearanceJobsFromJson(String str) => ClearanceJobs.fromJson(json.decode(str));

String clearanceJobsToJson(ClearanceJobs data) => json.encode(data.toJson());

class ClearanceJobs {
  ClearanceJobs({
    this.estimateNumber,
    this.exchange,
    this.section,
    this.noBlocs,
    this.plannedDate,
    this.blockages,
    this.jointboxes,
  });

  String estimateNumber;
  String exchange;
  String section;
  int noBlocs;
  DateTime plannedDate;
  List<Blockage> blockages;
  List<Blockage> jointboxes;

  factory ClearanceJobs.fromJson(Map<String, dynamic> json) => ClearanceJobs(
    estimateNumber: json["estimateNumber"],
    exchange: json["exchange"],
    section: json["section"],
    noBlocs: json["noBlocs"],
    plannedDate: DateTime.parse(json["plannedDate"]),
    blockages: List<Blockage>.from(json["blockages"].map((x) => Blockage.fromJson(x))),
    jointboxes: List<Blockage>.from(json["jointboxes"].map((x) => Blockage.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "estimateNumber": estimateNumber,
    "exchange": exchange,
    "section": section,
    "noBlocs": noBlocs,
    "plannedDate": plannedDate.toIso8601String(),
    "blockages": List<dynamic>.from(blockages.map((x) => x.toJson())),
    "jointboxes": List<dynamic>.from(jointboxes.map((x) => x.toJson())),
  };

  @override
  String toString() {
    return 'ClearanceJobs{estimateNumber: $estimateNumber, exchange: $exchange, section: $section, noBlocs: $noBlocs, plannedDate: $plannedDate, blockages: $blockages, jointboxes: $jointboxes}';
  }
}

class Blockage {
  Blockage({
    this.blockageId,
    this.latitude,
    this.longitude,
    this.address,
    this.jointboxId,
  });

  String blockageId;
  double latitude;
  double longitude;
  String address;
  String jointboxId;

  factory Blockage.fromJson(Map<String, dynamic> json) => Blockage(
    blockageId: json["blockageId"] == null ? null : json["blockageId"],
    latitude: json["latitude"].toDouble(),
    longitude: json["longitude"].toDouble(),
    address: json["address"],
    jointboxId: json["jointboxId"] == null ? null : json["jointboxId"],
  );

  Map<String, dynamic> toJson() => {
    "blockageId": blockageId == null ? null : blockageId,
    "latitude": latitude,
    "longitude": longitude,
    "address": address,
    "jointboxId": jointboxId == null ? null : jointboxId,
  };
}
