// To parse this JSON data, do
//
//     final surfaceType = surfaceTypeFromJson(jsonString);

import 'dart:convert';

List<SurfaceTypes> surfaceTypeFromJson(String str) => List<SurfaceTypes>.from(json.decode(str).map((x) => SurfaceTypes.fromJson(x)));

String surfaceTypeToJson(List<SurfaceTypes> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SurfaceTypes {
  SurfaceTypes({
    this.surfaceTypeId,
    this.surfaceTypeName,
  });

  int surfaceTypeId;
  String surfaceTypeName;

  factory SurfaceTypes.fromJson(Map<String, dynamic> json) => SurfaceTypes(
    surfaceTypeId: json["SurfaceTypeID"],
    surfaceTypeName: json["SurfaceTypeName"],
  );

  Map<String, dynamic> toJson() => {
    "SurfaceTypeID": surfaceTypeId,
    "SurfaceTypeName": surfaceTypeName,
  };
}
