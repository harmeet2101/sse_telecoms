// To parse this JSON data, do
//
//     final cableType = cableTypeFromJson(jsonString);

import 'dart:convert';

List<CableTypes> cableTypeFromJson(String str) => List<CableTypes>.from(json.decode(str).map((x) => CableTypes.fromJson(x)));

String cableTypeToJson(List<CableTypes> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CableTypes {
  CableTypes({
    this.cableTypeId,
    this.cableTypeName,
  });

  int cableTypeId;
  String cableTypeName;

  factory CableTypes.fromJson(Map<String, dynamic> json) => CableTypes(
    cableTypeId: json["CableTypeID"],
    cableTypeName: json["CableTypeName"],
  );

  Map<String, dynamic> toJson() => {
    "CableTypeID": cableTypeId,
    "CableTypeName": cableTypeName,
  };
}
