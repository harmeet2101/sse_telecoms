// To parse this JSON data, do
//
//     final cableInSitu = cableInSituFromJson(jsonString);

import 'dart:convert';

List<CableInSitu> cableInSituFromJson(String str) => List<CableInSitu>.from(json.decode(str).map((x) => CableInSitu.fromJson(x)));

String cableInSituToJson(List<CableInSitu> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CableInSitu {
  CableInSitu({
    this.cablesInSituId,
    this.cablesInSituName,
  });

  int cablesInSituId;
  String cablesInSituName;

  factory CableInSitu.fromJson(Map<String, dynamic> json) => CableInSitu(
    cablesInSituId: json["CablesInSituID"],
    cablesInSituName: json["CablesInSituName"],
  );

  Map<String, dynamic> toJson() => {
    "CablesInSituID": cablesInSituId,
    "CablesInSituName": cablesInSituName,
  };

  @override
  String toString() {
    return 'CableInSitu{cablesInSituId: $cablesInSituId, cablesInSituName: $cablesInSituName}';
  }
}
