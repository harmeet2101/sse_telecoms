// To parse this JSON data, do
//
//     final defectTypes = defectTypesFromJson(jsonString);

import 'dart:convert';

List<DefectTypes> defectTypesFromJson(String str) => List<DefectTypes>.from(json.decode(str).map((x) => DefectTypes.fromJson(x)));

String defectTypesToJson(List<DefectTypes> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DefectTypes {
  DefectTypes({
    this.defectTypeId,
    this.defectTypeName,
  });

  int defectTypeId;
  String defectTypeName;

  factory DefectTypes.fromJson(Map<String, dynamic> json) => DefectTypes(
    defectTypeId: json["DefectTypeID"],
    defectTypeName: json["DefectTypeName"],
  );

  Map<String, dynamic> toJson() => {
    "DefectTypeID": defectTypeId,
    "DefectTypeName": defectTypeName,
  };
}
