// To parse this JSON data, do
//
//     final trafficManagment = trafficManagmentFromJson(jsonString);

import 'dart:convert';

List<TrafficManagment> trafficManagmentFromJson(String str) => List<TrafficManagment>.from(json.decode(str).map((x) => TrafficManagment.fromJson(x)));

String trafficManagmentToJson(List<TrafficManagment> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TrafficManagment {
  TrafficManagment({
    this.trafficManagementId,
    this.trafficManagementName,
  });

  int trafficManagementId;
  String trafficManagementName;

  factory TrafficManagment.fromJson(Map<String, dynamic> json) => TrafficManagment(
    trafficManagementId: json["TrafficManagementID"],
    trafficManagementName: json["TrafficManagementName"],
  );

  Map<String, dynamic> toJson() => {
    "TrafficManagementID": trafficManagementId,
    "TrafficManagementName": trafficManagementName,
  };
}
