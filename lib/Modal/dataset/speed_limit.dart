// To parse this JSON data, do
//
//     final speedLimit = speedLimitFromJson(jsonString);

import 'dart:convert';

List<SpeedLimit> speedLimitFromJson(String str) => List<SpeedLimit>.from(json.decode(str).map((x) => SpeedLimit.fromJson(x)));

String speedLimitToJson(List<SpeedLimit> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SpeedLimit {
  SpeedLimit({
    this.speedLimitId,
    this.speedLimitName,
  });

  int speedLimitId;
  String speedLimitName;

  factory SpeedLimit.fromJson(Map<String, dynamic> json) => SpeedLimit(
    speedLimitId: json["SpeedLimitID"],
    speedLimitName: json["SpeedLimitName"],
  );

  Map<String, dynamic> toJson() => {
    "SpeedLimitID": speedLimitId,
    "SpeedLimitName": speedLimitName,
  };
}
