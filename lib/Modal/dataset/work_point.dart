// To parse this JSON data, do
//
//     final workPoint = workPointFromJson(jsonString);

import 'dart:convert';

List<WorkPoint> workPointFromJson(String str) => List<WorkPoint>.from(json.decode(str).map((x) => WorkPoint.fromJson(x)));

String workPointToJson(List<WorkPoint> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class WorkPoint {
  WorkPoint({
    this.jobWorkPointId,
    this.jobId,
    this.rowNum,
    this.dateCreated,
    this.createdByUserId,
    this.exchange1141Code,
    this.structureNumber,
    this.plantItem,
    this.objectClass,
    this.inventoryStatusCode,
    this.typeName,
    this.categoryName,
    this.ownershipTypeCode,
    this.objectid,
    this.orreferenceid,
    this.routeid,
    this.structureTypeName,
    this.isinvunsubmitted,
    this.isleadinonstructure,
    this.geometry,
    this.lat,
    this.lng,
  });

  int jobWorkPointId;
  int jobId;
  int rowNum;
  DateTime dateCreated;
  int createdByUserId;
  Exchange1141Code exchange1141Code;
  String structureNumber;
  String plantItem;
  ObjectClass objectClass;
  InventoryStatusCode inventoryStatusCode;
  String typeName;
  CategoryName categoryName;
  OwnershipTypeCode ownershipTypeCode;
  String objectid;
  Orreferenceid orreferenceid;
  String routeid;
  StructureTypeName structureTypeName;
  Isinvunsubmitted isinvunsubmitted;
  Isinvunsubmitted isleadinonstructure;
  String geometry;
  double lat;
  double lng;

  factory WorkPoint.fromJson(Map<String, dynamic> json) => WorkPoint(
    jobWorkPointId: json["JobWorkPointID"],
    jobId: json["JobID"],
    rowNum: json["RowNum"],
    dateCreated: DateTime.parse(json["DateCreated"]),
    createdByUserId: json["CreatedByUserID"],
    exchange1141Code: exchange1141CodeValues.map[json["EXCHANGE_1141_CODE"]],
    structureNumber: json["STRUCTURE_NUMBER"],
    plantItem: json["PLANT_ITEM"],
    objectClass: objectClassValues.map[json["OBJECT_CLASS"]],
    inventoryStatusCode: inventoryStatusCodeValues.map[json["INVENTORY_STATUS_CODE"]],
    typeName: json["TYPE_NAME"],
    categoryName: categoryNameValues.map[json["CATEGORY_NAME"]],
    ownershipTypeCode: ownershipTypeCodeValues.map[json["OWNERSHIP_TYPE_CODE"]],
    objectid: json["OBJECTID"],
    orreferenceid: orreferenceidValues.map[json["ORREFERENCEID"]],
    routeid: json["ROUTEID"],
    structureTypeName: structureTypeNameValues.map[json["STRUCTURE_TYPE_NAME"]],
    isinvunsubmitted: isinvunsubmittedValues.map[json["ISINVUNSUBMITTED"]],
    isleadinonstructure: isinvunsubmittedValues.map[json["ISLEADINONSTRUCTURE"]],
    geometry: json["GEOMETRY"],
    lat: json["Lat"].toDouble(),
    lng: json["Lng"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "JobWorkPointID": jobWorkPointId,
    "JobID": jobId,
    "RowNum": rowNum,
    "DateCreated": dateCreated.toIso8601String(),
    "CreatedByUserID": createdByUserId,
    "EXCHANGE_1141_CODE": exchange1141CodeValues.reverse[exchange1141Code],
    "STRUCTURE_NUMBER": structureNumber,
    "PLANT_ITEM": plantItem,
    "OBJECT_CLASS": objectClassValues.reverse[objectClass],
    "INVENTORY_STATUS_CODE": inventoryStatusCodeValues.reverse[inventoryStatusCode],
    "TYPE_NAME": typeName,
    "CATEGORY_NAME": categoryNameValues.reverse[categoryName],
    "OWNERSHIP_TYPE_CODE": ownershipTypeCodeValues.reverse[ownershipTypeCode],
    "OBJECTID": objectid,
    "ORREFERENCEID": orreferenceidValues.reverse[orreferenceid],
    "ROUTEID": routeid,
    "STRUCTURE_TYPE_NAME": structureTypeNameValues.reverse[structureTypeName],
    "ISINVUNSUBMITTED": isinvunsubmittedValues.reverse[isinvunsubmitted],
    "ISLEADINONSTRUCTURE": isinvunsubmittedValues.reverse[isleadinonstructure],
    "GEOMETRY": geometry,
    "Lat": lat,
    "Lng": lng,
  };
}

enum CategoryName { JOINTING_CHAMBER, BURIED }

final categoryNameValues = EnumValues({
  "BURIED": CategoryName.BURIED,
  "JOINTING CHAMBER": CategoryName.JOINTING_CHAMBER
});

enum Exchange1141Code { AB_DB }

final exchange1141CodeValues = EnumValues({
  "AB/DB": Exchange1141Code.AB_DB
});

enum InventoryStatusCode { IPL }

final inventoryStatusCodeValues = EnumValues({
  "IPL": InventoryStatusCode.IPL
});

enum Isinvunsubmitted { N }

final isinvunsubmittedValues = EnumValues({
  "N": Isinvunsubmitted.N
});

enum ObjectClass { JB, OTHER, MH }

final objectClassValues = EnumValues({
  "JB": ObjectClass.JB,
  "MH": ObjectClass.MH,
  "OTHER": ObjectClass.OTHER
});

enum Orreferenceid { PIANOI100001526020 }

final orreferenceidValues = EnumValues({
  "PIANOI100001526020": Orreferenceid.PIANOI100001526020
});

enum OwnershipTypeCode { RESERVED }

final ownershipTypeCodeValues = EnumValues({
  "RESERVED": OwnershipTypeCode.RESERVED
});

enum StructureTypeName { UG, NULL }

final structureTypeNameValues = EnumValues({
  "null": StructureTypeName.NULL,
  "UG": StructureTypeName.UG
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
