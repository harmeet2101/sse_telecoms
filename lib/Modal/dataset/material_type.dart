// To parse this JSON data, do
//
//     final materialType = materialTypeFromJson(jsonString);

import 'dart:convert';

List<MaterialTypes> materialTypeFromJson(String str) => List<MaterialTypes>.from(json.decode(str).map((x) => MaterialTypes.fromJson(x)));

String materialTypeToJson(List<MaterialTypes> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MaterialTypes {
  MaterialTypes({
    this.materialTypeId,
    this.materialTypeName,
  });

  int materialTypeId;
  String materialTypeName;

  factory MaterialTypes.fromJson(Map<String, dynamic> json) => MaterialTypes(
    materialTypeId: json["MaterialTypeID"],
    materialTypeName: json["MaterialTypeName"],
  );

  Map<String, dynamic> toJson() => {
    "MaterialTypeID": materialTypeId,
    "MaterialTypeName": materialTypeName,
  };
}
