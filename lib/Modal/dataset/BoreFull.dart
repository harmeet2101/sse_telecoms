// To parse this JSON data, do
//
//     final boreFull = boreFullFromJson(jsonString);

import 'dart:convert';

List<BoreFull> boreFullFromJson(String str) => List<BoreFull>.from(json.decode(str).map((x) => BoreFull.fromJson(x)));

String boreFullToJson(List<BoreFull> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BoreFull {
  BoreFull({
    this.boreFullId,
    this.boreFullName,
  });

  int boreFullId;
  String boreFullName;

  factory BoreFull.fromJson(Map<String, dynamic> json) => BoreFull(
    boreFullId: json["BoreFullID"],
    boreFullName: json["BoreFullName"],
  );

  Map<String, dynamic> toJson() => {
    "BoreFullID": boreFullId,
    "BoreFullName": boreFullName,
  };
}
