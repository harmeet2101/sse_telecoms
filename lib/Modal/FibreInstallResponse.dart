// To parse this JSON data, do
//
//     final fibreInstallResponse = fibreInstallResponseFromJson(jsonString);

import 'dart:convert';

FibreInstallResponse fibreInstallResponseFromJson(String str) => FibreInstallResponse.fromJson(json.decode(str));

String fibreInstallResponseToJson(FibreInstallResponse data) => json.encode(data.toJson());

class FibreInstallResponse {
  FibreInstallResponse({
    this.fibreInstallId,
  });

  int fibreInstallId;

  factory FibreInstallResponse.fromJson(Map<String, dynamic> json) => FibreInstallResponse(
    fibreInstallId: json["FibreInstallID"],
  );

  Map<String, dynamic> toJson() => {
    "FibreInstallID": fibreInstallId,
  };

  @override
  String toString() {
    return 'FibreInstallResponse{fibreInstallId: $fibreInstallId}';
  }
}
