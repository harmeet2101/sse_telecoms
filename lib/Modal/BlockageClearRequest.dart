// To parse this JSON data, do
//
//     final blockageClearRequest = blockageClearRequestFromJson(jsonString);

import 'dart:convert';

BlockageClearRequest blockageClearRequestFromJson(String str) => BlockageClearRequest.fromJson(json.decode(str));

String blockageClearRequestToJson(BlockageClearRequest data) => json.encode(data.toJson());

class BlockageClearRequest {
  BlockageClearRequest({
    this.authToken,
    this.submissionId,
    this.jobId,
    this.a55Id,
    this.blocks,
  });

  String authToken;
  String submissionId;
  int jobId;
  int a55Id;
  List<Block> blocks;

  factory BlockageClearRequest.fromJson(Map<String, dynamic> json) => BlockageClearRequest(
    authToken: json["AuthToken"],
    submissionId: json["SubmissionID"],
    jobId: json["JobID"],
    a55Id: json["A55ID"],
    blocks: List<Block>.from(json["Blocks"].map((x) => Block.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "AuthToken": authToken,
    "SubmissionID": submissionId,
    "JobID": jobId,
    "A55ID": a55Id,
    "Blocks": List<dynamic>.from(blocks.map((x) => x.toJson())),
  };

  @override
  String toString() {
    return 'BlockageClearRequest{authToken: $authToken, submissionId: $submissionId, jobId: $jobId, a55Id: $a55Id, blocks: $blocks}';
  }
}

class Block {
  Block({
    this.a55BlockageId,
    this.blockageOutcome,
    this.identifier,
    this.distance,
    this.rsLength,
    this.rsWidth,
    this.interim,
    this.surfaceTypeId,
    this.materialTypeId,
    this.blockageLat,
    this.blockageLng,
    this.comments,
  });

  int a55BlockageId;
  String blockageOutcome;
  String identifier;
  int distance;
  int rsLength;
  int rsWidth;
  bool interim;
  int surfaceTypeId;
  int materialTypeId;
  double blockageLat;
  double blockageLng;
  String comments;

  factory Block.fromJson(Map<String, dynamic> json) => Block(
    a55BlockageId: json["A55BlockageID"],
    blockageOutcome: json["BlockageOutcome"],
    identifier: json["Identifier"],
    distance: json["Distance"],
    rsLength: json["RsLength"],
    rsWidth: json["RsWidth"],
    interim: json["Interim"],
    surfaceTypeId: json["SurfaceTypeID"],
    materialTypeId: json["MaterialTypeID"],
    blockageLat: json["BlockageLat"],
    blockageLng: json["BlockageLng"],
    comments: json["Comments"],
  );

  Map<String, dynamic> toJson() => {
    "A55BlockageID": a55BlockageId,
    "BlockageOutcome": blockageOutcome,
    "Identifier": identifier,
    "Distance": distance,
    "RsLength": rsLength,
    "RsWidth": rsWidth,
    "Interim": interim,
    "SurfaceTypeID": surfaceTypeId,
    "MaterialTypeID": materialTypeId,
    "BlockageLat": blockageLat,
    "BlockageLng": blockageLng,
    "Comments": comments,
  };

  @override
  String toString() {
    return 'Block{a55BlockageId: $a55BlockageId, blockageOutcome: $blockageOutcome, identifier: $identifier, distance: $distance, rsLength: $rsLength, rsWidth: $rsWidth, interim: $interim, surfaceTypeId: $surfaceTypeId, materialTypeId: $materialTypeId, blockageLat: $blockageLat, blockageLng: $blockageLng, comments: $comments}';
  }
}
