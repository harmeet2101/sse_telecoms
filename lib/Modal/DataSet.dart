// To parse this JSON data, do
//
//     final dataSet = dataSetFromJson(jsonString);

import 'dart:convert';

DataSet dataSetFromJson(String str) => DataSet.fromJson(json.decode(str));

String dataSetToJson(DataSet data) => json.encode(data.toJson());

class DataSet {
  DataSet({
    this.surfaceTypes,
    this.speedLimits,
    this.trafficManagementTypes,
    this.cableTypes,
    this.materialTypes,
    this.percentageBoreFull,
    this.blockageOutcomeTypes,
    this.defectTypes,
    this.jointboxPhotoTypes,
  });

  List<SurfaceType> surfaceTypes;
  List<dynamic> speedLimits;
  List<TrafficManagementType> trafficManagementTypes;
  List<CableType> cableTypes;
  List<MaterialType> materialTypes;
  List<PercentageBoreFull> percentageBoreFull;
  List<BlockageOutcomeType> blockageOutcomeTypes;
  List<DefectType> defectTypes;
  List<dynamic> jointboxPhotoTypes;

  factory DataSet.fromJson(Map<String, dynamic> json) => DataSet(
    surfaceTypes: List<SurfaceType>.from(json["surfaceTypes"].map((x) => SurfaceType.fromJson(x))),
    speedLimits: List<dynamic>.from(json["speedLimits"].map((x) => x)),
    trafficManagementTypes: List<TrafficManagementType>.from(json["trafficManagementTypes"].map((x) => TrafficManagementType.fromJson(x))),
    cableTypes: List<CableType>.from(json["cableTypes"].map((x) => CableType.fromJson(x))),
    materialTypes: List<MaterialType>.from(json["materialTypes"].map((x) => MaterialType.fromJson(x))),
    percentageBoreFull: List<PercentageBoreFull>.from(json["percentageBoreFull"].map((x) => PercentageBoreFull.fromJson(x))),
    blockageOutcomeTypes: List<BlockageOutcomeType>.from(json["blockageOutcomeTypes"].map((x) => BlockageOutcomeType.fromJson(x))),
    defectTypes: List<DefectType>.from(json["defectTypes"].map((x) => DefectType.fromJson(x))),
    jointboxPhotoTypes: List<dynamic>.from(json["jointboxPhotoTypes"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "surfaceTypes": List<dynamic>.from(surfaceTypes.map((x) => x.toJson())),
    "speedLimits": List<dynamic>.from(speedLimits.map((x) => x)),
    "trafficManagementTypes": List<dynamic>.from(trafficManagementTypes.map((x) => x.toJson())),
    "cableTypes": List<dynamic>.from(cableTypes.map((x) => x.toJson())),
    "materialTypes": List<dynamic>.from(materialTypes.map((x) => x.toJson())),
    "percentageBoreFull": List<dynamic>.from(percentageBoreFull.map((x) => x.toJson())),
    "blockageOutcomeTypes": List<dynamic>.from(blockageOutcomeTypes.map((x) => x.toJson())),
    "defectTypes": List<dynamic>.from(defectTypes.map((x) => x.toJson())),
    "jointboxPhotoTypes": List<dynamic>.from(jointboxPhotoTypes.map((x) => x)),
  };
}

class BlockageOutcomeType {
  BlockageOutcomeType({
    this.blockageOutcomeTypeId,
    this.name,
    this.ordinal,
  });

  int blockageOutcomeTypeId;
  String name;
  int ordinal;

  factory BlockageOutcomeType.fromJson(Map<String, dynamic> json) => BlockageOutcomeType(
    blockageOutcomeTypeId: json["blockageOutcomeTypeId"],
    name: json["name"],
    ordinal: json["ordinal"],
  );

  Map<String, dynamic> toJson() => {
    "blockageOutcomeTypeId": blockageOutcomeTypeId,
    "name": name,
    "ordinal": ordinal,
  };
}

class CableType {
  CableType({
    this.cableTypeId,
    this.cableTypeName,
    this.isActive,
    this.createdDate,
    this.updatedDate,
  });

  String cableTypeId;
  String cableTypeName;
  bool isActive;
  DateTime createdDate;
  DateTime updatedDate;

  factory CableType.fromJson(Map<String, dynamic> json) => CableType(
    cableTypeId: json["cableTypeId"],
    cableTypeName: json["cableTypeName"],
    isActive: json["isActive"],
    createdDate: DateTime.parse(json["createdDate"]),
    updatedDate: DateTime.parse(json["updatedDate"]),
  );

  Map<String, dynamic> toJson() => {
    "cableTypeId": cableTypeId,
    "cableTypeName": cableTypeName,
    "isActive": isActive,
    "createdDate": createdDate.toIso8601String(),
    "updatedDate": updatedDate.toIso8601String(),
  };
}

class DefectType {
  DefectType({
    this.defectTypeId,
    this.name,
    this.ordinal,
  });

  int defectTypeId;
  String name;
  int ordinal;

  factory DefectType.fromJson(Map<String, dynamic> json) => DefectType(
    defectTypeId: json["defectTypeId"],
    name: json["name"],
    ordinal: json["ordinal"],
  );

  Map<String, dynamic> toJson() => {
    "defectTypeId": defectTypeId,
    "name": name,
    "ordinal": ordinal,
  };
}

class MaterialType {
  MaterialType({
    this.materialTypeId,
    this.materialTypeName,
    this.isActive,
    this.createdDate,
    this.updatedDate,
  });

  String materialTypeId;
  String materialTypeName;
  bool isActive;
  DateTime createdDate;
  DateTime updatedDate;

  factory MaterialType.fromJson(Map<String, dynamic> json) => MaterialType(
    materialTypeId: json["materialTypeId"],
    materialTypeName: json["materialTypeName"],
    isActive: json["isActive"],
    createdDate: DateTime.parse(json["createdDate"]),
    updatedDate: DateTime.parse(json["updatedDate"]),
  );

  Map<String, dynamic> toJson() => {
    "materialTypeId": materialTypeId,
    "materialTypeName": materialTypeName,
    "isActive": isActive,
    "createdDate": createdDate.toIso8601String(),
    "updatedDate": updatedDate.toIso8601String(),
  };
}

class PercentageBoreFull {
  PercentageBoreFull({
    this.percentageBoreFullId,
    this.percentageBoreFullName,
    this.isActive,
    this.createdDate,
    this.updatedDate,
  });

  String percentageBoreFullId;
  String percentageBoreFullName;
  bool isActive;
  DateTime createdDate;
  DateTime updatedDate;

  factory PercentageBoreFull.fromJson(Map<String, dynamic> json) => PercentageBoreFull(
    percentageBoreFullId: json["percentageBoreFullId"],
    percentageBoreFullName: json["percentageBoreFullName"],
    isActive: json["isActive"],
    createdDate: DateTime.parse(json["createdDate"]),
    updatedDate: DateTime.parse(json["updatedDate"]),
  );

  Map<String, dynamic> toJson() => {
    "percentageBoreFullId": percentageBoreFullId,
    "percentageBoreFullName": percentageBoreFullName,
    "isActive": isActive,
    "createdDate": createdDate.toIso8601String(),
    "updatedDate": updatedDate.toIso8601String(),
  };
}

class SurfaceType {
  SurfaceType({
    this.surfaceTypeId,
    this.surfaceTypeName,
    this.isActive,
    this.createdDate,
    this.updatedDate,
  });

  String surfaceTypeId;
  String surfaceTypeName;
  bool isActive;
  DateTime createdDate;
  DateTime updatedDate;

  factory SurfaceType.fromJson(Map<String, dynamic> json) => SurfaceType(
    surfaceTypeId: json["surfaceTypeId"],
    surfaceTypeName: json["surfaceTypeName"],
    isActive: json["isActive"],
    createdDate: DateTime.parse(json["createdDate"]),
    updatedDate: DateTime.parse(json["updatedDate"]),
  );

  Map<String, dynamic> toJson() => {
    "surfaceTypeId": surfaceTypeId,
    "surfaceTypeName": surfaceTypeName,
    "isActive": isActive,
    "createdDate": createdDate.toIso8601String(),
    "updatedDate": updatedDate.toIso8601String(),
  };
}

class TrafficManagementType {
  TrafficManagementType({
    this.a55TrafficManagementTypeId,
    this.a55TrafficManagementTypeName,
    this.createsA55TaskTypeId,
    this.isActive,
    this.createdDate,
    this.updatedDate,
    this.a55TaskType,
  });

  int a55TrafficManagementTypeId;
  String a55TrafficManagementTypeName;
  int createsA55TaskTypeId;
  bool isActive;
  DateTime createdDate;
  DateTime updatedDate;
  dynamic a55TaskType;

  factory TrafficManagementType.fromJson(Map<String, dynamic> json) => TrafficManagementType(
    a55TrafficManagementTypeId: json["a55TrafficManagementTypeId"],
    a55TrafficManagementTypeName: json["a55TrafficManagementTypeName"],
    createsA55TaskTypeId: json["createsA55TaskTypeId"],
    isActive: json["isActive"],
    createdDate: DateTime.parse(json["createdDate"]),
    updatedDate: DateTime.parse(json["updatedDate"]),
    a55TaskType: json["a55TaskType"],
  );

  Map<String, dynamic> toJson() => {
    "a55TrafficManagementTypeId": a55TrafficManagementTypeId,
    "a55TrafficManagementTypeName": a55TrafficManagementTypeName,
    "createsA55TaskTypeId": createsA55TaskTypeId,
    "isActive": isActive,
    "createdDate": createdDate.toIso8601String(),
    "updatedDate": updatedDate.toIso8601String(),
    "a55TaskType": a55TaskType,
  };
}
