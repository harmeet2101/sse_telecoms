// To parse this JSON data, do
//
//     final blockageLocateResponse = blockageLocateResponseFromJson(jsonString);

import 'dart:convert';

BlockageLocateResponse blockageLocateResponseFromJson(String str) => BlockageLocateResponse.fromJson(json.decode(str));

String blockageLocateResponseToJson(BlockageLocateResponse data) => json.encode(data.toJson());

class BlockageLocateResponse {
  BlockageLocateResponse({
    this.a55Id,
  });

  int a55Id;

  factory BlockageLocateResponse.fromJson(Map<String, dynamic> json) => BlockageLocateResponse(
    a55Id: json["A55ID"],
  );

  Map<String, dynamic> toJson() => {
    "A55ID": a55Id,
  };
}
