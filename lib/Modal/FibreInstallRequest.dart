// To parse this JSON data, do
//
//     final fibreInstallRequest = fibreInstallRequestFromJson(jsonString);

import 'dart:convert';

FibreInstallRequest fibreInstallRequestFromJson(String str) => FibreInstallRequest.fromJson(json.decode(str));

String fibreInstallRequestToJson(FibreInstallRequest data) => json.encode(data.toJson());

class FibreInstallRequest {
  FibreInstallRequest({
    this.fibreInstallationId,
    this.jobId,
    this.wpFrom,
    this.wpTo,
    this.cableNo,
    this.location,
    this.latitude,
    this.longitude,
    this.totalLength,
    this.section,
    this.ductSection,
    this.sectionLength,
    this.cableTypeId,
    this.noCables,
    this.boreIdentifiedAndUsed,
    this.manufacturerOfJoin,
    this.fibreCapacity,
    this.noCablePortsUsed,
    this.noCablePortsSpare,
    this.amountOfSlack,
    this.comments,
    this.loggedDateTime,
    this.fibreInstallationPhotos,
  });

  String fibreInstallationId;
  String jobId;
  String wpFrom;
  String wpTo;
  int cableNo;
  String location;
  double latitude;
  double longitude;
  String totalLength;
  String section;
  String ductSection;
  String sectionLength;
  String cableTypeId;
  int noCables;
  bool boreIdentifiedAndUsed;
  String manufacturerOfJoin;
  String fibreCapacity;
  int noCablePortsUsed;
  int noCablePortsSpare;
  String amountOfSlack;
  String comments;
  DateTime loggedDateTime;
  List<FibreInstallationPhoto> fibreInstallationPhotos;

  factory FibreInstallRequest.fromJson(Map<String, dynamic> json) => FibreInstallRequest(
    fibreInstallationId: json["fibreInstallationId"],
    jobId: json["jobId"],
    wpFrom: json["wpFrom"],
    wpTo: json["wpTo"],
    cableNo: json["cableNo"],
    location: json["location"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    totalLength: json["totalLength"],
    section: json["section"],
    ductSection: json["ductSection"],
    sectionLength: json["sectionLength"],
    cableTypeId: json["cableTypeId"],
    noCables: json["noCables"],
    boreIdentifiedAndUsed: json["boreIdentifiedAndUsed"],
    manufacturerOfJoin: json["manufacturerOfJoin"],
    fibreCapacity: json["fibreCapacity"],
    noCablePortsUsed: json["noCablePortsUsed"],
    noCablePortsSpare: json["noCablePortsSpare"],
    amountOfSlack: json["amountOfSlack"],
    comments: json["comments"],
    loggedDateTime: DateTime.parse(json["loggedDateTime"]),
    fibreInstallationPhotos: List<FibreInstallationPhoto>.from(json["fibreInstallationPhotos"].map((x) => FibreInstallationPhoto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "fibreInstallationId": fibreInstallationId,
    "jobId": jobId,
    "wpFrom": wpFrom,
    "wpTo": wpTo,
    "cableNo": cableNo,
    "location": location,
    "latitude": latitude,
    "longitude": longitude,
    "totalLength": totalLength,
    "section": section,
    "ductSection": ductSection,
    "sectionLength": sectionLength,
    "cableTypeId": cableTypeId,
    "noCables": noCables,
    "boreIdentifiedAndUsed": boreIdentifiedAndUsed,
    "manufacturerOfJoin": manufacturerOfJoin,
    "fibreCapacity": fibreCapacity,
    "noCablePortsUsed": noCablePortsUsed,
    "noCablePortsSpare": noCablePortsSpare,
    "amountOfSlack": amountOfSlack,
    "comments": comments,
    "loggedDateTime": loggedDateTime.toIso8601String(),
    "fibreInstallationPhotos": List<dynamic>.from(fibreInstallationPhotos.map((x) => x.toJson())),
  };
}

class FibreInstallationPhoto {
  FibreInstallationPhoto({
    this.fibreInstallationPhotoTypeId,
    this.fileBytes,
  });

  int fibreInstallationPhotoTypeId;
  String fileBytes;

  factory FibreInstallationPhoto.fromJson(Map<String, dynamic> json) => FibreInstallationPhoto(
    fibreInstallationPhotoTypeId: json["fibreInstallationPhotoTypeId"],
    fileBytes: json["fileBytes"],
  );

  Map<String, dynamic> toJson() => {
    "fibreInstallationPhotoTypeId": fibreInstallationPhotoTypeId,
    "fileBytes": fileBytes,
  };
}
