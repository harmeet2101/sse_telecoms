// To parse this JSON data, do
//
//     final postFibreInstall = postFibreInstallFromJson(jsonString);

import 'dart:convert';

PostFibreInstall postFibreInstallFromJson(String str) => PostFibreInstall.fromJson(json.decode(str));

String postFibreInstallToJson(PostFibreInstall data) => json.encode(data.toJson());

class PostFibreInstall {
  PostFibreInstall({
    this.authToken,
    this.submissionId,
    this.jobId,
    this.createdByUserId,
    this.startingJobWorkPointId,
    this.endingJobWorkPointId,
    this.ductSection,
    this.sectionLength,
    this.cableTypeId,
    this.cablesInSituId,
    this.boreIdentifiedAndUsed,
    this.manufacturerOfJoint,
    this.fibreCapacity,
    this.cablePortsUsed,
    this.cablesPortsSpare,
    this.slackAtJoint,
    this.comments,
  });

  String authToken;
  String submissionId;
  int jobId;
  int createdByUserId;
  int startingJobWorkPointId;
  int endingJobWorkPointId;
  String ductSection;
  int sectionLength;
  int cableTypeId;
  int cablesInSituId;
  String boreIdentifiedAndUsed;
  String manufacturerOfJoint;
  String fibreCapacity;
  int cablePortsUsed;
  int cablesPortsSpare;
  int slackAtJoint;
  String comments;

  factory PostFibreInstall.fromJson(Map<String, dynamic> json) => PostFibreInstall(
    authToken: json["AuthToken"],
    submissionId: json["SubmissionID"],
    jobId: json["JobID"],
    createdByUserId: json["CreatedByUserID"],
    startingJobWorkPointId: json["StartingJobWorkPointID"],
    endingJobWorkPointId: json["EndingJobWorkPointID"],
    ductSection: json["DuctSection"],
    sectionLength: json["SectionLength"],
    cableTypeId: json["CableTypeID"],
    cablesInSituId: json["CablesInSituID"],
    boreIdentifiedAndUsed: json["BoreIdentifiedAndUsed"],
    manufacturerOfJoint: json["ManufacturerOfJoint"],
    fibreCapacity: json["FibreCapacity"],
    cablePortsUsed: json["CablePortsUsed"],
    cablesPortsSpare: json["CablesPortsSpare"],
    slackAtJoint: json["SlackAtJoint"],
    comments: json["Comments"],
  );

  Map<String, dynamic> toJson() => {
    "AuthToken": authToken,
    "SubmissionID": submissionId,
    "JobID": jobId,
    "CreatedByUserID": createdByUserId,
    "StartingJobWorkPointID": startingJobWorkPointId,
    "EndingJobWorkPointID": endingJobWorkPointId,
    "DuctSection": ductSection,
    "SectionLength": sectionLength,
    "CableTypeID": cableTypeId,
    "CablesInSituID": cablesInSituId,
    "BoreIdentifiedAndUsed": boreIdentifiedAndUsed,
    "ManufacturerOfJoint": manufacturerOfJoint,
    "FibreCapacity": fibreCapacity,
    "CablePortsUsed": cablePortsUsed,
    "CablesPortsSpare": cablesPortsSpare,
    "SlackAtJoint": slackAtJoint,
    "Comments": comments,
  };
}
