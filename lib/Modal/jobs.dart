// To parse this JSON data, do
//
//     final jobs = jobsFromJson(jsonString);

import 'dart:convert';

List<Jobs> jobsFromJson(String str) => List<Jobs>.from(json.decode(str).map((x) => Jobs.fromJson(x)));

String jobsToJson(List<Jobs> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Jobs {
  Jobs({
    this.tempId,
    this.closedByName,
    this.closedByUserId,
    this.closedComments,
    this.closedDate,
    this.contractorId,
    this.contractorName,
    this.createdByName,
    this.createdByUserId,
    this.dateCreated,
    this.exchangeId,
    this.exchangeName,
    this.jobId,
    this.jobRefNumber,
    this.jobStatusId,
    this.jobStatusName,
    this.location,
    this.orReferenceNumber,
    this.postcode,
    this.regionId,
    this.regionName,
    this.whereaboutComments,
    this.endDate,
    this.gangId,
    this.startDate,
    this.whereaboutId,
    this.isHot,
    this.hotByUserId,
    this.hotComments,
    this.hotDate,
    this.gangRef,
  });

  String tempId;
  dynamic closedByName;
  dynamic closedByUserId;
  dynamic closedComments;
  dynamic closedDate;
  int contractorId;
  String contractorName;
  String createdByName;
  int createdByUserId;
  DateTime dateCreated;
  int exchangeId;
  String exchangeName;
  int jobId;
  String jobRefNumber;
  int jobStatusId;
  String jobStatusName;
  String location;
  String orReferenceNumber;
  String postcode;
  int regionId;
  String regionName;
  String whereaboutComments;
  DateTime endDate;
  int gangId;
  DateTime startDate;
  int whereaboutId;
  bool isHot;
  int hotByUserId;
  String hotComments;
  DateTime hotDate;
  String gangRef;

  factory Jobs.fromJson(Map<String, dynamic> json) => Jobs(
    tempId: json["TempID"],
    closedByName: json["ClosedByName"],
    closedByUserId: json["ClosedByUserID"],
    closedComments: json["ClosedComments"],
    closedDate: json["ClosedDate"],
    contractorId: json["ContractorID"],
    contractorName: json["ContractorName"],
    createdByName: json["CreatedByName"],
    createdByUserId: json["CreatedByUserID"],
    dateCreated: DateTime.parse(json["DateCreated"]),
    exchangeId: json["ExchangeID"],
    exchangeName: json["ExchangeName"],
    jobId: json["JobID"],
    jobRefNumber: json["JobRefNumber"],
    jobStatusId: json["JobStatusID"],
    jobStatusName: json["JobStatusName"],
    location: json["Location"],
    orReferenceNumber: json["OrReferenceNumber"],
    postcode: json["Postcode"],
    regionId: json["RegionID"],
    regionName: json["RegionName"],
    whereaboutComments: json["WhereaboutComments"] == null ? null : json["WhereaboutComments"],
    endDate: DateTime.parse(json["EndDate"]),
    gangId: json["GangID"],
    startDate: DateTime.parse(json["StartDate"]),
    whereaboutId: json["WhereaboutID"],
    isHot: json["IsHot"] == null ? null : json["IsHot"],
    hotByUserId: json["HotByUserID"] == null ? null : json["HotByUserID"],
    hotComments: json["HotComments"] == null ? null : json["HotComments"],
    hotDate: json["HotDate"] == null ? null : DateTime.parse(json["HotDate"]),
    gangRef: json["GangRef"],
  );

  Map<String, dynamic> toJson() => {
    "TempID": tempId,
    "ClosedByName": closedByName,
    "ClosedByUserID": closedByUserId,
    "ClosedComments": closedComments,
    "ClosedDate": closedDate,
    "ContractorID": contractorId,
    "ContractorName": contractorName,
    "CreatedByName": createdByName,
    "CreatedByUserID": createdByUserId,
    "DateCreated": dateCreated.toIso8601String(),
    "ExchangeID": exchangeId,
    "ExchangeName": exchangeName,
    "JobID": jobId,
    "JobRefNumber": jobRefNumber,
    "JobStatusID": jobStatusId,
    "JobStatusName": jobStatusName,
    "Location": location,
    "OrReferenceNumber": orReferenceNumber,
    "Postcode": postcode,
    "RegionID": regionId,
    "RegionName": regionName,
    "WhereaboutComments": whereaboutComments == null ? null : whereaboutComments,
    "EndDate": endDate.toIso8601String(),
    "GangID": gangId,
    "StartDate": startDate.toIso8601String(),
    "WhereaboutID": whereaboutId,
    "IsHot": isHot == null ? null : isHot,
    "HotByUserID": hotByUserId == null ? null : hotByUserId,
    "HotComments": hotComments == null ? null : hotComments,
    "HotDate": hotDate == null ? null : hotDate.toIso8601String(),
    "GangRef": gangRef,
  };

  @override
  String toString() {
    return 'Jobs{tempId: $tempId, closedByName: $closedByName, closedByUserId: $closedByUserId, closedComments: $closedComments, closedDate: $closedDate, contractorId: $contractorId, contractorName: $contractorName, createdByName: $createdByName, createdByUserId: $createdByUserId, dateCreated: $dateCreated, exchangeId: $exchangeId, exchangeName: $exchangeName, jobId: $jobId, jobRefNumber: $jobRefNumber, jobStatusId: $jobStatusId, jobStatusName: $jobStatusName, location: $location, orReferenceNumber: $orReferenceNumber, postcode: $postcode, regionId: $regionId, regionName: $regionName, whereaboutComments: $whereaboutComments, endDate: $endDate, gangId: $gangId, startDate: $startDate, whereaboutId: $whereaboutId, isHot: $isHot, hotByUserId: $hotByUserId, hotComments: $hotComments, hotDate: $hotDate, gangRef: $gangRef}';
  }
}
