// To parse this JSON data, do
//
//     final postBlockageLocate = postBlockageLocateFromJson(jsonString);

import 'dart:convert';

PostBlockageLocate postBlockageLocateFromJson(String str) => PostBlockageLocate.fromJson(json.decode(str));

String postBlockageLocateToJson(PostBlockageLocate data) => json.encode(data.toJson());

class PostBlockageLocate {
  PostBlockageLocate({
    this.authToken,
    this.submissionId,
    this.jobId,
    this.splitterNode,
    this.ductSection,
    this.sectionLength,
    this.boreFullId,
    this.trafficManagementId,
    this.tmComment,
    this.redRoute,
    this.private,
    this.contactDetails,
    this.speedLimitId,
    this.cableTypeId,
    this.cablesInSituId,
    this.numberOfBlocks,
    this.surfaceTypeAid,
    this.surfaceTypeBid,
    this.jointBoxALat,
    this.jointBoxBLat,
    this.jointBoxALng,
    this.jointBoxBLng,
    this.jointBoxALocation,
    this.jointBoxBLocation,
    this.blockageALat,
    this.blockageBLat,
    this.blockageALng,
    this.blockageBLng,
    this.blockageALocation,
    this.blockageBLocation,
    this.blockageADistance,
    this.blockageBDistance,
    this.ductPattern,
    this.jobWorkPointAid,
    this.jobWorkPointBid,
  });

  String authToken;
  String submissionId;
  int jobId;
  String splitterNode;
  String ductSection;
  int sectionLength;
  int boreFullId;
  int trafficManagementId;
  String tmComment;
  bool redRoute;
  bool private;
  String contactDetails;
  int speedLimitId;
  int cableTypeId;
  int cablesInSituId;
  int numberOfBlocks;
  int surfaceTypeAid;
  int surfaceTypeBid;
  int jointBoxALat;
  int jointBoxBLat;
  int jointBoxALng;
  int jointBoxBLng;
  String jointBoxALocation;
  String jointBoxBLocation;
  int blockageALat;
  int blockageBLat;
  int blockageALng;
  int blockageBLng;
  String blockageALocation;
  String blockageBLocation;
  int blockageADistance;
  int blockageBDistance;
  String ductPattern;
  int jobWorkPointAid;
  int jobWorkPointBid;

  factory PostBlockageLocate.fromJson(Map<String, dynamic> json) => PostBlockageLocate(
    authToken: json["AuthToken"],
    submissionId: json["SubmissionID"],
    jobId: json["JobID"],
    splitterNode: json["SplitterNode"],
    ductSection: json["DuctSection"],
    sectionLength: json["SectionLength"],
    boreFullId: json["BoreFullID"],
    trafficManagementId: json["TrafficManagementID"],
    tmComment: json["TMComment"],
    redRoute: json["RedRoute"],
    private: json["Private"],
    contactDetails: json["ContactDetails"],
    speedLimitId: json["SpeedLimitID"],
    cableTypeId: json["CableTypeID"],
    cablesInSituId: json["CablesInSituID"],
    numberOfBlocks: json["NumberOfBlocks"],
    surfaceTypeAid: json["SurfaceTypeAID"],
    surfaceTypeBid: json["SurfaceTypeBID"],
    jointBoxALat: json["JointBoxALat"],
    jointBoxBLat: json["JointBoxBLat"],
    jointBoxALng: json["JointBoxALng"],
    jointBoxBLng: json["JointBoxBLng"],
    jointBoxALocation: json["JointBoxALocation"],
    jointBoxBLocation: json["JointBoxBLocation"],
    blockageALat: json["BlockageALat"],
    blockageBLat: json["BlockageBLat"],
    blockageALng: json["BlockageALng"],
    blockageBLng: json["BlockageBLng"],
    blockageALocation: json["BlockageALocation"],
    blockageBLocation: json["BlockageBLocation"],
    blockageADistance: json["BlockageADistance"],
    blockageBDistance: json["BlockageBDistance"],
    ductPattern: json["DuctPattern"],
    jobWorkPointAid: json["JobWorkPointAID"],
    jobWorkPointBid: json["JobWorkPointBID"],
  );

  Map<String, dynamic> toJson() => {
    "AuthToken": authToken,
    "SubmissionID": submissionId,
    "JobID": jobId,
    "SplitterNode": splitterNode,
    "DuctSection": ductSection,
    "SectionLength": sectionLength,
    "BoreFullID": boreFullId,
    "TrafficManagementID": trafficManagementId,
    "TMComment": tmComment,
    "RedRoute": redRoute,
    "Private": private,
    "ContactDetails": contactDetails,
    "SpeedLimitID": speedLimitId,
    "CableTypeID": cableTypeId,
    "CablesInSituID": cablesInSituId,
    "NumberOfBlocks": numberOfBlocks,
    "SurfaceTypeAID": surfaceTypeAid,
    "SurfaceTypeBID": surfaceTypeBid,
    "JointBoxALat": jointBoxALat,
    "JointBoxBLat": jointBoxBLat,
    "JointBoxALng": jointBoxALng,
    "JointBoxBLng": jointBoxBLng,
    "JointBoxALocation": jointBoxALocation,
    "JointBoxBLocation": jointBoxBLocation,
    "BlockageALat": blockageALat,
    "BlockageBLat": blockageBLat,
    "BlockageALng": blockageALng,
    "BlockageBLng": blockageBLng,
    "BlockageALocation": blockageALocation,
    "BlockageBLocation": blockageBLocation,
    "BlockageADistance": blockageADistance,
    "BlockageBDistance": blockageBDistance,
    "DuctPattern": ductPattern,
    "JobWorkPointAID": jobWorkPointAid,
    "JobWorkPointBID": jobWorkPointBid,
  };
}
