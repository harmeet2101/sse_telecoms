// To parse this JSON data, do
//
//     final getEstimates = getEstimatesFromJson(jsonString);

import 'dart:convert';

GetEstimates getEstimatesFromJson(String str) => GetEstimates.fromJson(json.decode(str));

String getEstimatesToJson(GetEstimates data) => json.encode(data.toJson());

class GetEstimates {
  GetEstimates({
    this.estimates,
  });

  List<Estimate> estimates;

  factory GetEstimates.fromJson(Map<String, dynamic> json) => GetEstimates(
    estimates: List<Estimate>.from(json["estimates"].map((x) => Estimate.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "estimates": List<dynamic>.from(estimates.map((x) => x.toJson())),
  };
}

class Estimate {
  Estimate({
    this.jobId,
    this.estimateNumber,
  });

  String jobId;
  String estimateNumber;

  factory Estimate.fromJson(Map<String, dynamic> json) => Estimate(
    jobId: json["jobId"],
    estimateNumber: json["estimateNumber"],
  );

  Map<String, dynamic> toJson() => {
    "jobId": jobId,
    "estimateNumber": estimateNumber,
  };
}
