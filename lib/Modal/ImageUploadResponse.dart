// To parse this JSON data, do
//
//     final imageUploadResponse = imageUploadResponseFromJson(jsonString);

import 'dart:convert';

ImageUploadResponse imageUploadResponseFromJson(String str) => ImageUploadResponse.fromJson(json.decode(str));

String imageUploadResponseToJson(ImageUploadResponse data) => json.encode(data.toJson());

class ImageUploadResponse {
  ImageUploadResponse({
    this.photoId,
    this.dateCreated,
    this.filePath,
    this.photoTypeId,
    this.uploadedById,
    this.comments,
    this.jobId,
    this.a55Id,
    this.submissionId,
    this.a55BlockageId,
    this.defectId,
    this.fibreInstallId,
  });

  int photoId;
  DateTime dateCreated;
  String filePath;
  int photoTypeId;
  int uploadedById;
  dynamic comments;
  int jobId;
  int a55Id;
  dynamic submissionId;
  int a55BlockageId;
  int defectId;
  int fibreInstallId;

  factory ImageUploadResponse.fromJson(Map<String, dynamic> json) => ImageUploadResponse(
    photoId: json["PhotoID"],
    dateCreated: DateTime.parse(json["DateCreated"]),
    filePath: json["FilePath"],
    photoTypeId: json["PhotoTypeID"],
    uploadedById: json["UploadedByID"],
    comments: json["Comments"],
    jobId: json["JobID"],
    a55Id: json["A55ID"],
    submissionId: json["SubmissionID"],
    a55BlockageId: json["A55BlockageID"],
    defectId: json["DefectID"],
    fibreInstallId: json["FibreInstallID"],
  );

  Map<String, dynamic> toJson() => {
    "PhotoID": photoId,
    "DateCreated": dateCreated.toIso8601String(),
    "FilePath": filePath,
    "PhotoTypeID": photoTypeId,
    "UploadedByID": uploadedById,
    "Comments": comments,
    "JobID": jobId,
    "A55ID": a55Id,
    "SubmissionID": submissionId,
    "A55BlockageID": a55BlockageId,
    "DefectID": defectId,
    "FibreInstallID": fibreInstallId,
  };

  @override
  String toString() {
    return 'ImageUploadResponse{photoId: $photoId, dateCreated: $dateCreated, filePath: $filePath, photoTypeId: $photoTypeId, uploadedById: $uploadedById, comments: $comments, jobId: $jobId, a55Id: $a55Id, submissionId: $submissionId, a55BlockageId: $a55BlockageId, defectId: $defectId, fibreInstallId: $fibreInstallId}';
  }
}
