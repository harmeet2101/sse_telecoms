// To parse this JSON data, do
//
//     final a55Ids = a55IdsFromJson(jsonString);

import 'dart:convert';

A55Ids a55IdsFromJson(String str) => A55Ids.fromJson(json.decode(str));

String a55IdsToJson(A55Ids data) => json.encode(data.toJson());

class A55Ids {
  A55Ids({
    this.a55Ids,
  });

  List<String> a55Ids;

  factory A55Ids.fromJson(Map<String, dynamic> json) => A55Ids(
    a55Ids: List<String>.from(json["a55Ids"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "a55Ids": List<dynamic>.from(a55Ids.map((x) => x)),
  };

  @override
  String toString() {
    return 'A55Ids{a55Ids: $a55Ids}';
  }
}
