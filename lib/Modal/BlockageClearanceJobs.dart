// To parse this JSON data, do
//
//     final blockageClearanceJobs = blockageClearanceJobsFromJson(jsonString);

import 'dart:convert';

List<BlockageClearanceJobs> blockageClearanceJobsFromJson(String str) => List<BlockageClearanceJobs>.from(json.decode(str).map((x) => BlockageClearanceJobs.fromJson(x)));

String blockageClearanceJobsToJson(List<BlockageClearanceJobs> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BlockageClearanceJobs {
  BlockageClearanceJobs({
    this.tempId,
    this.a55Id,
    this.a55StatusId,
    this.boreFullId,
    this.jointBoxALat,
    this.jointBoxALng,
    this.jointBoxALocation,
    this.jointBoxBLat,
    this.jointBoxBLng,
    this.jointBoxBLocation,
    this.cablesInSituId,
    this.cableTypeId,
    this.contactDetails,
    this.dateLocated,
    this.dateValidated,
    this.ductSection,
    this.jobId,
    this.numberOfBlockages,
    this.private,
    this.redRouteBusLane,
    this.sectionLength,
    this.speedLimitId,
    this.splitterNode,
    this.tmComment,
    this.trafficManagementId,
    this.validatedBy,
    this.validatedComments,
    this.cableTypeName,
    this.trafficManagementName,
    this.cablesInSituName,
    this.boreFullName,
    this.a55StatusName,
    this.speedLimitName,
    this.exchangeId,
    this.jobRefNumber,
    this.jobStatusId,
    this.jobLocation,
    this.orReferenceNumber,
    this.jobPostcode,
    this.regionId,
    this.regionName,
    this.exchangeName,
    this.whereaboutId,
    this.gangId,
    this.startDate,
    this.endDate,
    this.contractorId,
    this.contractorName,
    this.comments,
    this.isHot,
    this.hotByUserId,
    this.hotComments,
    this.hotDate,
    this.createdBy,
    this.gangRef,
  });

  String tempId;
  int a55Id;
  int a55StatusId;
  int boreFullId;
  double jointBoxALat;
  double jointBoxALng;
  String jointBoxALocation;
  double jointBoxBLat;
  double jointBoxBLng;
  String jointBoxBLocation;
  int cablesInSituId;
  int cableTypeId;
  String contactDetails;
  DateTime dateLocated;
  DateTime dateValidated;
  String ductSection;
  int jobId;
  int numberOfBlockages;
  bool private;
  bool redRouteBusLane;
  double sectionLength;
  int speedLimitId;
  String splitterNode;
  String tmComment;
  int trafficManagementId;
  String validatedBy;
  String validatedComments;
  String cableTypeName;
  String trafficManagementName;
  String cablesInSituName;
  String boreFullName;
  String a55StatusName;
  String speedLimitName;
  int exchangeId;
  String jobRefNumber;
  int jobStatusId;
  String jobLocation;
  String orReferenceNumber;
  String jobPostcode;
  int regionId;
  String regionName;
  String exchangeName;
  int whereaboutId;
  int gangId;
  DateTime startDate;
  DateTime endDate;
  int contractorId;
  String contractorName;
  String comments;
  dynamic isHot;
  dynamic hotByUserId;
  dynamic hotComments;
  dynamic hotDate;
  String createdBy;
  String gangRef;

  factory BlockageClearanceJobs.fromJson(Map<String, dynamic> json) => BlockageClearanceJobs(
    tempId: json["TempID"],
    a55Id: json["A55ID"],
    a55StatusId: json["A55StatusID"],
    boreFullId: json["BoreFullID"],
    jointBoxALat: json["JointBoxALat"],
    jointBoxALng: json["JointBoxALng"],
    jointBoxALocation: json["JointBoxALocation"],
    jointBoxBLat: json["JointBoxBLat"],
    jointBoxBLng: json["JointBoxBLng"],
    jointBoxBLocation: json["JointBoxBLocation"],
    cablesInSituId: json["CablesInSituID"],
    cableTypeId: json["CableTypeID"],
    contactDetails: json["ContactDetails"],
    dateLocated: DateTime.parse(json["DateLocated"]),
    dateValidated: DateTime.parse(json["DateValidated"]),
    ductSection: json["DuctSection"],
    jobId: json["JobID"],
    numberOfBlockages: json["NumberOfBlockages"],
    private: json["Private"],
    redRouteBusLane: json["RedRouteBusLane"],
    sectionLength: json["SectionLength"],
    speedLimitId: json["SpeedLimitID"],
    splitterNode: json["SplitterNode"],
    tmComment: json["TMComment"],
    trafficManagementId: json["TrafficManagementID"],
    validatedBy: json["ValidatedBy"],
    validatedComments: json["ValidatedComments"],
    cableTypeName: json["CableTypeName"],
    trafficManagementName: json["TrafficManagementName"],
    cablesInSituName: json["CablesInSituName"],
    boreFullName: json["BoreFullName"],
    a55StatusName: json["A55StatusName"],
    speedLimitName: json["SpeedLimitName"],
    exchangeId: json["ExchangeID"],
    jobRefNumber: json["JobRefNumber"],
    jobStatusId: json["JobStatusID"],
    jobLocation: json["JobLocation"],
    orReferenceNumber: json["OrReferenceNumber"],
    jobPostcode: json["JobPostcode"],
    regionId: json["RegionID"],
    regionName: json["RegionName"],
    exchangeName: json["ExchangeName"],
    whereaboutId: json["WhereaboutID"],
    gangId: json["GangID"],
    startDate:DateTime.parse(json["StartDate"]),
    endDate: DateTime.parse(json["EndDate"]),
    contractorId: json["ContractorID"],
    contractorName: json["ContractorName"],
    comments: json["Comments"],
    isHot: json["IsHot"],
    hotByUserId: json["HotByUserID"],
    hotComments: json["HotComments"],
    hotDate: json["HotDate"],
    createdBy: json["CreatedBy"],
    gangRef: json["GangRef"],
  );

  Map<String, dynamic> toJson() => {
    "TempID": tempId,
    "A55ID": a55Id,
    "A55StatusID": a55StatusId,
    "BoreFullID": boreFullId,
    "JointBoxALat": jointBoxALat,
    "JointBoxALng": jointBoxALng,
    "JointBoxALocation": jointBoxALocation,
    "JointBoxBLat": jointBoxBLat,
    "JointBoxBLng": jointBoxBLng,
    "JointBoxBLocation": jointBoxBLocation,
    "CablesInSituID": cablesInSituId,
    "CableTypeID": cableTypeId,
    "ContactDetails": contactDetails,
    "DateLocated": dateLocated.toIso8601String(),
    "DateValidated": dateValidated.toIso8601String(),
    "DuctSection": ductSection,
    "JobID": jobId,
    "NumberOfBlockages": numberOfBlockages,
    "Private": private,
    "RedRouteBusLane": redRouteBusLane,
    "SectionLength": sectionLength,
    "SpeedLimitID": speedLimitId,
    "SplitterNode": splitterNode,
    "TMComment": tmComment,
    "TrafficManagementID": trafficManagementId,
    "ValidatedBy": validatedBy,
    "ValidatedComments": validatedComments,
    "CableTypeName": cableTypeName,
    "TrafficManagementName": trafficManagementName,
    "CablesInSituName": cablesInSituName,
    "BoreFullName": boreFullName,
    "A55StatusName": a55StatusName,
    "SpeedLimitName": speedLimitName,
    "ExchangeID": exchangeId,
    "JobRefNumber": jobRefNumber,
    "JobStatusID": jobStatusId,
    "JobLocation": jobLocation,
    "OrReferenceNumber": orReferenceNumber,
    "JobPostcode": jobPostcode,
    "RegionID": regionId,
    "RegionName": regionName,
    "ExchangeName": exchangeName,
    "WhereaboutID": whereaboutId,
    "GangID": gangId,
    "StartDate": startDate.toIso8601String(),
    "EndDate": endDate.toIso8601String(),
    "ContractorID": contractorId,
    "ContractorName": contractorName,
    "Comments": comments,
    "IsHot": isHot,
    "HotByUserID": hotByUserId,
    "HotComments": hotComments,
    "HotDate": hotDate,
    "CreatedBy": createdBy,
    "GangRef": gangRef,
  };

}
