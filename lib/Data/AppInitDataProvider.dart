import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/Modal/DataSet.dart';
import 'package:RouteProve/Modal/GetEstimates.dart';
import 'package:RouteProve/Modal/dataset/BoreFull.dart';
import 'package:RouteProve/Modal/dataset/DefectTypes.dart';
import 'package:RouteProve/Modal/dataset/cable_situ.dart';
import 'package:RouteProve/Modal/dataset/cable_type.dart';
import 'package:RouteProve/Modal/dataset/material_type.dart';
import 'package:RouteProve/Modal/dataset/speed_limit.dart';
import 'package:RouteProve/Modal/dataset/surface_type.dart';
import 'package:RouteProve/Modal/dataset/traffic_management.dart';

class AppInitDataProvider{

  static final AppInitDataProvider _obj = AppInitDataProvider._internal();
  factory AppInitDataProvider() => _obj;
  AppInitDataProvider._internal();

  DataSet _dataSet;
  GetEstimates _getEstimates;

  DataSet get dataSet => _dataSet;

  set dataSet(DataSet value) {
    _dataSet = value;
  }

  GetEstimates get getEstimates => _getEstimates;

  set getEstimates(GetEstimates value) {
    _getEstimates = value;
  }

  List<SpeedLimit> _speedLimitList;
  List<BoreFull> _boreFullList;
  List<CableInSitu> _cableSituList;
  List<CableTypes> _cableTypesList;
  List<MaterialTypes> _materialTypesList;
  List<SurfaceTypes> _surafaceTypesList;
  List<TrafficManagment> _TmTypesList;
  List<DefectTypes> _defectTypesList;


  List<DefectTypes> get defectTypesList => _defectTypesList;

  set defectTypesList(List<DefectTypes> value) {
    _defectTypesList = value;
  }

  List<BoreFull> get boreFullList => _boreFullList;

  set boreFullList(List<BoreFull> value) {
    _boreFullList = value;
  }

  List<ChoiceItem> getPercetageBoreFill(){

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_boreFullList.length;i++){

      ls.add(new ChoiceItem(key: _boreFullList[i].boreFullId,
          value: _boreFullList[i].boreFullName));
    }
    return ls;
  }

  List<ChoiceItem> getTrafficManagmentType(){

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_TmTypesList.length;i++){

      ls.add(new ChoiceItem(key: _TmTypesList[i].trafficManagementId,
          value: _TmTypesList[i].trafficManagementName));
    }
    return ls;
  }

  List<ChoiceItem> getCableType(){

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_cableTypesList.length;i++){

      ls.add(new ChoiceItem(key:_cableTypesList[i].cableTypeId,
          value: _cableTypesList[i].cableTypeName));
    }
    return ls;
  }

  List<ChoiceItem> getCableSituType(){

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_cableSituList.length;i++){

      ls.add(new ChoiceItem(key:_cableSituList[i].cablesInSituId,
          value: _cableSituList[i].cablesInSituName));
    }
    return ls;
  }

  List<ChoiceItem> getSurfaceType(){

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_surafaceTypesList.length;i++){

      ls.add(new ChoiceItem(key: _surafaceTypesList[i].surfaceTypeId,
          value: _surafaceTypesList[i].surfaceTypeName));
    }
    return ls;
  }

  List<ChoiceItem> getMaterialType(){

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_materialTypesList.length;i++){

      ls.add(new ChoiceItem(key:_materialTypesList[i].materialTypeId,
          value: _materialTypesList[i].materialTypeName));
    }
    return ls;
  }

  List<ChoiceItem> getSpeedLimitType(){

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_speedLimitList.length;i++){

      ls.add(new ChoiceItem(key:_speedLimitList[i].speedLimitId,
          value: _speedLimitList[i].speedLimitName));
    }
    return ls;
  }

  List<ChoiceItem> getBlockageOutcome(){ //getBlockageOutcome

    List<ChoiceItem> ls = new List();

   /* for(int i =0;i<_dataSet.blockageOutcomeTypes.length;i++){

      ls.add(new ChoiceItem(key: '${_dataSet.blockageOutcomeTypes[i].blockageOutcomeTypeId}',
          value: _dataSet.blockageOutcomeTypes[i].name));
    }*/
    return ls;
  }


  List<ChoiceItem> getDefectType(){ //getBlockageOutcome

    List<ChoiceItem> ls = new List();

    for(int i =0;i<_defectTypesList.length;i++){

      ls.add(new ChoiceItem(key: _defectTypesList[i].defectTypeId,
          value: _defectTypesList[i].defectTypeName));
    }
    return ls;
  }

  List<ChoiceItem> getEstimatesType(){

    List<ChoiceItem> ls = new List();

    /*for(int i =0;i<_getEstimates.estimates.length;i++){

      ls.add(new ChoiceItem(key: _getEstimates.estimates[i].jobId,
          value: _getEstimates.estimates[i].jobId));
    }*/
    return ls;
  }

  List<CableInSitu> get cableSituList => _cableSituList;

  set cableSituList(List<CableInSitu> value) {
    _cableSituList = value;
  }

  List<TrafficManagment> get TmTypesList => _TmTypesList;

  set TmTypesList(List<TrafficManagment> value) {
    _TmTypesList = value;
  }

  List<SurfaceTypes> get surafaceTypesList => _surafaceTypesList;

  set surafaceTypesList(List<SurfaceTypes> value) {
    _surafaceTypesList = value;
  }

  List<MaterialTypes> get materialTypesList => _materialTypesList;

  set materialTypesList(List<MaterialTypes> value) {
    _materialTypesList = value;
  }

  List<SpeedLimit> get speedLimitList => _speedLimitList;

  set speedLimitList(List<SpeedLimit> value) {
    _speedLimitList = value;
  }

  List<CableTypes> get cableTypesList => _cableTypesList;

  set cableTypesList(List<CableTypes> value) {
    _cableTypesList = value;
  }
}