import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:RouteProve/Common/helper.dart';
import 'package:RouteProve/Data/AppInitDataProvider.dart';
import 'package:RouteProve/Data/DBProvider.dart';
import 'package:RouteProve/Modal/A55Ids.dart';
import 'package:RouteProve/Modal/BlockageClearanceJobs.dart';
import 'package:RouteProve/Modal/BlockageLocateResponse.dart';
import 'package:RouteProve/Modal/ClearanceJobs.dart';
import 'package:RouteProve/Modal/DataSet.dart';
import 'package:RouteProve/Modal/DefectResponse.dart';
import 'package:RouteProve/Modal/FibreInstallResponse.dart';
import 'package:RouteProve/Modal/GetEstimates.dart';
import 'package:RouteProve/Modal/ImageUploadResponse.dart';
import 'package:RouteProve/Modal/KitBagResponse.dart';
import 'package:RouteProve/Modal/UserResponse.dart';
import 'package:RouteProve/Modal/dataset/BoreFull.dart';
import 'package:RouteProve/Modal/dataset/DefectTypes.dart';
import 'package:RouteProve/Modal/dataset/cable_situ.dart';
import 'package:RouteProve/Modal/dataset/cable_type.dart';
import 'package:RouteProve/Modal/dataset/material_type.dart';
import 'package:RouteProve/Modal/dataset/speed_limit.dart';
import 'package:RouteProve/Modal/dataset/surface_type.dart';
import 'package:RouteProve/Modal/dataset/traffic_management.dart';
import 'package:RouteProve/Modal/dataset/work_point.dart';
import 'package:RouteProve/Modal/jobs.dart';
import 'package:dio/dio.dart';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:path_provider/path_provider.dart';

class Api {
  static Dio httpDio;
  static Api api;

  static BaseOptions options = BaseOptions(
      baseUrl: Constants.BaseUrl,
      followRedirects: false,
      validateStatus: (status) {
        return status != 401;
      });

  Future<Dio> _getClient(BaseOptions baseOptions) async {
    String token = await Constants.getJwtToken();
    httpDio = httpDio == null ? Dio() : httpDio;
    httpDio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers["Authorization"] = "Bearer $token";
      options.headers["Accept"] = "application/json";
      options.headers['Content-Type'] = "application/json";

      return options;
    }, onResponse: (Response response) {
      return response;
    }, onError: (DioError error) async {
         // print('error: ${error.toString()}');
      if (error.response?.statusCode == 401) {
        RequestOptions options = error.request;
        String username = await Constants.getUsername();
        String password = await Constants.getPassword();
        Map<String, dynamic> body = {
          "Username": username,
          "Password": password
        };

        var response = await httpDio.post(
          "${Constants.BaseUrl}/Authentication/Authenticate",
          data: body,
          options: Options(),
        );
        if (response.statusCode == 200) {
          UserResponse userResponse = UserResponse.fromJson(response.data);
          if (userResponse != null) {
            if (userResponse!= null) {
              if (userResponse.token != null) {
                Constants.setUser(userResponse);
                Constants.setUserID(userResponse.userId.toString());
                Constants.setJwtToken(userResponse.token);
                options.headers["Authorization"] =
                    "Bearer ${userResponse.token}";
                return httpDio.request(options.path, options: options);
              }
            }
          }
        }
      }
      return error;
    }));

    if (baseOptions == null) {
      httpDio.options = options;
    } else {
      httpDio.options = baseOptions;
    }
    return httpDio;
  }

  Future<UserResponse> login(String username, String password) async {
    Map<String, dynamic> body = {"Username": username, "Password": password};

    /*body.forEach((key, value) {
      print("Key = $key & Value = $value");
      //Structurise the requestData using these Key and Value Pairs
    });*/
    Dio dio = Dio();
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";
    var response = await dio.post("${Constants.BaseUrl}/Authentication/Authenticate", data: body);

    if (response.statusCode == 200) {
      UserResponse userResponse = UserResponse.fromJson(response.data);
      return userResponse;
    } else {
      return null;
    }
  }

  Future<BlockageLocateResponse> submitBlockageLocate(Map<String, dynamic> requestData) async {
    try {
      Dio dio = Dio();
      String token = await Constants.getJwtToken();
      dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
        options.headers["Authorization"] = "Bearer $token";
        options.headers["Accept"] = "application/json";
        options.headers['Content-Type'] = "application/json";
        //return options;
      }, onError: (DioError error) async {
        print('error: ${error.toString()}');
        return null;
      }));

      var response = await dio.post("${Constants.BaseUrl}/BoxToBox/BlockageLocate",
          data: requestData);
      print(response.toString());
      BlockageLocateResponse locateResponse = BlockageLocateResponse.fromJson(response.data);
      if (response.statusCode == 200) {
        return locateResponse;
      } else {
        return null;
      }
    } on Exception catch (e) {
      // TODO
      return null;
    }
  }

  Future<bool> submitBlockageClearance(Map<String, dynamic> requestData) async {

    try {
      Dio dio = Dio();
      String token = await Constants.getJwtToken();
      dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
        options.headers["Authorization"] = "Bearer $token";
        options.headers["Accept"] = "application/json";
        options.headers['Content-Type'] = "application/json";
        //return options;
      }, onError: (DioError error) async {
        print('error: ${error.toString()}');
        return false;
      }));

      var response = await dio.post("${Constants.BaseUrl}/BoxToBox/BlockageClear",
          data: requestData);
      print(response.toString());
      if (response.statusCode == 200) {
        return false;
      } else {
        return false;
      }
    } on Exception catch (e) {
      // TODO
      return false;
    }

  }

  Future<FibreInstallResponse> submitFibreInstall(Map<String, dynamic> requestData) async {


    try {
      Dio dio = Dio();
      String token = await Constants.getJwtToken();
      dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
        options.headers["Authorization"] = "Bearer $token";
        options.headers["Accept"] = "application/json";
        options.headers['Content-Type'] = "application/json";
        //return options;
      }, onError: (DioError error) async {
        print('error: ${error.toString()}');
        return null;
      }));

      var response = await dio.post("${Constants.BaseUrl}/BoxToBox/SubmitFibreInstall",
          data: requestData);
      FibreInstallResponse fibreInstallResponse = FibreInstallResponse.fromJson(response.data);
      print(fibreInstallResponse.toString());
      if (response.statusCode == 200) {
        return fibreInstallResponse;
      } else {
        return fibreInstallResponse;
      }
    } on Exception catch (e) {
      // TODO
      return null;
    }


    /*Dio dio = await _getClient(null);
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";

    var response = await dio.post("${Constants.BaseUrl}/BoxToBox/SubmitFibreInstall",
        data: requestData);
    FibreInstallResponse fibreInstallResponse = FibreInstallResponse.fromJson(response.data);
    print(fibreInstallResponse.toString());
    if (response.statusCode == 200) {
      return fibreInstallResponse;
    } else {
      return fibreInstallResponse;
    }*/
  }

  Future<DefectResponse> submitDefect(Map<String, dynamic> requestData) async {

    try {
      Dio dio = Dio();
      String token = await Constants.getJwtToken();
      dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
        options.headers["Authorization"] = "Bearer $token";
        options.headers["Accept"] = "application/json";
        options.headers['Content-Type'] = "application/json";
        //return options;
      }, onError: (DioError error) async {
        print('error: ${error.toString()}');
        return null;
      }));

      var response = await dio.post("${Constants.BaseUrl}/BoxToBox/SubmitDefect",
          data: requestData);
      DefectResponse defectResponse = DefectResponse.fromJson(response.data);
      print(response.toString());
      if (response.statusCode == 200) {
        return defectResponse;
      } else {
        return defectResponse;
      }
    } on Exception catch (e) {
      // TODO
      return null;
    }

    /*Dio dio = await _getClient(null);
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";


    var response = await dio.post("${Constants.BaseUrl}/BoxToBox/SubmitDefect",
        data: requestData);
    DefectResponse defectResponse = DefectResponse.fromJson(response.data);
    print(response.toString());
    if (response.statusCode == 200) {
      return defectResponse;
    } else {
      return defectResponse;
    }*/
  }

  /*Future<DataSet> getDataSet()async{

    Dio dio = await _getClient(null);
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";

    var response = await dio.get("${Constants.BaseUrl}/app/route-proving/get-dataset");
   // print(response.toString());
    if (response.statusCode == 200) {
      DataSet dataSet = DataSet.fromJson(response.data);
      AppInitDataProvider().dataSet = dataSet;
      return dataSet;
    } else {
      return null;
    }
  }*/

  Future<GetEstimates> getEstimates()async{

    Dio dio = await _getClient(null);
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";

    var response = await dio.get("${Constants.BaseUrl}/app/route-proving/get-estimates");
    //print(response.toString());
    if (response.statusCode == 200) {
      GetEstimates getEstimates = GetEstimates.fromJson(response.data);
      AppInitDataProvider().getEstimates = getEstimates;
      return getEstimates;
    } else {
      return null;
    }
  }

  Future<A55Ids> getA55Ids(Estimate estimate)async{

    Dio dio = await _getClient(null);
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";

    var response = await dio.get("${Constants.BaseUrl}/app/route-proving/${estimate.jobId}/get-a55");
    if (response.statusCode == 200) {
      A55Ids a55ids = A55Ids.fromJson(response.data);
      print(a55ids.toString());
      return a55ids;
    }else {
      return null;
  }
  }

  Future<List<ClearanceJobs>> getClearanceJobs(List<String> a55Id)async{

    Dio dio = await _getClient(null);
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";

    List<Future<ClearanceJobs>> list = new List();
    for(int i =0;i<a55Id.length;i++){
      list.add(temp(a55Id[i]));
    }
    List<ClearanceJobs> cl = await Future.wait(list);


    // print(cl.toString());

     return cl;
  }

  Future<ClearanceJobs> temp(String a55id)async{

    Dio dio = await _getClient(null);
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";
    var response = await dio.get("${Constants.BaseUrl}/app/route-proving/${a55id}/get-clearance-jobs");
    if (response.statusCode == 200) {
      ClearanceJobs clearanceJobs = ClearanceJobs.fromJson(response.data);
      return clearanceJobs;
    } else {
      return null;
    }
  }

  Future<List<Jobs>> getJobs()async{
    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
    Dio dio = await _getClient(null);
    String authToken = await Constants.getAuthToken();
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";
    var response = await dio.get("${Constants.BaseUrl}/BoxToBox/GetJobs?AuthToken=${authToken}");

    if (response.statusCode == 200) {
      List<Jobs> jobs = List<Jobs>.from((response.data).map((x) => Jobs.fromJson(x)));
      Map<String, dynamic> mp = {'jobs': jobs};
      DBProvider.db.addAnswer('jobs', 'jobs', mp);
      //print(jobs.length);
      return jobs;
    } else {
      return null;
    }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('jobs', 'jobs');
      List<Jobs> ls = List<Jobs>.from((mp['jobs']).map((x) => Jobs.fromJson(x)));
      return ls;
    }
  }

  Future<List<BoreFull>> getBoreFull()async{
    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get(
          "${Constants.BaseUrl}/BoxToBox/GetBoreFulls?AuthToken=${authToken}");
      // print(response.toString());
      if (response.statusCode == 200) {
        List<BoreFull> ls = List<BoreFull>.from(
            (response.data).map((x) => BoreFull.fromJson(x)));
        AppInitDataProvider().boreFullList = ls;
        Map<String, dynamic> mp = {'borefullType': ls};
        DBProvider.db.addAnswer('borefull_types', 'borefull_types', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('borefull_types', 'borefull_types');
      List<BoreFull> ls = List<BoreFull>.from((mp['borefullType']).map((x) => BoreFull.fromJson(x)));
      return ls;
    }
  }
  Future<List<SpeedLimit>> getSpeedLimits()async{
    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get("${Constants
          .BaseUrl}/BoxToBox/GetSpeedLimits?AuthToken=${authToken}");
      // print(response.toString());
      if (response.statusCode == 200) {
        List<SpeedLimit> ls = List<SpeedLimit>.from(
            (response.data).map((x) => SpeedLimit.fromJson(x)));
        AppInitDataProvider().speedLimitList = ls;
        Map<String, dynamic> mp = {'slType': ls};
        DBProvider.db.addAnswer('sl_types', 'sl_types', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('sl_types', 'sl_types');
      List<SpeedLimit> ls = List<SpeedLimit>.from((mp['slType']).map((x) => SpeedLimit.fromJson(x)));
      return ls;
    }
  }
  Future<List<TrafficManagment>> getTmTypes()async{
    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get(
          "${Constants.BaseUrl}/BoxToBox/GetTmTypes?AuthToken=${authToken}");
      // print(response.toString());
      if (response.statusCode == 200) {
        List<TrafficManagment> ls = List<TrafficManagment>.from(
            (response.data).map((x) => TrafficManagment.fromJson(x)));
        AppInitDataProvider().TmTypesList = ls;
        Map<String, dynamic> mp = {'TmType': ls};
        DBProvider.db.addAnswer('tm_types', 'tm_types', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('tm_types', 'tm_types');
      List<TrafficManagment> ls = List<TrafficManagment>.from((mp['TmType']).map((x) => TrafficManagment.fromJson(x)));
      return ls;
    }
  }
  Future<List<SurfaceTypes>> getSurfaceTypes()async{

    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get("${Constants
          .BaseUrl}/BoxToBox/GetSurfaceTypes?AuthToken=${authToken}");
      // print(response.toString());
      if (response.statusCode == 200) {
        List<SurfaceTypes> ls = List<SurfaceTypes>.from(
            (response.data).map((x) => SurfaceTypes.fromJson(x)));
        AppInitDataProvider().surafaceTypesList = ls;
        Map<String, dynamic> mp = {'surfaceType': ls};
        DBProvider.db.addAnswer('surface_types', 'surface_types', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('surface_types', 'surface_types');
      List<SurfaceTypes> ls = List<SurfaceTypes>.from((mp['surfaceType']).map((x) => SurfaceTypes.fromJson(x)));
      return ls;
    }
  }
  Future<List<MaterialTypes>> getMaterialTypes()async{

    bool internetAvail = await isInternetAvailable();
    if(internetAvail){
      Dio dio = await _getClient(null);
    String authToken = await Constants.getAuthToken();
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";

    var response = await dio.get("${Constants.BaseUrl}/BoxToBox/GetMaterialTypes?AuthToken=${authToken}");
    // print(response.toString());
    if (response.statusCode == 200) {
      List<MaterialTypes> ls = List<MaterialTypes>.from((response.data).map((x) => MaterialTypes.fromJson(x)));
      AppInitDataProvider().materialTypesList = ls;
      Map<String,dynamic> mp = {'materialType':ls};
      DBProvider.db.addAnswer('material_types', 'material_types', mp);
      return ls;
    } else {
      return null;
    }
  }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('material_types', 'material_types');
      List<MaterialTypes> ls = List<MaterialTypes>.from((mp['materialType']).map((x) => MaterialTypes.fromJson(x)));
      return ls;
    }

  }
  Future<List<CableInSitu>> getCableInSitu()async{

    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get("${Constants
          .BaseUrl}/BoxToBox/GetCableInSitus?AuthToken=${authToken}");
       print(response.toString());
      if (response.statusCode == 200) {
        List<CableInSitu> ls = List<CableInSitu>.from(
            (response.data).map((x) => CableInSitu.fromJson(x)));

        ls.sort((a, b) {
          return a.cablesInSituId.compareTo(b.cablesInSituId);
        });
        print(ls.toString());
        AppInitDataProvider().cableSituList = ls;
        Map<String, dynamic> mp = {'cableTypeSitu': ls};
        DBProvider.db.addAnswer('cable_types_situ', 'cable_types_situ', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('cable_types_situ', 'cable_types_situ');
      List<CableInSitu> ls = List<CableInSitu>.from((mp['cableTypeSitu']).map((x) => CableInSitu.fromJson(x)));
      return ls;
    }
  }
  Future<List<CableTypes>> getCableTypes()async{

    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get(
          "${Constants.BaseUrl}/BoxToBox/GetCableTypes?AuthToken=${authToken}");
      // print(response.toString());
      if (response.statusCode == 200) {
        List<CableTypes> ls = List<CableTypes>.from(
            (response.data).map((x) => CableTypes.fromJson(x)));
        AppInitDataProvider().cableTypesList = ls;
        Map<String, dynamic> mp = {'cableType': ls};
        DBProvider.db.addAnswer('cable_types', 'cable_types', mp);
        return ls;
      } else {
        return null;
      }
    }else{

      Map<String,dynamic> mp = await DBProvider.db.getAnswer('cable_types', 'cable_types');
      List<CableTypes> ls = List<CableTypes>.from((mp['cableType']).map((x) => CableTypes.fromJson(x)));
      return ls;
    }
  }


  Future<List<DefectTypes>> getDefectTypes()async{

    bool internetAvail = await isInternetAvailable();
    if(internetAvail) {
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get("${Constants
          .BaseUrl}/BoxToBox/GetDefectTypes?AuthToken=${authToken}");
      // print(response.toString());
      if (response.statusCode == 200) {
        List<DefectTypes> ls = List<DefectTypes>.from(
            (response.data).map((x) => DefectTypes.fromJson(x)));
        AppInitDataProvider().defectTypesList = ls;
        Map<String, dynamic> mp = {'defectType': ls};
        DBProvider.db.addAnswer('defect_types', 'defect_types', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('defect_types', 'defect_types');
      List<DefectTypes> ls = List<DefectTypes>.from((mp['defectType']).map((x) => DefectTypes.fromJson(x)));
      return ls;
    }
  }

  Future<List<WorkPoint>> getWorkPoints(int jobId)async{

    bool internetAvail = await isInternetAvailable();
    if(internetAvail){
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get("${Constants.BaseUrl}/BoxToBox/GetStructure?AuthToken=${authToken}&JobID=${jobId}");
      //  print(response.toString());
      if (response.statusCode == 200) {
        List<WorkPoint> ls = List<WorkPoint>.from((response.data).map((x) => WorkPoint.fromJson(x)));
        Map<String,dynamic> mp = {'wp':ls};
        DBProvider.db.addAnswer('work_points', 'work_points', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('work_points', 'work_points');
      List<WorkPoint> ls = List<WorkPoint>.from((mp['wp']).map((x) => WorkPoint.fromJson(x)));
      return ls;
    }


  }

  Future<List<BlockageClearanceJobs>> getBlockageClearanceJobs()async{

    bool internetAvail = await isInternetAvailable();
    if(internetAvail){
      Dio dio = await _getClient(null);
      String authToken = await Constants.getAuthToken();
      dio.options.headers["Accept"] = "application/json";
      dio.options.headers['Content-Type'] = "application/json";

      var response = await dio.get("${Constants.BaseUrl}/BoxToBox/GetBlockageClearanceJobs?AuthToken=${authToken}");
      // print(response.toString());
      if (response.statusCode == 200) {
        List<BlockageClearanceJobs> ls = List<BlockageClearanceJobs>.from((response.data).map((x) => BlockageClearanceJobs.fromJson(x)));
        Map<String,dynamic> mp = {'clearanceJobs':ls};
        DBProvider.db.addAnswer('clearance_Jobs', 'clearance_Jobs', mp);
        return ls;
      } else {
        return null;
      }
    }else{
      Map<String,dynamic> mp = await DBProvider.db.getAnswer('clearance_Jobs', 'clearance_Jobs');
      List<BlockageClearanceJobs> ls = List<BlockageClearanceJobs>.from((mp['clearanceJobs']).map((x) =>
          BlockageClearanceJobs.fromJson(x)));
      return ls;
    }

  }

  Future<List<KitBagResponse>> getKitBagList()async{

    Dio dio = await _getClient(null);
    String authToken = await Constants.getAuthToken();
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "application/json";

    var response = await dio.get("${Constants.BaseUrl}/KitBag/Get?AuthToken=${authToken}");
     print(response.toString());
    if (response.statusCode == 200) {
      List<KitBagResponse> ls = List<KitBagResponse>.from((response.data).map((x) => KitBagResponse.fromJson(x)));
      print(ls.length);
      return ls;
    } else {
      return null;
    }
  }

  Future<ImageUploadResponse> uploadImage(Map<String,dynamic> mp)async{

    FormData formData = new FormData.fromMap(mp);
    Dio dio = new Dio();
    dio.options.headers["Accept"] = "application/json";
    dio.options.headers['Content-Type'] = "multipart/form-data";
   /* mp.forEach((key, value) {
      print('${key}: ${value}');
    });*/
    var response = await dio.post("${Constants.BaseUrl}/BoxToBox/Photos/Upload", data: formData);
    print(response.toString());
    if (response.statusCode == 200) {
      ImageUploadResponse ls = ImageUploadResponse.fromJson(response.data);
      print(ls.toString());
      // AppInitDataProvider().cableTypesList = ls;
      return ls;
    } else {
      return null;
    }
  }


  Future<String> downloadEbook(String fileUrl ,Function showDownloadProgress) async {
    try {
      var appDirectory = await getApplicationDocumentsDirectory();
      bool isDirectExists = await Directory('${appDirectory.path}/Docs').exists();
      if(!isDirectExists){

        await Directory('${appDirectory.path}/Docs').create();
      }
      String filePath = "${appDirectory.path}/Docs/${fileUrl.split('/').last}.pdf";
      bool fileExists = await File(filePath).exists();
      if (fileExists) {
        return filePath;
      } else {
        Response response = await Dio().get(
          fileUrl,
          options: Options(
              responseType: ResponseType.bytes,
              followRedirects: true,
              validateStatus: (status) {
                return status < 500;
              }),
          onReceiveProgress: showDownloadProgress,
        );
        File file = File(filePath);
        var raf = file.openSync(mode: FileMode.write);
        raf.writeFromSync(response.data);
        await raf.close();
        return filePath;
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<String> checkPdf(String fileUrl ) async {
    try {
      var appDirectory = await getApplicationDocumentsDirectory();
      String filePath = "${appDirectory.path}/Docs/${fileUrl.split('/').last}.pdf";
      bool fileExists = await File(filePath).exists();
      print('read $filePath');
      if (fileExists) {
        print(fileExists);
        return filePath;
      }else{
        return '';
      }
    }  catch (e) {
      return null;
    }
  }
}
