import 'dart:async';
import 'dart:typed_data';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Common/helper.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

const TABLE_ANSWERS = "answers";

//columns name for various tables
const CL_ID = "id";
const CL_NAME = "name";
const CL_USER = "user";
const CL_IS_COMPLETED = "is_completed";
const CL_IS_UPLOADED = "is_uploaded";
const CL_ANSWER = "answer";

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    } else {
      _database = await initDB();
      return _database;
    }
  }

  initDB() async {
    String path = "${FileHandler.localPath}/DataProvider.db";
    debugPrint("DB_PATH : $path");
    Sqflite.setDebugModeOn(true);
    return await openDatabase(path, version: 1, onOpen: (db) {
      db.getVersion().then((v) {
        debugPrint("database opend V : $v");
      });
    }, onCreate: (Database db, int version) async {
      //CREATE ANSWERS TABLE
      await db.execute("CREATE TABLE $TABLE_ANSWERS ("
          "$CL_ID TEXT PRIMARY KEY,"
          "$CL_NAME TEXT,"
          "$CL_USER TEXT,"
          "$CL_IS_COMPLETED BIT,"
          "$CL_IS_UPLOADED BIT,"
          "$CL_ANSWER BLOB"
          ")");
    });
  }

  addAnswer(String id, String name, Map<String, dynamic> answers) async {
    final db = await database;
    Batch batch = db.batch();
    Uint8List data = encodeMap(answers);
    String user = await Constants.getUserID();
    List<Map> existingData = await db.query(
      TABLE_ANSWERS,
      where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
      whereArgs: [id, name, user],
      limit: 1,
    );
    if (existingData != null && existingData.length > 0) {
      batch.update(
        TABLE_ANSWERS,
        {
          CL_ANSWER: data,
        },
        where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
        whereArgs: [id, name, user],
      );
    } else {
      batch.insert(TABLE_ANSWERS, {
        CL_ID: id,
        CL_NAME: name,
        CL_USER: user,
        CL_IS_COMPLETED: 0,
        CL_IS_UPLOADED: 0,
        CL_ANSWER: data,
      });
    }

    await batch.commit(noResult: true);
  }

  changeAnswerUploadStatus(String id, String name, bool isUploaded) async {
    final db = await database;
    int value = isUploaded ? 1 : 0;
    String user = await Constants.getUserID();
    await db.update(
        TABLE_ANSWERS,
        {
          '$CL_IS_UPLOADED': value,
        },
        where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
        whereArgs: [id, name, user]);
  }

  changeAnswerCompletedStatus(String id, String name, bool isCompleted) async {
    final db = await database;
    int value = isCompleted ? 1 : 0;
    String user = await Constants.getUserID();
    await db.update(
        TABLE_ANSWERS,
        {
          '$CL_IS_COMPLETED': value,
        },
        where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
        whereArgs: [id, name, user]);
  }

  Future<Map<String, dynamic>> getAnswer(String id, String name) async {
    final db = await database;
    String user = await Constants.getUserID();
    List<Map> data = await db.query(TABLE_ANSWERS,
        columns: [CL_ANSWER],
        where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
        whereArgs: [id, name, user],
        limit: 1);
    if (data != null && data.length > 0 && data[0][CL_ANSWER] != null) {
      return decodeMap(data[0][CL_ANSWER]);
    } else {
      return Map<String, dynamic>();
    }
  }

  Future<bool> getAnswerUploadStatus(String id, String name) async {
    final db = await database;
    String user = await Constants.getUserID();
    List<Map> data = await db.query(TABLE_ANSWERS,
        columns: [CL_IS_UPLOADED],
        where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
        whereArgs: [id, name, user],
        limit: 1);
    if (data != null && data.length > 0 && data[0][CL_IS_UPLOADED] != null) {
      int value = data[0][CL_IS_UPLOADED];
      bool boolValue = value == 1 ? true : false;
      return boolValue;
    } else {
      return false;
    }
  }

  Future<bool> getAnswerCompletedStatus(String id, String name) async {
    final db = await database;
    String user = await Constants.getUserID();
    List<Map> data = await db.query(TABLE_ANSWERS,
        columns: [CL_IS_COMPLETED],
        where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
        whereArgs: [id, name, user],
        limit: 1);
    if (data != null && data.length > 0 && data[0][CL_IS_COMPLETED] != null) {
      int value = data[0][CL_IS_COMPLETED];
      bool boolValue = value == 1 ? true : false;
      return boolValue;
    } else {
      return false;
    }
  }

  Future<List<String>> getOfflineQueueList() async {
    final db = await database;
    String user = await Constants.getUserID();
    List<Map> data = await db.query(TABLE_ANSWERS,
        columns: [CL_ID,CL_NAME],
        where: "$CL_IS_COMPLETED = 1 AND $CL_IS_UPLOADED = 0");

    List<String> ls = new List();
    if (data != null && data.length > 0 ) {

      data.forEach((element) {
        ls.add('${element[CL_ID]}_${element[CL_NAME]}');

      });

      ls.forEach((element) {
        print(element);
      });
      return ls;

    } else {
      return ls;
    }
  }


  deleteAnswer(String id, String name) async {
    final db = await database;
    String user = await Constants.getUserID();
    await db.delete(TABLE_ANSWERS,
        where: "$CL_ID = ? AND $CL_NAME = ? AND $CL_USER = ?",
        whereArgs: [id, name, user]);
  }
}
