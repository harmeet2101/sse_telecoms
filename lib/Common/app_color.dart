import 'package:flutter/material.dart';

const transparentColor = Colors.transparent;
const appThemeColor = Color.fromRGBO(81, 81, 81, 1);
const appBackgroundColor = Color.fromRGBO(247, 247, 250, 1);
const appTextColor = Colors.black87;
const appSecondaryTextColor = Colors.black54;
const appSecondaryColor = Color.fromRGBO(38, 167, 69, 1);


// rgb(81, 81, 81)
// rgb(38, 167, 69)

//247, 247, 250,

//72 117 182