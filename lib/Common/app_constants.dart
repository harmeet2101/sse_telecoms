import 'dart:convert';

import 'package:RouteProve/Modal/UserResponse.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_keychain/flutter_keychain.dart';

class Constants {
  static const String BaseUrl = 'https://dev-sse-telecoms-app-api.depotnet.co.uk';   //"https://cass-blockages-app-api.depotnet.co.uk";

  static Future<String> fetchAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  static Future<String> fetchAppBuildNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.buildNumber;
  }

  static setLoginStatus(bool loginStatus) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("isLogin", loginStatus);
  }

  static Future<bool> getLoginStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool("isLogin");
    if (isLogin == null) {
      isLogin = false;
    }
    return isLogin;
  }

  static setUsername(String username) async {
    await FlutterKeychain.put(key: "username", value: username);
  }

  static Future<String> getUsername() async {
    var username = await FlutterKeychain.get(key: "username");
    if (username == null || username.isEmpty) {
      username = "";
    }
    return username;
  }

  static setPassword(String password) async {
    await FlutterKeychain.put(key: "password", value: password);
  }

  static Future<String> getPassword() async {
    var password = await FlutterKeychain.get(key: "password");
    if (password == null || password.isEmpty) {
      password = "";
    }
    return password;
  }

  static setUser(UserResponse user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = jsonEncode(user);
    await prefs.setString("user", userString);
  }

  static Future<UserResponse> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userString = prefs.getString('user');
    Map userMap = jsonDecode(userString);
    var user = UserResponse.fromJson(userMap);
    if (user == null) {
      user = UserResponse();
    }
    return user;
  }

  static setUserID(String userId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("userId", userId);
  }

  static Future<String> getUserID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString('userId');
    if (userId == null) {
      userId = "userId";
    }
    return userId;
  }

  static setJwtToken(String string) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("token", string);
  }

  static setAuthToken(String string) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("authToken", string);
  }
  static Future<String> getAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString("authToken");
    if (string == null || string.isEmpty) {
      string = "";
    }
    return string;
  }
  static Future<String> getJwtToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString("token");
    if (string == null || string.isEmpty) {
      string = "JWT_Token";
    }
    return string;
  }

  static clearAllUserDefaults() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("isLogin", false);
    await prefs.remove("user");
    await prefs.remove("userId");
    await prefs.remove("token");
    await prefs.remove("authToken");
    await FlutterKeychain.remove(key: "username");
    await FlutterKeychain.remove(key: "password");
  }
}
