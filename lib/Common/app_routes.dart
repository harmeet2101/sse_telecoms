import 'package:RouteProve/Screen/dashboard/home_screen.dart';
import 'package:RouteProve/Screen/login_scr.dart';
import 'package:RouteProve/Screen/splash_src.dart';
import 'package:flutter/material.dart';

const Login_Screen = 'login_screen';
const Home_Screen = 'home_screen';


Map<String, WidgetBuilder> getAppRoute(){
  return {
    '/' : (BuildContext context) => SplashScreen(),
    Login_Screen : (BuildContext context) => LoginScreen(),
    Home_Screen : (BuildContext context) => HomeScreen0(),
  };
}
