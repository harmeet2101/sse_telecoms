import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';


Uint8List encodeMap(Map<String, dynamic> map) {
  String blobText = json.encode(map);
  Uint8List blob = Uint8List.fromList(blobText.codeUnits);
  return blob;
}

Map<String, dynamic> decodeMap(Uint8List blob) {
  String blobText = String.fromCharCodes(blob);
  Map<String, dynamic> convertedMap =
      json.decode(blobText) as Map<String, dynamic>;
  return convertedMap;
}

String reformatDate(DateTime  date) {
var newFormat = DateFormat("dd/MM/yyyy");
var str = newFormat.format(date);
return str;
}

String reformatDate2(DateTime  date) {
  var newFormat = DateFormat("yyyy-MM-dd");
  var str = newFormat.format(date);
  return str;
}

Future<bool> isInternetAvailable() async {
    try {
       final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
     return true;
     } else {
       return false;
     }
    } on SocketException catch (_) {
      return false;
    }
}

