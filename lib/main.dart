import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Screen/splash_src.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: appThemeColor,
        appBarTheme: AppBarTheme(
            color: appBackgroundColor, brightness: Brightness.light),
        fontFamily: 'Nunito',
        backgroundColor: appBackgroundColor,
        scaffoldBackgroundColor: transparentColor,
        applyElevationOverlayColor: false,
      ),
      darkTheme: ThemeData(
        primaryColor: appThemeColor,
        appBarTheme: AppBarTheme(
            color: appBackgroundColor, brightness: Brightness.light),
        fontFamily: 'Nunito',
        backgroundColor: appBackgroundColor,
        scaffoldBackgroundColor: transparentColor,
        applyElevationOverlayColor: false,
      ),
      color: appThemeColor,
      title: "SSE TELECOMS",
      home: SplashScreen(),
    );
  }
}
