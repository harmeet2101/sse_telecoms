import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:flutter/material.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

class MarkBlockageWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  MarkBlockageWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return MarkBlockageWidgetState();
  }
}

class MarkBlockageWidgetState extends State<MarkBlockageWidget> {
  BlockageLocation blockageLocation = BlockageLocation();

  @override
  void initState() {
    super.initState();
    blockageLocation = BlockageLocation.fromJson(widget.item.blockageLocation);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        height: 350,
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Jointbox - B",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: appTextColor,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: blockageLocation.numberOfBlockages == 1
                    ? oneBlockageBody()
                    : doubleBlockageBody(),
              ),
            ),
            Text(
              "Jointbox - A",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: appTextColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  oneBlockageBody() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            "Blockage at\n${(blockageLocation.blockageAtA ?? 0).toInt()}m",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: appThemeColor),
          ),
        ),
        FlutterSlider(
          axis: Axis.vertical,
          values: [blockageLocation.blockageAtA],
          min: 0,
          disabled: false,
          handler: getHandler(),
          rtl: true,
          max: blockageLocation.sectionLength ?? 100,
          rangeSlider: false,
          onDragging: (handlerIndex, lowerValue, upperValue) {
            blockageLocation.blockageAtA = lowerValue;
            widget.didAssignValue(widget.item.key, blockageLocation.toJson());
            setState(() {});
          },
          trackBar: FlutterSliderTrackBar(
            activeTrackBarHeight: 5,
            inactiveTrackBarHeight: 5,
            inactiveTrackBar: BoxDecoration(
              borderRadius: BorderRadius.circular(2.5),
              color: Colors.black12,
              border: Border.all(
                width: 1,
                color: appThemeColor.withOpacity(0.2),
              ),
            ),
            activeTrackBar: BoxDecoration(
              borderRadius: BorderRadius.circular(2.5),
              color: appThemeColor,
            ),
          ),
          tooltip: FlutterSliderTooltip(
            disabled: true,
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "Section Length\n${blockageLocation.sectionLength.toInt()}m",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: appThemeColor),
              ),
            ),
          ],
        ),
      ],
    );
  }

  doubleBlockageBody() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "Blockage at\n${blockageLocation.blockageAtB.toInt()}m",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: appThemeColor),
              ),
            ),
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "Blockage at\n${blockageLocation.blockageAtA.toInt()}m",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: appThemeColor),
              ),
            )
          ],
        ),
        FlutterSlider(
          axis: Axis.vertical,
          values: [
            blockageLocation.blockageAtA,
            blockageLocation.sectionLength - blockageLocation.blockageAtB
          ],
          min: 0,
          disabled: false,
          handler: getHandler(),
          rightHandler: getHandler(),
          max: blockageLocation.sectionLength ?? 100,
          rangeSlider: true,
          rtl: true,
          onDragging: (handlerIndex, lowerValue, upperValue) {
            blockageLocation.blockageAtA = lowerValue;
            blockageLocation.blockageAtB =
                blockageLocation.sectionLength - upperValue;
            widget.didAssignValue(widget.item.key, blockageLocation.toJson());
            setState(() {});
          },
          trackBar: FlutterSliderTrackBar(
            activeTrackBarHeight: 5,
            inactiveTrackBarHeight: 5,
            inactiveTrackBar: BoxDecoration(
              borderRadius: BorderRadius.circular(2.5),
              color: Colors.black12,
              border: Border.all(
                width: 1,
                color: appThemeColor.withOpacity(0.2),
              ),
            ),
            activeTrackBar: BoxDecoration(
              borderRadius: BorderRadius.circular(2.5),
              color: appThemeColor,
            ),
          ),
          tooltip: FlutterSliderTooltip(
            disabled: true,
          ),
        ),
        FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            "Section Length\n${blockageLocation.sectionLength.toInt()}m",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: appThemeColor),
          ),
        ),
      ],
    );
  }

  getHandler() {
    return FlutterSliderHandler(
      decoration: BoxDecoration(),
      child: Material(
        type: MaterialType.circle,
        color: Colors.red,
        elevation: 3,
        child: Container(
          padding: EdgeInsets.all(5),
          child: Icon(
            Icons.close,
            size: 25,
            color: appBackgroundColor,
          ),
        ),
      ),
    );
  }
}
