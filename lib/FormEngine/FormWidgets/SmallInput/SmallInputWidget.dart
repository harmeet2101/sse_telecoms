import 'package:flutter/material.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class SmallInputWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  SmallInputWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return SmallInputWidgetState();
  }
}

class SmallInputWidgetState extends State<SmallInputWidget> {
  TextEditingController textController = TextEditingController(text: "");

  @override
  void initState() {
    super.initState();
    if (widget.value != null) {
      String savedAnswer = widget.value as String;
      textController.text = savedAnswer ?? "";
      setState(() {});
    }
  }

  @override
  void didUpdateWidget(SmallInputWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.value != null) {
      String savedAnswer = widget.value as String;
      textController.text = savedAnswer ?? "";
      setState(() {});
    } else {
      textController.text = "";
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Text(
            widget.item.label,
            textAlign: TextAlign.left,
            style: widget.item.labelTextStyle,
          ),
        ),
        Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: BorderSide(color: appThemeColor,width: 0.5),
          ),
          child: Container(
            height: 50,
            padding: EdgeInsets.only(left: 10, right: 10),
            alignment: Alignment.center,

            child: TextField(
              controller: textController,
              maxLines: 1,
              decoration: InputDecoration(
                fillColor: appBackgroundColor,
                border: InputBorder.none,
              ),
              keyboardType: getTextInputType(),
              style: widget.item.inputTextStyle,
              obscureText: widget.item.dataType == FormFieldDataType.secure,
              onChanged: (text) {
                widget.didAssignValue(widget.item.key, text);
              },
              textInputAction: TextInputAction.done,
              onSubmitted: (text) {},
            ),
          ),
        ),
      ],
    );
  }

  getTextInputType() {
    switch (widget.item.dataType) {
      case FormFieldDataType.text:
        return TextInputType.text;
      case FormFieldDataType.number:
        return TextInputType.numberWithOptions(decimal: false);
      case FormFieldDataType.decimal:
        return TextInputType.numberWithOptions(decimal: true);
      case FormFieldDataType.email:
        return TextInputType.emailAddress;
      case FormFieldDataType.secure:
        return TextInputType.text;
    }
  }
}
