import 'package:RouteProve/Common/app_color.dart';
import 'package:flutter/material.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class CardWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  CardWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return CardWidgetState();
  }
}

class CardWidgetState extends State<CardWidget> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(CardWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Icon sideIcon;
    switch (widget.item.status) {
      case FormControlStatus.active:
        sideIcon = Icon(
          Icons.done,
          color: Colors.green,
          size: 24,
        );
        break;
      case FormControlStatus.normal:
        sideIcon = Icon(
          Icons.arrow_forward_ios,
          color: appSecondaryTextColor,
          size: 20,
        );
        break;
      case FormControlStatus.disabled:
        sideIcon = Icon(
          Icons.cancel,
          color: Colors.red,
          size: 24,
        );
        break;
    }

    return Container(
      height: 80,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 20,
            right: 0,
            bottom: 0,
            child: Card(
              child: FlatButton(
                padding: EdgeInsets.all(0),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 10, 10, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        widget.item.label,
                        style: TextStyle(
                            fontSize: 16,
                            color: appSecondaryTextColor,
                            fontWeight: FontWeight.w600),
                      ),
                      sideIcon
                    ],
                  ),
                ),
                onPressed: () {
                  widget.didAssignValue(widget.item.key, widget.item.tag);
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  color: appThemeColor, borderRadius: BorderRadius.circular(5)),
              clipBehavior: Clip.hardEdge,
              padding: EdgeInsets.all(5),
              child: Image.asset("assets/blockage_locate.png"),
            ),
          )
        ],
      ),
    );
  }
}
