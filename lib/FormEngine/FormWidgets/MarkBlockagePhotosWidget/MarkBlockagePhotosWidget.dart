import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/FormEngine/FormWidgets/MarkBlockagePhotosWidget/BlockagePhotosScr.dart';
import 'package:RouteProve/FormEngine/FormWidgets/MarkBlockagePhotosWidget/JointBoxPhotosScr.dart';
import 'package:flutter/material.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

const PhotoOfArea = "photoOfArea";
const PhotoOfBox = "photoOfBox";
const JointBoxA = "jointBoxA";
const JointBoxB = "jointBoxB";
const BlockageAtA = "blockageAtA";
const BlockageAtB = "blockageAtB";

class MarkBlockagePhotosWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;
  final String formId;

  MarkBlockagePhotosWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue,
      @required this.formId});

  @override
  State<StatefulWidget> createState() {
    return MarkBlockagePhotosWidgetState();
  }
}

class MarkBlockagePhotosWidgetState extends State<MarkBlockagePhotosWidget> {
  BlockageLocation blockageLocation = BlockageLocation();
  Map<String, dynamic> blockagePhotos = Map<String, dynamic>();

  @override
  void initState() {
    super.initState();
    blockageLocation = BlockageLocation.fromJson(widget.item.blockageLocation);
    if (blockageLocation.blockageAtB == 0.0) {
      blockageLocation.blockageAtB = blockageLocation.sectionLength;
      widget.didAssignValue(widget.item.key, blockageLocation.toJson());
      setState(() {});
    }

    if (widget.value != null) {
      Map<String, dynamic> savedAnswer = widget.value;
      if (savedAnswer != null) {
        blockagePhotos = savedAnswer;
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        height: 350,
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Jointbox - B",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: appTextColor,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: blockageLocation.numberOfBlockages == 1
                    ? oneBlockageBody()
                    : doubleBlockageBody(),
              ),
            ),
            Text(
              "Jointbox - A",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: appTextColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  oneBlockageBody() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            "Blockage at\n${(blockageLocation.blockageAtA ?? 0).toInt()}m",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: appThemeColor),
          ),
        ),
        Stack(
          alignment: Alignment.center,
          children: <Widget>[
            FlutterSlider(
              axis: Axis.vertical,
              values: [blockageLocation.sectionLength * 0.5],
              min: 0,
              disabled: true,
              handler: getBlockageAtAHandler(),
              rtl: true,
              max: blockageLocation.sectionLength ?? 100,
              rangeSlider: false,
              trackBar: FlutterSliderTrackBar(
                activeTrackBarHeight: 5,
                inactiveTrackBarHeight: 5,
                inactiveTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.5),
                  color: Colors.black12,
                  border: Border.all(
                    width: 1,
                    color: appThemeColor.withOpacity(0.2),
                  ),
                ),
                activeTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.5),
                  color: appThemeColor,
                ),
              ),
              tooltip: FlutterSliderTooltip(
                disabled: true,
              ),
            ),
            getJointBoxPhotoWidgets(),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "Section Length\n${blockageLocation.sectionLength.toInt()}m",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: appThemeColor),
              ),
            ),
          ],
        ),
      ],
    );
  }

  doubleBlockageBody() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "Blockage at\n${blockageLocation.blockageAtB.toInt()}m",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: appThemeColor),
              ),
            ),
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "Blockage at\n${blockageLocation.blockageAtA.toInt()}m",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: appThemeColor),
              ),
            )
          ],
        ),
        Stack(
          alignment: Alignment.center,
          children: <Widget>[
            FlutterSlider(
              axis: Axis.vertical,
              values: [
                blockageLocation.sectionLength * 0.25,
                blockageLocation.sectionLength * 0.75
              ],
              min: 0,
              disabled: true,
              handler: getBlockageAtBHandler(),
              rightHandler: getBlockageAtAHandler(),
              max: blockageLocation.sectionLength ?? 100,
              rangeSlider: true,
              rtl: true,
              trackBar: FlutterSliderTrackBar(
                activeTrackBarHeight: 5,
                inactiveTrackBarHeight: 5,
                inactiveTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.5),
                  color: Colors.black12,
                  border: Border.all(
                    width: 1,
                    color: appThemeColor.withOpacity(0.2),
                  ),
                ),
                activeTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.5),
                  color: appThemeColor,
                ),
              ),
              tooltip: FlutterSliderTooltip(
                disabled: true,
              ),
            ),
            getJointBoxPhotoWidgets(),
          ],
        ),
        FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            "Section Length\n${blockageLocation.sectionLength.toInt()}m",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                color: appThemeColor),
          ),
        ),
      ],
    );
  }

  getJointBoxPhotoWidgets() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Material(
          type: MaterialType.circle,
          color: validateJointBoxBPhotos() ? Colors.green : Colors.red,
          elevation: 3,
          child: GestureDetector(
            onTap: () {
              getJointBoxAtBPhotos();
            },
            child: Container(
              padding: EdgeInsets.all(5),
              child: Icon(
                Icons.camera_alt,
                size: 25,
                color: appBackgroundColor,
              ),
            ),
          ),
        ),
        Material(
          type: MaterialType.circle,
          color: validateJointBoxAPhotos() ? Colors.green : Colors.red,
          elevation: 3,
          child: GestureDetector(
            onTap: () {
              getJointBoxAtAPhotos();
            },
            child: Container(
              padding: EdgeInsets.all(5),
              child: Icon(
                Icons.camera_alt,
                size: 25,
                color: appBackgroundColor,
              ),
            ),
          ),
        )
      ],
    );
  }

  getJointBoxAtAPhotos() async {
    Map<String, dynamic> photos = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => JointBoxPhotosScr(
                primaryJointBox: "A",
                secondaryJointBox: "B",
                jointBoxPhotos: blockagePhotos[JointBoxA],
                formId: widget.formId,
              ),
          fullscreenDialog: true),
    );
    if (photos != null) {
      blockagePhotos[JointBoxA] = photos;
      widget.didAssignValue(widget.item.key, blockagePhotos);
      setState(() {});
    }
  }

  getJointBoxAtBPhotos() async {
    Map<String, dynamic> photos = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => JointBoxPhotosScr(
                primaryJointBox: "B",
                secondaryJointBox: "A",
                jointBoxPhotos: blockagePhotos[JointBoxB],
                formId: widget.formId,
              ),
          fullscreenDialog: true),
    );
    if (photos != null) {
      blockagePhotos[JointBoxB] = photos;
      widget.didAssignValue(widget.item.key, blockagePhotos);
      setState(() {});
    }
  }

  validateJointBoxAPhotos() {
    if (blockagePhotos[JointBoxA] != null &&
        blockagePhotos[JointBoxA][PhotoOfArea] != null &&
        blockagePhotos[JointBoxA][PhotoOfBox] != null) {
      return true;
    } else {
      return false;
    }
  }

  validateJointBoxBPhotos() {
    if (blockagePhotos[JointBoxB] != null &&
        blockagePhotos[JointBoxB][PhotoOfArea] != null &&
        blockagePhotos[JointBoxB][PhotoOfBox] != null) {
      return true;
    } else {
      return false;
    }
  }

  getBlockageAtAHandler() {
    return FlutterSliderHandler(
      decoration: BoxDecoration(),
      child: Material(
        type: MaterialType.circle,
        color: validateBlockageAtAPhotos() ? Colors.green : Colors.red,
        elevation: 3,
        child: GestureDetector(
          onTap: () {
            getBlockageAtAPhotos();
          },
          child: Container(
            padding: EdgeInsets.all(5),
            child: Image.asset(
              "assets/blockage_image_icon.png",
              height: 25,
              width: 25,
              color: appBackgroundColor,
            ),
          ),
        ),
      ),
    );
  }

  getBlockageAtAPhotos() async {
    List<Map<String, dynamic>> photos = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => BlockagePhotosScr(
                blockagePhotos: blockagePhotos[BlockageAtA],
                formId: widget.formId,
              ),
          fullscreenDialog: true),
    );
    if (photos != null) {
      blockagePhotos[BlockageAtA] = photos;
      widget.didAssignValue(widget.item.key, blockagePhotos);
      setState(() {});
    }
  }

  validateBlockageAtAPhotos() {
    if (blockagePhotos[BlockageAtA] != null &&
        blockagePhotos[BlockageAtA].length > 0) {
      return true;
    } else {
      return false;
    }
  }

  getBlockageAtBHandler() {
    return FlutterSliderHandler(
      decoration: BoxDecoration(),
      child: Material(
        type: MaterialType.circle,
        color: validateBlockageAtBPhotos() ? Colors.green : Colors.red,
        elevation: 3,
        child: GestureDetector(
          onTap: () {
            getBlockageAtBPhotos();
          },
          child: Container(
            padding: EdgeInsets.all(5),
            child: Image.asset(
              "assets/blockage_image_icon.png",
              height: 25,
              width: 25,
              color: appBackgroundColor,
            ),
          ),
        ),
      ),
    );
  }

  getBlockageAtBPhotos() async {
    List<Map<String, dynamic>> photos = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => BlockagePhotosScr(
                blockagePhotos: blockagePhotos[BlockageAtB],
                formId: widget.formId,
              ),
          fullscreenDialog: true),
    );
    if (photos != null) {
      blockagePhotos[BlockageAtB] = photos;
      widget.didAssignValue(widget.item.key, blockagePhotos);
      setState(() {});
    }
  }

  validateBlockageAtBPhotos() {
    if (blockagePhotos[BlockageAtB] != null &&
        blockagePhotos[BlockageAtB].length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
