import 'dart:io';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/helper.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/FormEngine/FormWidgets/LocationWidget/CustomLocationWidget.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:RouteProve/Widgets/appBarWidget.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:RouteProve/Widgets/progressHud.dart';

class BlockagePhotosScr extends StatefulWidget {
  final List<dynamic> blockagePhotos;
  final String formId;

  BlockagePhotosScr({this.blockagePhotos, @required this.formId});

  @override
  State<StatefulWidget> createState() {
    return BlockagePhotosScrState();
  }
}

class BlockagePhotosScrState extends State<BlockagePhotosScr> {
  double screenWidth;
  double screenHeight;
  List<Map<String, dynamic>> blockagePhotos = List<Map<String, dynamic>>();
  final ImagePicker _picker = ImagePicker();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    resetValues();
  }

  resetValues() {
    if (widget.blockagePhotos != null) {
      blockagePhotos.clear();
      List<dynamic> savedAnswer = widget.blockagePhotos;
      if (savedAnswer.length > 0) {
        for (int i = 0; i < savedAnswer.length; i++) {
          Map<String, dynamic> image = savedAnswer[i];
          if (image != null) {
            blockagePhotos.add(image);
          }
        }
      }
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

    return getScaffold(
      context,
      ProgressHUD(
        inAsyncCall: isLoading,
        child: Scaffold(
          appBar: getActionbar(
              context,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    width: screenWidth - 140,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        "Images for Blockage",
                        style: TextStyle(
                            color: appBackgroundColor,
                            fontSize: 18,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(width: 60)
                ],
              ),
              image: "assets/mark_blockage.png"),
          body: SafeArea(
            child: Column(
              children: <Widget>[
                CustomLocationWidget(getLocation),
                Expanded(
                  child: buildBody(),
                ),
                buildBottomButton()
              ],
            ),
          ),
        ),
      ),
    );
  }

  buildBody() {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
      child: GridView.count(
        scrollDirection: Axis.vertical,
        crossAxisCount: 2,
        mainAxisSpacing: 15,
        crossAxisSpacing: 15,
        childAspectRatio: 1,
        children: buildImageCards(),
      ),
    );
  }

  buildImageCards() {
    List<Widget> list = List<Widget>();
    for (var i = 0; i < blockagePhotos.length; i++) {
      list.add(getImageCard(i));
    }
    if (blockagePhotos.length < 4) {
      list.add(addImageCard());
    }

    return list;
  }

  Widget getImageCard(int index) {
    return Stack(
      children: <Widget>[
        Card(
          elevation: 1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          color: Colors.white,
          clipBehavior: Clip.hardEdge,
          child: Image.file(
            File("${FileHandler.localPath}/${blockagePhotos[index]["path"]}"),
            width: (screenWidth - 60) * 0.5,
            height: (screenWidth - 60) * 0.5,
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          top: -5,
          right: -5,
          child: GestureDetector(
            onTap: () {
              removeImage(index);
            },
            child: Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    color: appBackgroundColor,
                    borderRadius: BorderRadius.circular(12)),
                alignment: Alignment.center,
                child: Icon(
                  Icons.cancel,
                  color: Colors.red,
                )),
          ),
        ),
      ],
    );
  }

  Widget addImageCard() {
    return GestureDetector(
      onTap: () {
        showPickerOptions(context);
      },
      child: Card(
        elevation: 1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        color: Colors.white,
        clipBehavior: Clip.hardEdge,
        child: Container(
          width: (screenWidth - 60) * 0.5,
          height: (screenWidth - 60) * 0.5,
          alignment: Alignment.center,
          child: Icon(
            Icons.add_a_photo,
            size: 50,
            color: appSecondaryColor,
          ),
        ),
      ),
    );
  }

  buildBottomButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
      child: Container(
        /*height: 40,
        width: screenWidth / 2 - 15,*/
        child:Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: RaisedButton(
                elevation: 5,
                color: appThemeColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                ),
                child: RichText(
                    text: TextSpan(
                        text: "Back",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: appBackgroundColor))),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            SizedBox(width: 20,),
            Expanded(
              child: RaisedButton(
                elevation: 5,
                color: appSecondaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                ),
                child: RichText(
                    text: TextSpan(
                        text: "Save",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: appBackgroundColor))),
                onPressed: () {
                  saveButtonAction();
                },
              ),
            ),
          ],
        ) /*RaisedButton(
          elevation: 5,
          color: appThemeColor,
          textColor: appBackgroundColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
          ),
          child: RichText(
              text: TextSpan(
                  text: "SAVE",
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: appBackgroundColor))),
          onPressed: () {
            saveButtonAction();
          },
        )*/,
      ),
    );
  }

  saveButtonAction() {
    if (blockagePhotos.length > 0) {
      Navigator.of(context).pop(blockagePhotos);
    } else {
      showAlertMessage(
          message: "Please take at least 1 mandatory image.",
          type: AlertType.Failure,
          context: context);
    }
  }

  showPickerOptions(context) {
    showModalBottomSheet(
        context: context,
        elevation: 8,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(
                        Icons.camera,
                        color: appThemeColor,
                      ),
                      title: new Text(
                        'Take Picture',
                        style: TextStyle(fontSize: 14),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        getImageFromCamera();
                      }),
                  new ListTile(
                    leading: new Icon(
                      Icons.image,
                      color: appThemeColor,
                    ),
                    title: new Text(
                      'Upload Image',
                      style: TextStyle(fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      getImageFromGallery();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future getImageFromCamera() async {
    var image = await _picker.getImage(
        source: ImageSource.camera,
        imageQuality: 50,
        maxHeight: 500.0,
        maxWidth: 500.0);
    if (image != null) {
      saveImage(image);
    }
  }

  Future getImageFromGallery() async {
    var image = await _picker.getImage(
        source: ImageSource.gallery,
        imageQuality: 50,
        maxHeight: 500.0,
        maxWidth: 500.0);
    if (image != null) {
      saveImage(image);
    }
  }

  void removeImage(int index) {
    blockagePhotos.removeAt(index);
    setState(() {});
  }

  saveImage(PickedFile file) async {
    updateLoadingStatus(true);
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    String dateTime = DateTime.now().toIso8601String();
    debugPrint(file.path);
    String filePath = await FileHandler.saveFile(file.path, widget.formId);
    debugPrint(filePath);
    blockagePhotos.add({
      "path": filePath,
      "lat": position.latitude ?? 0.0,
      "lon": position.longitude ?? 0.0,
      "dateTime": dateTime,
      "address":address,
    });
    updateLoadingStatus(false);
  }

  updateLoadingStatus(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }

  String address = 'NA';

  Function getLocation(String address){
    this.address = address;
  }
}
