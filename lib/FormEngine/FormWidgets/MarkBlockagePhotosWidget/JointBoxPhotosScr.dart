import 'dart:io';

import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/helper.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/FormEngine/FormWidgets/LocationWidget/CustomLocationWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/MarkBlockagePhotosWidget/MarkBlockagePhotosWidget.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:RouteProve/Widgets/appBarWidget.dart';
import 'package:RouteProve/Widgets/progressHud.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';

class JointBoxPhotosScr extends StatefulWidget {
  final String primaryJointBox;
  final String secondaryJointBox;
  final Map<String, dynamic> jointBoxPhotos;
  final String formId;

  JointBoxPhotosScr(
      {@required this.primaryJointBox,
      @required this.secondaryJointBox,
      this.jointBoxPhotos,
      @required this.formId});

  @override
  State<StatefulWidget> createState() {
    return JointBoxPhotosScrState();
  }
}

class JointBoxPhotosScrState extends State<JointBoxPhotosScr> {
  double screenWidth;
  double screenHeight;
  Map<String, dynamic> jointBoxPhotos;
  final ImagePicker _picker = ImagePicker();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    if (widget.jointBoxPhotos != null) {
      jointBoxPhotos = widget.jointBoxPhotos;
    } else {
      jointBoxPhotos = Map<String, dynamic>();
    }
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

    return getScaffold(
      context,
      ProgressHUD(
        inAsyncCall: isLoading,
        child: Scaffold(
          appBar: getActionbar(
              context,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    width: screenWidth - 140,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        "Images for Jointbox - ${widget.primaryJointBox}",
                        style: TextStyle(
                            color: appBackgroundColor,
                            fontSize: 18,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                  SizedBox(width: 60)
                ],
              ),
              image: "assets/mark_blockage.png"),
          body: SafeArea(
            child: LayoutBuilder(builder:
                (BuildContext context, BoxConstraints viewportConstraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: viewportConstraints.maxHeight,
                  ),
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        CustomLocationWidget(getLocation),
                        buildBody(),
                        Spacer(),
                        buildBottomButton()
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        ),
      ),
    );
  }

  buildBody() {
    return Padding(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            getImageCard(PhotoOfArea),
            getImageCard(PhotoOfBox),
          ],
        ));
  }

  Widget getImageCard(String type) {
    return Stack(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            showPickerOptions(context, type);
          },
          child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              clipBehavior: Clip.hardEdge,
              child: Container(
                width: (screenWidth - 60) * 0.5,
                child: Column(
                  children: <Widget>[
                    Container(
                      width: (screenWidth - 60) * 0.5,
                      height: (screenWidth - 60) * 0.5,
                      child: getCardImage(type),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: Text(
                        type == PhotoOfArea
                            ? "General Area of Box ${widget.primaryJointBox} and route up towards Box ${widget.secondaryJointBox}"
                            : "Photo of ${widget.primaryJointBox}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: appSecondaryTextColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              )),
        ),
        jointBoxPhotos[type] != null
            ? Positioned(
                top: -5,
                right: -5,
                child: GestureDetector(
                  onTap: () {
                    removeImage(type);
                  },
                  child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                          color: appBackgroundColor,
                          borderRadius: BorderRadius.circular(12)),
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.cancel,
                        color: Colors.red,
                      )),
                ),
              )
            : SizedBox(),
      ],
    );
  }

  Widget getCardImage(String type) {
    if (jointBoxPhotos[type] != null) {
      return Image.file(
        File("${FileHandler.localPath}/${jointBoxPhotos[type]["path"]}"),
        width: (screenWidth - 60) * 0.5,
        height: (screenWidth - 60) * 0.5,
        fit: BoxFit.cover,
      );
    } else {
      return Icon(
        Icons.add_a_photo,
        size: (screenWidth - 60) * 0.25,
        color: appSecondaryColor,
      );
    }
  }

  buildBottomButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: RaisedButton(
                elevation: 5,
                color: appThemeColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                ),
                child: RichText(
                    text: TextSpan(
                        text: "Back",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: appBackgroundColor))),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            SizedBox(width: 20,),
            Expanded(
              child: RaisedButton(
                elevation: 5,
                color: appSecondaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                ),
                child: RichText(
                    text: TextSpan(
                        text: "Save",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: appBackgroundColor))),
                onPressed: () {
                  saveButtonAction();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  saveButtonAction() {
    if (jointBoxPhotos[PhotoOfArea] == null) {
      showAlertMessage(
          message:
              "Please insert a photo of Area of Box ${widget.primaryJointBox}.",
          type: AlertType.Failure,
          context: context);
    } else if (jointBoxPhotos[PhotoOfBox] == null) {
      showAlertMessage(
          message: "Please insert a photo of Box ${widget.primaryJointBox}.",
          type: AlertType.Failure,
          context: context);
    } else {
      Navigator.of(context).pop(jointBoxPhotos);
    }
  }

  showPickerOptions(context, String type) {
    showModalBottomSheet(
        context: context,
        elevation: 8,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(
                        Icons.camera,
                        color: appThemeColor,
                      ),
                      title: new Text(
                        'Take Picture',
                        style: TextStyle(fontSize: 14),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        getImageFromCamera(type);
                      }),
                  new ListTile(
                    leading: new Icon(
                      Icons.image,
                      color: appThemeColor,
                    ),
                    title: new Text(
                      'Upload Image',
                      style: TextStyle(fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      getImageFromGallery(type);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future getImageFromCamera(String type) async {
    var image = await _picker.getImage(
        source: ImageSource.camera,
        imageQuality: 50,
        maxHeight: 500.0,
        maxWidth: 500.0);
    if (image != null) {
      saveImage(image, type);
    }
  }

  Future getImageFromGallery(String type) async {
    var image = await _picker.getImage(
        source: ImageSource.gallery,
        imageQuality: 50,
        maxHeight: 500.0,
        maxWidth: 500.0);
    if (image != null) {
      saveImage(image, type);
    }
  }

  void removeImage(String type) {
    jointBoxPhotos.remove(type);
    setState(() {});
  }

  saveImage(PickedFile file, String type) async {
    updateLoadingStatus(true);
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    String dateTime = DateTime.now().toIso8601String();
    debugPrint(file.path);
    String filePath = await FileHandler.saveFile(file.path, widget.formId);
    debugPrint(filePath);
    jointBoxPhotos[type] = {
      "path": filePath,
      "lat": position.latitude ?? 0.0,
      "lon": position.longitude ?? 0.0,
      "dateTime": dateTime,
      "address":address,
    };

    updateLoadingStatus(false);
  }

  updateLoadingStatus(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }

  String address = 'NA';

  Function getLocation(String address){
    this.address = address;
  }
}
