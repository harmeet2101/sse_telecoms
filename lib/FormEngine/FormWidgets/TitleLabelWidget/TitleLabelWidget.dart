import 'package:flutter/material.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class TitleLabelWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  TitleLabelWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return TitleLabelWidgetState();
  }
}

class TitleLabelWidgetState extends State<TitleLabelWidget> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.item.label,
      textAlign: TextAlign.center,
      style: widget.item.titleTextStyle,
    );
  }
}
