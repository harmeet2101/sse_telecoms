import 'package:flutter/material.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class SwitchWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  SwitchWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return SwitchWidgetState();
  }
}

class SwitchWidgetState extends State<SwitchWidget> {
  bool isSwitched = false;

  @override
  void initState() {
    super.initState();
    if (widget.value != null) {
      bool savedAnswer = widget.value as bool;
      isSwitched = savedAnswer ?? false;
      setState(() {});
    } else {
      widget.didAssignValue(widget.item.key, isSwitched);
    }
  }

  @override
  void didUpdateWidget(SwitchWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.value != null) {
      bool savedAnswer = widget.value as bool;
      isSwitched = savedAnswer ?? false;
      setState(() {});
    } else {
      isSwitched = false;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        height: 50,
        padding: EdgeInsets.only(left: 10, right: 10),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              widget.item.label,
              textAlign: TextAlign.left,
            ),
            SizedBox(width: 20),
            Switch(
              value: isSwitched,
              onChanged: (value) {
                setState(() {
                  isSwitched = value;
                  widget.didAssignValue(widget.item.key, isSwitched);
                });
              },
              activeTrackColor: Colors.green,
              activeColor: appBackgroundColor,
            )
          ],
        ),
      ),
    );
  }
}
