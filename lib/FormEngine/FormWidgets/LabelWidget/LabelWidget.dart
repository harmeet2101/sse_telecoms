import 'package:flutter/material.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class LabelWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  LabelWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return LabelWidgetState();
  }
}

class LabelWidgetState extends State<LabelWidget> {

  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.item.label,
      textAlign: TextAlign.center,
      style: widget.item.labelTextStyle,
    );
  }
}
