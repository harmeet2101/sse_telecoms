import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class JointboxAWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  JointboxAWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return JointboxAWidgetState();
  }
}

class JointboxAWidgetState extends State<JointboxAWidget> {
  double screenWidth;
  double screenHeight;
  String pickedValue = "";
  TextEditingController textController = TextEditingController(text: "");
  List<String> ducts = ["1", "2", "3", "4", "6", "8", "9", "12", "16"];
  Map<String, Map<String, List<String>>> ductAndBores = {
    "1": {
      "A": ["1"]
    },
    "2": {
      "A": ["1", "2"],
      "B": ["1", "2"]
    },
    "3": {
      "A": ["1", "2", "3"],
      "B": ["1", "2", "3"],
      "C": ["1", "2", "3"],
      "D": ["1", "2", "3"],
      "E": ["1", "2", "3"],
    },
    "4": {
      "A": ["1", "2", "3", "4"],
    },
    "6": {
      "A": ["1", "2", "3", "4", "5", "6"],
      "B": ["1", "2", "3", "4", "5", "6"],
    },
    "8": {
      "A": ["1", "2", "3", "4", "5", "6", "7", "8"],
      "B": ["1", "2", "3", "4", "5", "6", "7", "8"],
      "C": ["1", "2", "3", "4", "5", "6", "7", "8"],
    },
    "9": {
      "A": ["1", "2", "3", "4", "5", "6", "7", "8", "9"],
    },
    "12": {
      "A": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
      "B": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
    },
    "16": {
      "A": [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16"
      ],
    },
  };

  @override
  void initState() {
    super.initState();
   if (widget.value != null) {
      String savedAnswer = widget.value as String;
      pickedValue = savedAnswer ?? "";
      textController.text = savedAnswer.split("-").first ?? "";
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Text(
            widget.item.label,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: appSecondaryColor,
                fontSize: 16,
                fontWeight: FontWeight.w500),
          ),
        ),
        Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            height: 50,
            padding: EdgeInsets.only(left: 10),
            alignment: Alignment.center,
            child: TextField(
              controller: textController,
              maxLines: 1,
              decoration: InputDecoration(
                fillColor: appBackgroundColor,
                border: InputBorder.none,
                hintText: "Select ${widget.item.label}",
                suffixIcon: Icon(
                  Icons.arrow_drop_down,
                  color: appSecondaryColor,
                  size: 24,
                ),
              ),
              style: widget.item.inputTextStyle,
              readOnly: true,
              onTap: () {
                showModalBottomSheet(
                    elevation: 8,
                    context: context,
                    builder: (BuildContext builder) {
                      return buildDuctPicker(builder);
                    });
              },
            ),
          ),
        ),
        buildDuctsGrid(),
      ],
    );
  }

  Widget buildDuctsGrid() {
    if (textController.text != "") {
      List<String> boreTypes = List<String>();
      ductAndBores[textController.text].forEach((key, value) {
        boreTypes.add("${textController.text}-$key");
      });

      if (boreTypes.length == 1) {
        String choosenAnswer = pickedValue != "" ? pickedValue : boreTypes[0];
        return Padding(
          padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
          child: GestureDetector(
            onTap: () {
              showModalBottomSheet(
                  elevation: 8,
                  context: context,
                  builder: (BuildContext builder) {
                    return buildBorePicker(builder, boreTypes[0]);
                  });
            },
            child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: <Widget>[
                  Text(
                    choosenAnswer,
                    style: TextStyle(
                        color: appSecondaryColor,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(height: 5),
                  Image.asset(
                    "assets/patterns/${choosenAnswer.replaceAll("-", "_")}.png",
                    width: (screenWidth) * 0.4,
                    height: (screenWidth) * 0.4,
                  ),
                ],
              ),
            ),
          ),
        );
      } else {
        double heightOfTile = ((screenWidth - 60) * 0.5) + 45;
        double widthOfTile = (screenWidth - 60) * 0.5;
        double heightOfContainer = boreTypes.length % 2 == 1
            ? ((boreTypes.length + 1) / 2) * (heightOfTile + 15)
            : (boreTypes.length / 2) * (heightOfTile + 15);
        double aspectRatio = widthOfTile / heightOfTile;
        return Padding(
          padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
          child: Container(
            height: heightOfContainer,
            child: GridView.count(
              scrollDirection: Axis.vertical,
              crossAxisCount: 2,
              mainAxisSpacing: 15,
              crossAxisSpacing: 15,
              childAspectRatio: aspectRatio,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: new List<Widget>.generate(
                boreTypes.length,
                (int index) {
                  String choosenAnswer = boreTypes[index];
                  if (pickedValue != "" &&
                      pickedValue.contains(choosenAnswer)) {
                    choosenAnswer = pickedValue;
                  }
                  return GestureDetector(
                    onTap: () {
                      showModalBottomSheet(
                          elevation: 8,
                          context: context,
                          builder: (BuildContext builder) {
                            return buildBorePicker(builder, boreTypes[index]);
                          });
                    },
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        children: <Widget>[
                          Text(
                            choosenAnswer,
                            style: TextStyle(
                                color: appSecondaryColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(height: 5),
                          Image.asset(
                            "assets/patterns/${choosenAnswer.replaceAll("-", "_")}.png",
                            width: widthOfTile,
                            height: widthOfTile,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        );
      }
    } else {
      return SizedBox();
    }
  }

  Widget buildDuctPicker(BuildContext builder) {
    int selectedValue = 0;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 50,
          color: appBackgroundColor,
          alignment: Alignment.centerRight,
          child: Container(
            margin: EdgeInsets.only(right: 10),
            height: 30,
            width: 100,
            child: RaisedButton(
              onPressed: () {
                setState(() {
                  pickedValue = "";
                  widget.didAssignValue(widget.item.key, pickedValue);
                  textController.text = ducts[selectedValue];
                });
                Navigator.pop(context);
              },
              color: appThemeColor,
              child: Text(
                "DONE",
                style: TextStyle(
                    color: appBackgroundColor,
                    fontSize: 14,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ),
        Container(
          height: 200,
          child: CupertinoPicker(
            useMagnifier: true,
            magnification: 1.2,
            itemExtent: 40,
            backgroundColor: Colors.grey[100],
            onSelectedItemChanged: (int index) {
              selectedValue = index;
            },
            children: new List<Widget>.generate(ducts.length, (int index) {
              return new Center(
                child: new Text(
                  ducts[index],
                  style: TextStyle(
                    color: appThemeColor,
                  ),
                ),
              );
            }),
          ),
        ),
      ],
    );
  }

  Widget buildBorePicker(BuildContext builder, String choosedValue) {
    int selectedValue = 0;
    List<String> boreTypes =
        ductAndBores[textController.text][choosedValue.split("-").last];
    List<String> borePatterns = List<String>();
    for (int i = 0; i < boreTypes.length; i++) {
      borePatterns.add("$choosedValue-${boreTypes[i]}");
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 50,
          color: appBackgroundColor,
          alignment: Alignment.centerRight,
          child: Container(
            margin: EdgeInsets.only(right: 10),
            height: 30,
            width: 100,
            child: RaisedButton(
              onPressed: () {
                setState(() {
                  pickedValue = borePatterns[selectedValue];
                  widget.didAssignValue(widget.item.key, pickedValue);
                });
                Navigator.pop(context);
              },
              color: appThemeColor,
              child: Text(
                "DONE",
                style: TextStyle(
                    color: appBackgroundColor,
                    fontSize: 14,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ),
        Container(
          height: 200,
          child: CupertinoPicker(
            useMagnifier: true,
            magnification: 1.2,
            itemExtent: 40,
            backgroundColor: Colors.grey[100],
            onSelectedItemChanged: (int index) {
              selectedValue = index;
            },
            children:
                new List<Widget>.generate(borePatterns.length, (int index) {
              return new Center(
                child: new Text(
                  borePatterns[index],
                  style: TextStyle(
                    color: appThemeColor,
                  ),
                ),
              );
            }),
          ),
        ),
      ],
    );
  }
  
}
