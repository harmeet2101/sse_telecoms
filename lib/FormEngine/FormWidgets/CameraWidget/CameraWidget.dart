import 'dart:io';
import 'package:RouteProve/Common/helper.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class CameraWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;
  final String formId;

  CameraWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue,
      @required this.formId});

  @override
  State<StatefulWidget> createState() {
    return CameraWidgetState();
  }
}

class CameraWidgetState extends State<CameraWidget> {
  List<Map<String, dynamic>> photos = List<Map<String, dynamic>>();
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    if (widget.value != null) {
      resetValues();
    }
  }

  @override
  void didUpdateWidget(CameraWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.value != null) {
      resetValues();
    } else {
      photos.clear();
      setState(() {});
    }
  }

  resetValues() {
    photos.clear();
    List<dynamic> savedAnswer = widget.value;
    if (savedAnswer.length > 0) {
      for (int i = 0; i < savedAnswer.length; i++) {
        Map<String, dynamic> image = savedAnswer[i];
        if (image != null) {
          photos.add(image);
        }
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      clipBehavior: Clip.hardEdge,
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            Text(
              widget.item.label,
              textAlign: TextAlign.center,
              style: widget.item.labelTextStyle,
            ),
            Container(
              height: 110,
              margin: EdgeInsets.only(top: 8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.black12),
              child: GridView.count(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.all(5),
                crossAxisCount: 1,
                mainAxisSpacing: 4,
                childAspectRatio: 1,
                children: buildImageCards(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildImageCards() {
    List<Widget> list = List<Widget>();
    if (photos.length < widget.item.maxRequired) {
      list.add(addImageCard());
    }
    for (var i = photos.length - 1; i >= 0; i--) {
      list.add(getImageCard(i));
    }

    return list;
  }

  Widget getImageCard(int index) {
    return Stack(
      children: <Widget>[
        Card(
          elevation: 1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          color: Colors.white,
          clipBehavior: Clip.hardEdge,
          child: Image.file(
            File("${FileHandler.localPath}/${photos[index]["path"]}"),
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: GestureDetector(
            onTap: () {
              removeImage(index);
            },
            child: Container(
                height: 24,
                width: 24,
                decoration: BoxDecoration(
                    color: appBackgroundColor,
                    borderRadius: BorderRadius.circular(12)),
                alignment: Alignment.center,
                child: Icon(
                  Icons.cancel,
                  color: Colors.red,
                )),
          ),
        ),
      ],
    );
  }

  Widget addImageCard() {
    return GestureDetector(
      onTap: () {
        showPickerOptions(context);
      },
      child: Card(
        elevation: 1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        color: Colors.white,
        clipBehavior: Clip.hardEdge,
        child: Container(
          height: 100,
          width: 100,
          alignment: Alignment.center,
          child: Icon(
            Icons.add_a_photo,
            size: 50,
            color: appThemeColor,
          ),
        ),
      ),
    );
  }

  showPickerOptions(context) {
    showModalBottomSheet(
        context: context,
        elevation: 8,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(
                        Icons.camera,
                        color: appThemeColor,
                      ),
                      title: new Text(
                        'Take Picture',
                        style: TextStyle(fontSize: 14),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                        getImageFromCamera();
                      }),
                  new ListTile(
                    leading: new Icon(
                      Icons.image,
                      color: appThemeColor,
                    ),
                    title: new Text(
                      'Upload Image',
                      style: TextStyle(fontSize: 14),
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      getImageFromGallery();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future getImageFromCamera() async {
    var image = await _picker.getImage(
        source: ImageSource.camera,
        imageQuality: 50,
        maxHeight: 500.0,
        maxWidth: 500.0);
    if (image != null) {
      saveImage(image);
    }
  }

  Future getImageFromGallery() async {
    var image = await _picker.getImage(
        source: ImageSource.gallery,
        imageQuality: 50,
        maxHeight: 500.0,
        maxWidth: 500.0);
    if (image != null) {
      saveImage(image);
    }
  }

  void removeImage(int index) {
    photos.removeAt(index);
    widget.didAssignValue(widget.item.key, photos);
    setState(() {});
  }

  saveImage(PickedFile file) async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    String dateTime = DateTime.now().toIso8601String();
    debugPrint(file.path);
    String filePath = await FileHandler.saveFile(file.path, widget.formId);
    debugPrint(filePath);
    photos.add({
      "path": filePath,
      "lat": position.latitude ?? 0.0,
      "lon": position.longitude ?? 0.0,
      "dateTime": dateTime
    });
    widget.didAssignValue(widget.item.key, photos);
    setState(() {});
  }
}
