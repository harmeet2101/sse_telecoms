import 'package:flutter/material.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/FormEngine/FormWidgets/SingleChoiceWidget/SingleChoiceControlScr.dart';

class SingleChoiceWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  SingleChoiceWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return SingleChoiceWidgetState();
  }
}

class SingleChoiceWidgetState extends State<SingleChoiceWidget> {
  TextEditingController textController = TextEditingController(text: "");

  @override
  void initState() {
    super.initState();
    if (widget.value != null) {
      String savedAnswer = widget.value as String;
      textController.text = savedAnswer.split("_").first ?? "";
      setState(() {});
    }
  }

  @override
  void didUpdateWidget(SingleChoiceWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.value != null) {
      String savedAnswer = widget.value as String;
      textController.text = savedAnswer.split("_").first ?? "";
      setState(() {});
    } else {
      textController.text = "";
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Text(
            widget.item.label,
            textAlign: TextAlign.left,
            style: widget.item.labelTextStyle,
          ),
        ),
        Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            height: 50,
            padding: EdgeInsets.only(left: 10),
            alignment: Alignment.center,
            child: TextField(
              controller: textController,
              maxLines: 1,
              decoration: InputDecoration(
                fillColor: appBackgroundColor,
                border: InputBorder.none,
                hintText: "Select ${widget.item.label}",
                suffixIcon: Icon(
                  Icons.arrow_drop_down,
                  color: appSecondaryColor,
                  size: 24,
                ),
              ),
              style: widget.item.inputTextStyle,
              readOnly: true,
              onTap: () {
                getChoiceItem();
              },
            ),
          ),
        ),
      ],
    );
  }

  getChoiceItem() async {
    ChoiceItem item = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SingleChoiceControlScreen(
                formItem: widget.item,
              ),
          fullscreenDialog: true),
    );
    if (item != null) {
      textController.text = item.value;
      widget.didAssignValue(widget.item.key, "${item.value}_${item.key}");
    }
  }
}
