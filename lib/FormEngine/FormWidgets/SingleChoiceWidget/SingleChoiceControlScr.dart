import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/material.dart';

class SingleChoiceControlScreen extends StatefulWidget {
  final FormItem formItem;
  SingleChoiceControlScreen({@required this.formItem});

  @override
  SingleChoiceControlScreenState createState() =>
      SingleChoiceControlScreenState();
}

class SingleChoiceControlScreenState extends State<SingleChoiceControlScreen> {
  double screenWidth = 0;
  double screenHeight = 0;
  bool isSearching = false;
  List<ChoiceItem> choiceItems = List<ChoiceItem>();
  List<ChoiceItem> searchChoiceItems = List<ChoiceItem>();

  @override
  void initState() {
    super.initState();
    choiceItems = widget.formItem.choiceItems ?? List<ChoiceItem>();
    searchChoiceItems = widget.formItem.choiceItems ?? List<ChoiceItem>();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

    return getScaffold(
      context,
      getScreenLayout(),
    );
  }

  Widget getScreenLayout() {
    return Scaffold(
      appBar: getAppBar(),
      body: SafeArea(
        child: ListView.separated(
          itemCount:
              isSearching ? searchChoiceItems.length : choiceItems.length,
          itemBuilder: (BuildContext context, int index) {
            return getChoiceItem(index);
          },
          separatorBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Divider(height: 1),
            );
          },
        ),
      ),
    );
  }

  Widget getChoiceItem(int index) {
    ChoiceItem item =
        isSearching ? searchChoiceItems[index] : choiceItems[index];

    return FlatButton(
      color: appBackgroundColor,
      padding: EdgeInsets.all(20),
      onPressed: () {
        Navigator.of(context).pop(item);
      },
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              item.value,
              style: TextStyle(
                color: appSecondaryTextColor,
                fontSize: 16,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Icon(
            Icons.arrow_forward_ios,
            color: appSecondaryColor,
            size: 20,
          )
        ],
      ),
    );
  }

  Widget getAppBar() {
    return AppBar(
      backgroundColor: appThemeColor,
      titleSpacing: 0,
      title: isSearching
          ? Container(
              height: 40,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
              alignment: Alignment.centerLeft,
              child: TextField(
                cursorColor: appThemeColor,
                decoration: InputDecoration(
                  icon: Icon(Icons.search),
                  border: InputBorder.none,
                  hintText: "Search",
                  focusColor: appTextColor,
                ),
                autocorrect: false,
                textInputAction: TextInputAction.search,
                onChanged: (text) {
                  setState(() {
                    searchChoiceItems = widget.formItem.choiceItems
                        .where((item) => item.value
                            .toLowerCase()
                            .contains(text.toLowerCase()))
                        .toList();
                  });
                },
              ),
            )
          : FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "Select ${widget.formItem.label}",
                style: TextStyle(color: appBackgroundColor, fontSize: 16),
              ),
            ),
      actions: isSearching
          ? <Widget>[
              IconButton(
                focusColor: Colors.transparent,
                hoverColor: Colors.transparent,
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                icon: Icon(Icons.cancel),
                onPressed: () {
                  setState(() {
                    isSearching = false;
                  });
                },
              )
            ]
          : <Widget>[
              IconButton(
                focusColor: Colors.transparent,
                hoverColor: Colors.transparent,
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                icon: Icon(Icons.search),
                onPressed: () {
                  setState(() {
                    searchChoiceItems = widget.formItem.choiceItems;
                    isSearching = true;
                  });
                },
              )
            ],
    );
  }
}
