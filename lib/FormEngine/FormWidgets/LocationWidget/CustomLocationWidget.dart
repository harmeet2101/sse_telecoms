import 'package:RouteProve/Common/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

typedef void addressCallback(String address);

class CustomLocationWidget extends StatefulWidget{

  final addressCallback callback;


  CustomLocationWidget(this.callback);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CustomLocationState();
  }
}

class CustomLocationState extends State<CustomLocationWidget>{

  Map<String, dynamic> location = {"address": "", "lat": 0.0, "lon": 0.0};
  TextEditingController textController = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: Container(

        child: FutureBuilder<Map<String, dynamic>>(builder:(BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot){

          if(snapshot.hasData){

            return postLoadingWidget();

          }else{
            return preLoadingWidget();
          }

        },future: locateUser(),),
        height: 80,
        decoration: BoxDecoration(
          color: appBackgroundColor,
          shape: BoxShape.rectangle,
          border: Border.all(color: appThemeColor,width: 0.7),
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
    );
  }

  Widget preLoadingWidget(){
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            child: Icon(Icons.edit,color: appSecondaryColor),
            onTap: (){

            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Fetching location...',style: TextStyle(fontSize: 18,color: appThemeColor),),
        ),
        Spacer(flex: 1,),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(child: Theme(
            data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
            child: new CircularProgressIndicator(),
          ),height: 20,width: 20,),
        ),
      ],
    );
  }

  Widget postLoadingWidget(){
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            child: Icon(Icons.edit,color: appSecondaryColor),
            onTap: (){

            },
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(1.0),
            child: Container(
              height: 100,
              width: MediaQuery.of(context).size.width - 100,
              child: TextField(

                expands: false,
                controller: textController,
                maxLines: 5,
                minLines: 1,
                decoration: InputDecoration(
                  fillColor: appBackgroundColor,
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.multiline,
                style: TextStyle(
                    fontWeight: FontWeight.w400, fontSize: 18, color: appTextColor),
                onChanged: (text) {
                  location["address"] = text;
                  //widget.didAssignValue(widget.item.key, location);
                },
                textInputAction: TextInputAction.newline,
                onSubmitted: (text) {},
              ),
            ),
          ),
        ),
      ],
    );
  }


  Future<Map<String, dynamic>> locateUser() async{
    try {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      List<Placemark> placemarks = await Geolocator()
          .placemarkFromCoordinates(position.latitude, position.longitude);

      Placemark placeMark = placemarks[0];
      String name = placeMark.name;
      String subLocality = placeMark.subLocality;
      String locality = placeMark.locality;
      String administrativeArea = placeMark.administrativeArea;
      String postalCode = placeMark.postalCode;
      String country = placeMark.country;
      String address =
          "$name, $subLocality, $locality, $administrativeArea $country";

     // textController.text = address;
      location["address"] = address;
      location["lat"] = position.latitude;
      location["lon"] = position.longitude;
      textController.text= '$address\nlat:${position.latitude.roundToDouble()}, lng:${position.longitude.roundToDouble()}';
      //  widget.didAssignValue(widget.item.key, location);
    } catch (e) {
      print(e);
      textController.text = "Failed to fetch your current location.";
      location["address"] = "Failed to fetch your current location.";
    }


    widget.callback(location["address"]);
    return location;
  }
}