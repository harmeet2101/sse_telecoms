import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class LocationWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  LocationWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return LocationWidgetState();
  }
}

class LocationWidgetState extends State<LocationWidget> {
  double screenHeight;
  double screenWidth;
  Map<String, dynamic> location = {"address": "", "lat": 0.0, "lon": 0.0};
  TextEditingController textController = TextEditingController(text: "");

  @override
  void initState() {
    super.initState();
    if (widget.value != null) {
      Map<String, dynamic> savedAnswer = widget.value as Map<String, dynamic>;
      textController.text = savedAnswer["address"] ?? "";
      setState(() {});
    }
  }

  @override
  void didUpdateWidget(LocationWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.value != null) {
      Map<String, dynamic> savedAnswer = widget.value as Map<String, dynamic>;
      textController.text = savedAnswer["address"] ?? "";
      setState(() {});
    } else {
      location = {"address": "", "lat": 0.0, "lon": 0.0};
      textController.text = "";
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Text(
            widget.item.label,
            textAlign: TextAlign.left,
            style: widget.item.labelTextStyle,
          ),
        ),
        Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          clipBehavior: Clip.hardEdge,
          child: Padding(
            padding: EdgeInsets.only(left: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 100,
                  width: screenWidth - 166,
                  child: TextField(
                    expands: false,
                    controller: textController,
                    maxLines: 4,
                    minLines: 1,
                    decoration: InputDecoration(
                      fillColor: appBackgroundColor,
                      border: InputBorder.none,
                    ),
                    keyboardType: TextInputType.multiline,
                    style: widget.item.inputTextStyle,
                    onChanged: (text) {
                      location["address"] = text;
                      widget.didAssignValue(widget.item.key, location);
                    },
                    textInputAction: TextInputAction.newline,
                    onSubmitted: (text) {},
                  ),
                ),
                SizedBox(width: 20),
                FlatButton(
                  padding: EdgeInsets.all(0),
                  child: Container(
                    color: appSecondaryColor,
                    height: 100,
                    width: 100,
                    child: Center(
                        child: Text(
                      "Locate\nMe",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: appBackgroundColor,
                      ),
                    )),
                  ),
                  onPressed: () {
                    getLocation();
                  },
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  void getLocation() async {
    try {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      List<Placemark> placemarks = await Geolocator()
          .placemarkFromCoordinates(position.latitude, position.longitude);

      Placemark placeMark = placemarks[0];
      String name = placeMark.name;
      String subLocality = placeMark.subLocality;
      String locality = placeMark.locality;
      String administrativeArea = placeMark.administrativeArea;
      String postalCode = placeMark.postalCode;
      String country = placeMark.country;
      String address =
          "$name, $subLocality, $locality, $administrativeArea $postalCode, $country";

      setState(() {
        textController.text = address;
        location["address"] = address;
        location["lat"] = position.latitude;
        location["lon"] = position.longitude;
        widget.didAssignValue(widget.item.key, location);
      });
    } catch (e) {
      print(e);
      setState(() {
        textController.text = "Failed to fetch your current location.";
        location["address"] = "Failed to fetch your current location.";
        widget.didAssignValue(widget.item.key, location);
      });
    }
  }
}
