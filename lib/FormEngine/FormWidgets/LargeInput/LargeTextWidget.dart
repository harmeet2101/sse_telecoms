import 'package:flutter/material.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';

class LargeTextWidget extends StatefulWidget {
  final FormItem item;
  final dynamic value;
  final void Function(String key, dynamic value) didAssignValue;

  LargeTextWidget(
      {@required this.item,
      @required this.value,
      @required this.didAssignValue});

  @override
  State<StatefulWidget> createState() {
    return LargeTextWidgetState();
  }
}

class LargeTextWidgetState extends State<LargeTextWidget> {
  double screenHeight;
  double screenWidth;
  TextEditingController textController = TextEditingController(text: "");

  @override
  void initState() {
    super.initState();
    if (widget.value != null) {
      String savedAnswer = widget.value as String;
      textController.text = savedAnswer ?? "";
      setState(() {});
    }
  }

  @override
  void didUpdateWidget(LargeTextWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.value != null) {
      String savedAnswer = widget.value as String;
      textController.text = savedAnswer ?? "";
      setState(() {});
    } else {
      textController.text = "";
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Text(
            widget.item.label,
            textAlign: TextAlign.left,
            style: widget.item.labelTextStyle,
          ),
        ),
        Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          clipBehavior: Clip.hardEdge,
          child: Container(
            height: 100,
            width: screenWidth - 30,
            padding: EdgeInsets.all(8),
            child: TextField(
              expands: false,
              controller: textController,
              maxLines: 4,
              minLines: 1,
              decoration: InputDecoration(
                fillColor: appBackgroundColor,
                border: InputBorder.none,
              ),
              keyboardType: TextInputType.multiline,
              style: widget.item.inputTextStyle,
              onChanged: (text) {
                widget.didAssignValue(widget.item.key, text);
              },
              textInputAction: TextInputAction.newline,
              onSubmitted: (text) {},
            ),
          ),
        ),
      ],
    );
  }
}
