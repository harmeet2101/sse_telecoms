import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Data/DBProvider.dart';
import 'package:RouteProve/FormEngine/FormPage.dart';
import 'package:RouteProve/FormEngine/FormWidgets/MarkBlockagePhotosWidget/MarkBlockagePhotosWidget.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:RouteProve/Widgets/appBarWidget.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:flutter/material.dart';

class FormController extends StatefulWidget {
  final String formTitle;
  final String formName;
  final String formId;
  final bool isSubTitleHidden;
  final bool isProgressBarHidden;
  final bool isNavButtonsHidden;
  final List<FormPage> pages;
  final void Function(Map<String, dynamic> data) didFetchSavedAnswers;
  final void Function(Map<String, dynamic> data) changeInAnswers;
  final void Function(String key, dynamic value) didAssignValue;
  final void Function() onSubmit;
  final void Function(int currentPage) changeInPageIndex;
  final String appBarImage;

  FormController(
      {@required this.formTitle,
      @required this.formName,
      @required this.formId,
      this.isSubTitleHidden = false,
      this.isProgressBarHidden = false,
      this.isNavButtonsHidden = false,
      @required this.pages,
      this.changeInAnswers,
      this.didFetchSavedAnswers,
      this.didAssignValue,
      @required this.onSubmit,
      this.changeInPageIndex,
      this.appBarImage = "assets/app_logo.png"});

  @override
  State<StatefulWidget> createState() {
    return FormControllerState();
  }
}

class FormControllerState extends State<FormController> {
  double screenWidth;
  double screenHeight;
  int currentPageIndex = 0;
  Map<String, dynamic> answers = Map<String, dynamic>();
  ScrollController controller = ScrollController();

  @override
  void initState() {
    super.initState();
    initialiseFormController();
  }

  initialiseFormController() async {
    var savedAnswers = await getSavedAnswersFromDB();
    if (savedAnswers != null) {
      answers = savedAnswers;
      if (widget.didFetchSavedAnswers != null) {
        widget.didFetchSavedAnswers(answers);
      }
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

    return getScaffold(
      context,
      Scaffold(
        appBar: getActionbar(
            context,
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                SizedBox(
                  width: screenWidth - 140,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      widget.formTitle,
                      style: TextStyle(
                          color: appBackgroundColor,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                SizedBox(width: 60)
              ],
            ),
            image: widget.appBarImage),
        body: SafeArea(
          child: LayoutBuilder(builder:
              (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
              controller: controller,
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: IntrinsicHeight(
                  child: Column(
                    children: <Widget>[
                      widget.isSubTitleHidden ? SizedBox() : buildTitle(),
                      widget.isProgressBarHidden
                          ? SizedBox()
                          : buildProgressBar(),
                      buildBody(),
                      widget.isNavButtonsHidden ? SizedBox() : Spacer(),
                      widget.isNavButtonsHidden
                          ? SizedBox()
                          : buildBottomButtons()
                    ],
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  buildTitle() {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
      child: Text(
        widget.pages[currentPageIndex].title,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: appTextColor, fontSize: 16, fontWeight: FontWeight.w600),
      ),
    );
  }

  buildProgressBar() {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          height: 10,
          child: LinearProgressIndicator(
            value: widget.pages[currentPageIndex].progress,
            valueColor: AlwaysStoppedAnimation<Color>(appThemeColor),
            backgroundColor: appThemeColor.withOpacity(0.5),
          ),
        ),
      ),
    );
  }

  buildBody() {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Column(
        children: widget.pages[currentPageIndex].items.map((formItem) {
          return formItem.isHidden
              ? Container()
              : Container(
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color:
                          formItem.isValid ? transparentColor : Colors.red[300],
                      borderRadius: BorderRadius.circular(10)),
                  child: buildFormItem(widget.formId, formItem,
                      answers[formItem.key], didAssignValue),
                );
        }).toList(),
      ),
    );
  }

  buildBottomButtons() {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          currentPageIndex > 0
              ? Container(
                height: 40,
                width: screenWidth / 2 - 20,
                child: RaisedButton(
                  elevation: 5,
                  color: appThemeColor,
                  shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.all(Radius.elliptical(10, 10)),
                  ),
                  child: RichText(
                      text: TextSpan(
                          text: "Back",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: appBackgroundColor))),
                  onPressed: () {
                    previousButtonAction();
                  },
                ),
              )
              : SizedBox(),
          currentPageIndex > 0 ? SizedBox(width: 10) : SizedBox(),
          Container(
            height: 40,
            width: currentPageIndex > 0 ? screenWidth / 2 - 20 : screenWidth / 2 - 30,
            child: RaisedButton(
              elevation: 5,
              color: appSecondaryColor,
              textColor: appBackgroundColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
              ),
              child: RichText(
                  text: TextSpan(
                      text: (currentPageIndex) == (widget.pages.length - 1)
                          ? "Submit"
                          : "Next",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: appBackgroundColor))),
              onPressed: () {
                nextButtonAction();
              },
            ),
          )
        ],
      ),
    );
  }

  previousButtonAction() {
    if (currentPageIndex > 0) {
      currentPageIndex -= 1;
      if (widget.changeInPageIndex != null) {
        widget.changeInPageIndex(currentPageIndex);
      }
      reloadForm();
    }
  }

  nextButtonAction() {
    if (isFormValid(widget.pages[currentPageIndex])) {
      saveAnswersToDB();
      if ((currentPageIndex) == (widget.pages.length - 1)) {
        reloadForm();
        widget.onSubmit();
      } else {
        currentPageIndex += 1;
        reloadForm();
        if (widget.changeInPageIndex != null) {
          widget.changeInPageIndex(currentPageIndex);
        }
      }
    } else {
      reloadForm();
      showInvalidFormAlert();
    }
  }

  bool isFormValid(FormPage page) {
    bool isFormValid = true;

    for (var i = 0; i < page.items.length; i++) {
      FormItem item = page.items[i];
      if (item.isHidden == false && item.isRequired == true) {
        if (item.type == FormViewType.Camera) {
          // For Camera Type Items
          final answer = answers[item.key] as List<dynamic>;
          if (answer != null) {
            if (answer.length < item.minRequired ||
                answer.length > item.maxRequired) {
              isFormValid = false;
              widget.pages[currentPageIndex].items[i].isValid = false;
            } else {
              widget.pages[currentPageIndex].items[i].isValid = true;
            }
          } else {
            isFormValid = false;
            widget.pages[currentPageIndex].items[i].isValid = false;
          }
        } else if (item.type == FormViewType.Card) {
          // For Card Type Items
          if (item.status != FormControlStatus.active) {
            isFormValid = false;
            widget.pages[currentPageIndex].items[i].isValid = false;
          } else {
            widget.pages[currentPageIndex].items[i].isValid = true;
          }
        } else if (item.type == FormViewType.Mark_Blockage_Photo) {
          final Map<String, dynamic> blockagePhotos = answers[item.key];
          if (blockagePhotos != null) {
            if (blockagePhotos[JointBoxA] != null &&
                blockagePhotos[JointBoxA][PhotoOfArea] != null &&
                blockagePhotos[JointBoxA][PhotoOfBox] != null &&
                blockagePhotos[JointBoxB] != null &&
                blockagePhotos[JointBoxA][PhotoOfArea] != null &&
                blockagePhotos[JointBoxA][PhotoOfBox] != null &&
                blockagePhotos[BlockageAtA] != null &&
                blockagePhotos[BlockageAtA].length > 0 &&
                blockagePhotos[BlockageAtB] != null &&
                blockagePhotos[BlockageAtB].length > 0) {
              isFormValid = true;
            } else {
              isFormValid = false;
              widget.pages[currentPageIndex].items[i].isValid = false;
            }
          } else {
            isFormValid = false;
            widget.pages[currentPageIndex].items[i].isValid = false;
          }
        } else if (item.type == FormViewType.JointboxA ||
            item.type == FormViewType.JointboxB) {
          // For Card Type Items
          final answer = answers[item.key] as String;
          if (answer != null) {
            if (answer == "") {
              isFormValid = false;
              widget.pages[currentPageIndex].items[i].isValid = false;
            } else {
              widget.pages[currentPageIndex].items[i].isValid = true;
            }
          } else {
            isFormValid = false;
            widget.pages[currentPageIndex].items[i].isValid = false;
          }
        } else {
          // For Other Items
          if (answers[item.key] == null) {
            isFormValid = false;
            widget.pages[currentPageIndex].items[i].isValid = false;
          } else {
            widget.pages[currentPageIndex].items[i].isValid = true;
          }
        }
      }
    }

    return isFormValid;
  }

  saveAnswersToDB() async {
    await DBProvider.db.addAnswer(widget.formId, widget.formName, answers);
  }

  Future<Map<String, dynamic>> getSavedAnswersFromDB() async {
    Map<String, dynamic> savedAnswers =
        await DBProvider.db.getAnswer(widget.formId, widget.formName);
    return savedAnswers;
  }

  showInvalidFormAlert() {
    showAlertMessage(
        message: "Please provide value for all required fields.",
        type: AlertType.Failure,
        context: context);
  }

  showSuccesFormAlert(String message) {
    showAlertMessage(
        message: message, type: AlertType.Success, context: context);
  }

  reloadForm() {
    controller.jumpTo(0.0);
    setState(() {});
  }

  didAssignValue(String key, dynamic value) {
    answers[key] = value;
    if (widget.changeInAnswers != null) {
      widget.changeInAnswers(answers);
    }
    if (widget.didAssignValue != null) {
      widget.didAssignValue(key, value);
    }
  }
}
