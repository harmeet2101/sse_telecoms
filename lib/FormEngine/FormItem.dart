import 'package:RouteProve/FormEngine/FormWidgets/DefectPhotoWidget/DefectPhotoWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/FibreInstallPhotoWidget/FibreInstallPhotoWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/JointboxAWidget/JointboxAWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/JointboxBWidget/JointboxBWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/MarkBlockagePhotosWidget/MarkBlockagePhotosWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/MarkBlockageWidget/MarkBlockageWidget.dart';
import 'package:flutter/material.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/FormEngine/FormWidgets/CardWidget/CardWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/CameraWidget/CameraWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/LargeInput/LargeTextWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/LocationWidget/LocationWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/SwitchWidget/SwitchWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/LabelWidget/LabelWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/SingleChoiceWidget/SingleChoiceWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/SmallInput/SmallInputWidget.dart';
import 'package:RouteProve/FormEngine/FormWidgets/TitleLabelWidget/TitleLabelWidget.dart';

enum FormViewType {
  Title_Label,
  Label,
  Small_Input,
  Large_Input,
  Single_Choice,
  Card,
  Switch,
  Location,
  Camera,
  Mark_Blockage,
  Mark_Blockage_Photo,
  JointboxA,
  JointboxB,
  FibrePhotoWidget,
  DefectPhotoWidget
}

enum FormControlStatus { active, normal, disabled }

enum FormFieldDataType { text, number, decimal, email, secure }

class FormItem {
  final String label;
  final String key;
  final bool isRequired;
  final FormControlStatus status;
  final FormViewType type;
  final List<ChoiceItem> choiceItems;
  final List<MultiChoiceItem> multiChoiceItems;
  final int tag;
  final int minRequired;
  final int maxRequired;
  final bool isHidden;
  final FormFieldDataType dataType;
  final TextStyle titleTextStyle;
  final TextStyle labelTextStyle;
  final TextStyle inputTextStyle;
  final Map<String, dynamic> blockageLocation;
  final String initialDuctValue;
  bool isValid;

  FormItem(
      {@required this.label,
      @required this.key,
      @required this.isRequired,
      this.status = FormControlStatus.normal,
      @required this.type,
      this.choiceItems,
      this.multiChoiceItems,
      this.tag = 0,
      this.minRequired = 0,
      this.maxRequired = 20,
      this.isHidden = false,
      this.dataType = FormFieldDataType.text,
      this.titleTextStyle = const TextStyle(
          fontWeight: FontWeight.w600, fontSize: 16, color: appTextColor),
      this.labelTextStyle = const TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 14,
          color: appSecondaryColor),
      this.inputTextStyle = const TextStyle(
          fontWeight: FontWeight.w400, fontSize: 14, color: appTextColor),
      this.blockageLocation,
      this.initialDuctValue,
      this.isValid = true});
}

buildFormItem(String formId, FormItem item, dynamic value,
    void Function(String key, dynamic value) didAssignValue) {
  switch (item.type) {
    case FormViewType.Title_Label:
      return TitleLabelWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Label:
      return LabelWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Small_Input:
      return SmallInputWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Large_Input:
      return LargeTextWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Single_Choice:
      return SingleChoiceWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Card:
      return CardWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Switch:
      return SwitchWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Location:
      return LocationWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Camera:
      return CameraWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
        formId: formId,
      );
    case FormViewType.Mark_Blockage:
      return MarkBlockageWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.Mark_Blockage_Photo:
      return MarkBlockagePhotosWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
        formId: formId,
      );
    case FormViewType.JointboxA:
      return JointboxAWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.JointboxB:
      return JointboxBWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );

    case FormViewType.FibrePhotoWidget:
      return FibreInstallPhotoWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );
    case FormViewType.DefectPhotoWidget:
      return DefectPhotoWidget(
        item: item,
        value: value,
        didAssignValue: didAssignValue,
      );

  }
}
