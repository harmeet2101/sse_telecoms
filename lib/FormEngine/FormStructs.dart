//Single Choice Item
import 'package:flutter/material.dart';

class ChoiceItem {
  final int key;
  final String value;

  ChoiceItem({
    @required this.key,
    @required this.value,
  });
}

//Multi Choice Item
class MultiChoiceItem {
  final String key;
  final String value;
  bool isSelected;

  MultiChoiceItem({
    @required this.key,
    @required this.value,
    this.isSelected = false,
  });
}

//Multi Choice Item
class BlockageLocation {
  BlockageLocation(
      {this.blockageAtA,
      this.blockageAtB,
      this.sectionLength,
      this.numberOfBlockages});

  double blockageAtA;
  double blockageAtB;
  double sectionLength;
  int numberOfBlockages;

  factory BlockageLocation.fromJson(Map<String, dynamic> json) =>
      BlockageLocation(
        blockageAtA: json["blockageAtA"] == null ? null : json["blockageAtA"],
        blockageAtB: json["blockageAtB"] == null ? null : json["blockageAtB"],
        sectionLength:
            json["sectionLength"] == null ? null : json["sectionLength"],
        numberOfBlockages: json["numberOfBlockages"] == null
            ? null
            : json["numberOfBlockages"],
      );

  Map<String, dynamic> toJson() => {
        "blockageAtA": blockageAtA == null ? null : blockageAtA,
        "blockageAtB": blockageAtB == null ? null : blockageAtB,
        "sectionLength": sectionLength == null ? null : sectionLength,
        "numberOfBlockages":
            numberOfBlockages == null ? null : numberOfBlockages,
      };
}
