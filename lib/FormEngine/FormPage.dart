import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:flutter/material.dart';

class FormPage {
  final String title;
  final double progress;
  final List<FormItem> items;

  FormPage({
    this.title = "",
    this.progress = 0,
    @required this.items,
  });
}
