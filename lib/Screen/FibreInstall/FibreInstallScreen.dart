import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Data/AppInitDataProvider.dart';
import 'package:RouteProve/Data/DBProvider.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/FormEngine/FormController.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:RouteProve/FormEngine/FormPage.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/Modal/FibreInstallRequest.dart';
import 'package:RouteProve/Modal/FibreInstallResponse.dart';
import 'package:RouteProve/Modal/PostFibreInstall.dart';
import 'package:RouteProve/Modal/dataset/work_point.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class FibreInstallScreen extends StatefulWidget{

  int jobId;
  FibreInstallScreen({this.jobId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FibreInstallState();
  }
}

class FibreInstallState extends State<FibreInstallScreen>{

  int currentPage = 0;
  List<String> images = [
    "assets/app_logo.png",
    "assets/app_logo.png",
    "assets/app_logo.png",
  ];
 // String fibreInstallationId;
  List<ChoiceItem> wp1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  //  fibreInstallationId = Uuid().v4();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return FutureBuilder<List<WorkPoint>>(builder:(BuildContext context, AsyncSnapshot<List<WorkPoint>> snapshot){

     if(snapshot.hasData){

       wp1 = new List<ChoiceItem>();

       for(int i =0;i<snapshot.data.length;i++){

         wp1.add(new ChoiceItem(key: snapshot.data[i].jobWorkPointId,
             value: 'Work Point ${snapshot.data[i].rowNum}-'
                 '${snapshot.data[i].plantItem} (${(snapshot.data[i].categoryName==CategoryName.BURIED)?'BURIED':'JOINTING CHAMBER'})'));
       }

       return FormController(
         formId: widget.jobId.toString(),
         formName: "FibreInstall",
         formTitle: "Fibre Installation",
         appBarImage: images[currentPage],
         isSubTitleHidden: false,
         isProgressBarHidden: true,
         pages: getFormPages(),
         didFetchSavedAnswers: (data) {


         },
         didAssignValue: (key, value) {

         },
         onSubmit: () {
           getSavedAnswersFromDB();
           //submitTapped();
         },
         changeInPageIndex: (int pageIndex) {
           currentPage = pageIndex;
           setState(() {});
         },
       );

     }
     else return Stack(
       children: [
         new FormController(
       formId: widget.jobId.toString(),
       formName: "FibreInstall",
       formTitle: "Fibre Installation",
       appBarImage: images[currentPage],
       isSubTitleHidden: false,
       isProgressBarHidden: true,
       pages: getFormPages(),
       didFetchSavedAnswers: (data) {


       },
       didAssignValue: (key, value) {

       },
       onSubmit: () {
         getSavedAnswersFromDB();
         //submitTapped();
       },
       changeInPageIndex: (int pageIndex) {
         currentPage = pageIndex;
         setState(() {});
       },
     ),
         new Opacity(
           opacity: 0.3,
           child: const ModalBarrier(
               dismissible: false, color: Colors.grey),
         ),
         new Center(
           child: Theme(
             data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
             child: new CircularProgressIndicator(),
           ),
         ),
       ],
     );


   } ,future:Api().getWorkPoints(widget.jobId));
  }

  List<FormPage> getFormPages() {
    List<FormPage> pages = List<FormPage>();

    pages.add(
      FormPage(
        title: "",
        progress: 0.15,
        items: <FormItem>[

          FormItem(
              label: "Work Point 1",
              key: "workPoint1Details",
              isRequired: true,
              type: FormViewType.Single_Choice,
              choiceItems: wp1),
          FormItem(
              label: "Work Point 2",
              key: "workPoint2Details",
              isRequired: true,
              type: FormViewType.Single_Choice,
              choiceItems: wp1),
          FormItem(
              label: "Duct Section",
              key: "ductSection",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.text),
          FormItem(
              label: "Section Length (m)",
              key: "sectionLength",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.number),
          FormItem(
            label: "Cable Type",
            key: "cableTypeId",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getCableType(),
          ),
          FormItem(
            label: "Number of Cables in situ",
            key: "noCables",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getCableSituType(),
          ),
          FormItem(
            label: "boreIdentified And Used",
            key: "boreIdentifiedAndUsed",
            isRequired: true,
            type: FormViewType.Switch,
          ),
          FormItem(
              label: "Manufacturer Of Join",
              key: "manufacturerOfJoin",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.text),

        ],
      ),
    );

    pages.add(
      FormPage(
        title: "",
        progress: 0.30,
        items: <FormItem>[



          FormItem(
              label: "Fibre Capacity",
              key: "fibreCapacity",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.text),
          FormItem(
              label: "No. CablePorts Used",
              key: "noCablePortsUsed",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.number),
          FormItem(
              label: "noCablePortsSpare",
              key: "noCablePortsSpare",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.number),
          FormItem(
              label: "Amount Of Slack",
              key: "amountOfSlack",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.number),
          FormItem(
            label: "Comments",
            key: "comments",
            isRequired: true,
            type: FormViewType.Large_Input,
          ),
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Images of Fibre Installation",
        progress: 1.0,
        items: <FormItem>[
          FormItem(
              label: '',
              key: 'fibrePhoto',
              isRequired: false,
              type: FormViewType.FibrePhotoWidget
          )
        ],
      ),
    );

    return pages;
  }

  submitTapped() async {
    showAlertMessage(
        message: "Submit Successful",
        context: context,
        type: AlertType.Success);
    // var response = await Api.api
    //     .submitBlockageLocate("BlockageLocate", "BlockageLocate");
  }

  Future<Map<String, dynamic>> getSavedAnswersFromDB() async {
    Map<String, dynamic> savedAnswers =
    await DBProvider.db.getAnswer(widget.jobId.toString(), 'FibreInstall');
    savedAnswers.forEach((key, value) {
      print('${key}: ${value}');
    });
    String authToken = await Constants.getAuthToken();
    PostFibreInstall postFibreInstall = new PostFibreInstall(

      authToken:  authToken,
      jobId: widget.jobId,
      submissionId: Uuid().v4(),
      startingJobWorkPointId: int.parse(savedAnswers['workPoint1Details'].toString().split('_').last),
      endingJobWorkPointId: int.parse(savedAnswers['workPoint2Details'].toString().split('_').last),
      ductSection: savedAnswers['ductSection'],
      sectionLength: int.parse(savedAnswers['sectionLength']),
      cableTypeId: int.parse(savedAnswers['cableTypeId'].toString().split('_').last),
      cablesInSituId: int.parse(savedAnswers['noCables'].toString().split('_').last),
      fibreCapacity: savedAnswers['fibreCapacity'],
      cablePortsUsed: int.parse(savedAnswers['noCablePortsUsed']),
      cablesPortsSpare: int.parse(savedAnswers['noCablePortsSpare']),
      slackAtJoint: int.parse(savedAnswers['amountOfSlack']),
      comments: savedAnswers['comments'],
      boreIdentifiedAndUsed: savedAnswers['boreIdentifiedAndUsed'].toString(),
      createdByUserId: 0,
      manufacturerOfJoint: savedAnswers['manufacturerOfJoin'],

    );
    FibreInstallResponse resp = await Api().submitFibreInstall(postFibreInstall.toJson());

    print(resp.toString());
    if(resp!=null){
      showAlertMessage(
          message: "Submit Successful",
          context: context,
          type: AlertType.Success);
      DBProvider.db.changeAnswerUploadStatus(widget.jobId.toString(), 'FibreInstall', true);
    }
    else{
      DBProvider.db.changeAnswerCompletedStatus(widget.jobId.toString(), 'FibreInstall', true);
      DBProvider.db.changeAnswerUploadStatus(widget.jobId.toString(), 'FibreInstall', false);
    }

    try {
      Map<String, dynamic> fibrePhoto  =  savedAnswers['fibrePhoto'];
      Map<String, dynamic> fibre_photo_1  =  fibrePhoto['Picture of Installation'];
      Map<String, dynamic> fibre_photo_2  =  fibrePhoto['Picture of labels'];
      if(fibre_photo_1!=null)
        uploadPhotos(fibre_photo_1['path'], resp, authToken, widget.jobId);
      if(fibre_photo_2!=null)
        uploadPhotos(fibre_photo_2['path'], resp, authToken, widget.jobId);
    } on Exception catch (e) {
      // TODO
    }

    return savedAnswers;
  }


  Future<void> uploadPhotos(String path,FibreInstallResponse response,String authToken,int jobId)async{

    String str = await FileHandler.readFile('${FileHandler.localPath}/${path}');
    Map<String,dynamic> mp ={"Photo":str,"fibreInstallId":response.fibreInstallId,"AuthToken":authToken,"JobID":jobId};
    await Api().uploadImage(mp);
  }
}