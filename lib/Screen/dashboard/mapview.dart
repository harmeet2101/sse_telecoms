import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget{

  String address;

  MapScreen({this.address});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MapState();
  }
}

class MapState extends State<MapScreen>{

  Completer<GoogleMapController> _controller = Completer();
  Position _currentPosition;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0.0, 0.0),
    zoom: 14.4746,
  );

  CameraPosition _kLake ;

  final Map<String, Marker> _markers = {};


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   _getCurrentLocation();
  }

  _getCurrentLocation()async{


    // From a query
    final query = widget.address;
    var addresses = await Geocoder.local.findAddressesFromQuery(query);
    var first = addresses.first;
    print("${first.featureName} : ${first.coordinates}");

    setState(() async{
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId("curr_loc"),
        position: LatLng(first.coordinates.latitude, first.coordinates.longitude),
        infoWindow: InfoWindow(title: 'Your Location'),
      );

      _markers["Current Location"] = marker;
      _kLake = CameraPosition(
        // bearing: 192.8334901395799,
          target: LatLng(first.coordinates.latitude, first.coordinates.longitude),
          // tilt: 59.440717697143555,
          zoom: 15.0
      );
      final GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
    });

    /*geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() async {
        _currentPosition = position;

        _markers.clear();
        final marker = Marker(
          markerId: MarkerId("curr_loc"),
          position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
          infoWindow: InfoWindow(title: 'Your Location'),
        );

        _markers["Current Location"] = marker;
        _kLake = CameraPosition(
          // bearing: 192.8334901395799,
            target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
            // tilt: 59.440717697143555,
            zoom: 15.0
        );
        final GoogleMapController controller = await _controller.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
      });
    }).catchError((e) {
      print(e);
    });*/
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: GoogleMap(

        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlex,
        markers: _markers.values.toSet(),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
    );
  }
}