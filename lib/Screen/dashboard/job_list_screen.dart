import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/helper.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Modal/jobs.dart';
import 'package:RouteProve/Screen/custom_calendar/calendarro.dart';
import 'package:RouteProve/Screen/custom_calendar/date_utils.dart';
import 'package:RouteProve/Screen/dashboard/job_details.dart';
import 'package:RouteProve/Screen/dashboard/show_all_jobs_screen.dart';
import 'package:RouteProve/Screen/dashboard/work_log_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class JobListScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return JobListState();
  }
}

class JobListState extends State<JobListScreen>{

  String selectedDate ;
  DateTime _selectedDate;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedDate = reformatDate2(DateTime.now());
    _selectedDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.grey[200],
      child: Column(
        children: [
          Calendarro(
            startDate:DateUtils.getFirstDayOfCurrentMonth(),
            endDate: DateUtils.getLastDayOfCurrentMonth(),
            selectedSingleDate: DateTime.now(),
            ShowAllJobs: (){

              Navigator.push(context, MaterialPageRoute(builder: (context)=>AllJobsScreen()));
            },
            onTap: (date){
              _selectedDate = date;
              selectedDate = reformatDate2(date);
              setState(() {});
            },
          ),

          Expanded(child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: FutureBuilder<List<Jobs>>(
              future: Api().getJobs(),
              builder: (context, snapshot){
                if(snapshot.hasData){

                 List<Jobs> jb = snapshot.data;
                 List<Jobs> jobListFilterd = null/*jb.where((element)=>
                 reformatDate2(element.dateCreated)==selectedDate).
                 toList()*/;

                 jobListFilterd = new List();
                 for(int i =0;i<jb.length;i++){

                   if((_selectedDate.isAtSameMomentAs(jb[i].startDate)||_selectedDate.isAfter(jb[i].startDate ))
                      && jb[i].endDate.compareTo(_selectedDate)>=0){
                     jobListFilterd.add(jb[i]);
                   }

                 }
                 
                  return Container(
                    color: Colors.grey[200],
                    child: jobListFilterd.isEmpty?
                    Center(child: Text('No jobs are present for this day',style: TextStyle(
                      color: appSecondaryColor,fontSize: 18
                    ),),):
                    ListView.builder(itemBuilder: (BuildContext context,int count){
                      return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: Card(
                            color: Colors.white,
                            elevation: 5.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),

                            ),
                            child: getMainContent(context,jobListFilterd[count]),
                          ));
                    },itemCount: jobListFilterd.length,),
                  );
                }else{
                  return Center(child: Theme(
                    data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
                    child: new CircularProgressIndicator(),
                  ));
                }
              },
            ),
          )),
        ],
      ),
    );
  }
  Widget getMainContent(BuildContext context,Jobs job){
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        children: [
          InkWell(
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>WorkLogScreen(jobId: job.jobId,)));
            },
            child: Row(
              children: [

                Container(

                  width: 120,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Center(
                    child: Icon(Icons.report,color: Colors.yellow[800],size: 60.0,),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 20, 20,0),
                      child: Text('Exchange: ${job.exchangeName}',style:TextStyle(
                          color: Colors.black,
                          fontSize: 18

                      ) ,),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20,0),
                      child: Text('Job No: ${job.jobId}',style:TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                      ) ,softWrap: true,maxLines: 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20,10),
                      child: Text('Section: ${job.jobRefNumber}',style:TextStyle(
                          color: Colors.black,
                          fontSize: 18

                      ) ,),
                    ),
                  ],
                ),

              ],
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>JobDetailScreen(jobDetails: job,)));
            },
            child: Container(child: Center(child: Text('View Job',style: TextStyle(fontSize: 16,color: appBackgroundColor),),)
              ,height: 45,decoration: BoxDecoration(
                color: appThemeColor,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10.0)),
              ),),
          ),
        ],
      ),
    );
  }
}