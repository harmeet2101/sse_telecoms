import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Data/DBProvider.dart';
import 'package:RouteProve/Screen/Defect/DefectScreen.dart';
import 'package:RouteProve/Screen/FibreInstall/FibreInstallScreen.dart';
import 'package:RouteProve/Screen/blockagClearance/blockage_list_screen.dart';
import 'package:RouteProve/Screen/blockage_locator_scr.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OfflineQueueScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OfflineQueueState();
  }
}

class OfflineQueueState extends State<OfflineQueueScreen>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<List<String>>(
      future: DBProvider.db.getOfflineQueueList(),
      builder: (context, snapshot){
        if(snapshot.hasData){
          return snapshot.data.isNotEmpty?Container(
            color: Colors.grey[200],
            child: ListView.builder(itemBuilder: (BuildContext context,int count){
              return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 4),
                  child: Card(
                      color: Colors.white,
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),

                      ),
                      child: QueueCard(context,snapshot.data[count],)
                  ));
            },itemCount: snapshot.data.length,),
          ):Container(
            child: Center(
              child: Text('Queue List Empty',style: TextStyle(color: appSecondaryColor,fontSize: 18),),
            ),
          );
        }else{
          return Center(child: Theme(
            data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
            child: new CircularProgressIndicator(),
          ));
        }
      },
    );
  }

  Widget QueueCard(BuildContext context,String result){


    return InkWell(
      onTap: (){

        String type = result.split('_').last;

        switch(type){

          case 'Defect1024':{
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        DefectScreen(jobId: int.parse(result.split('_').first))));
          }
            break;
          case 'FibreInstall':{
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        FibreInstallScreen(jobId: int.parse(result.split('_').first))));

          }
            break;

          case 'BlockageLocate':{
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        BlockageLocatorScreen(jobId: int.parse(result.split('_').first))));

          }
          break;
          case 'BlockageClearance':{
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        BlockageListScreen()));

          }
          break;
        }

      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(10)
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.white,
                      //   width: 200,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 20, 20,0),
                            child: Text('JobId: ${result.split('_').first}',style:TextStyle(
                                color: Colors.black,
                                fontSize: 18

                            ) ,),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 20, 20,0),
                            child: Text('Type:${result.split('_').last}',style:TextStyle(
                                color: Colors.black,
                                fontSize: 18

                            ) ,),
                          ),
                          SizedBox(height: 20,),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(child: Container(
                  child: Center(child: Icon(Icons.navigate_next,
                    size: 40,color: appSecondaryColor,)),
                ),flex: 0,)
                ,SizedBox(width: 10,),
              ],
            ),
          ],
        ),
      ),
    );
  }
}