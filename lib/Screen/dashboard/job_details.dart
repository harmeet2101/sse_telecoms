import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/helper.dart';
import 'package:RouteProve/Modal/jobs.dart';
import 'package:RouteProve/Screen/dashboard/job_pack_screen.dart';
import 'package:RouteProve/Screen/dashboard/mapview.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class JobDetailScreen extends StatefulWidget{

  Jobs jobDetails;


  JobDetailScreen({this.jobDetails});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
     return JobDetailState();
  }
}

class JobDetailState extends State<JobDetailScreen>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return getScaffold(context, Scaffold(

      appBar: AppBar(
        backgroundColor: appThemeColor,
        title: Text('Job ID: ${widget.jobDetails.jobId}',style: TextStyle(color: appBackgroundColor,),),
        centerTitle: true,
      ),
      body:CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(child: MapScreen(address: widget.jobDetails.location,),height: 250),
          ),
          SliverToBoxAdapter(child: Column(
            children: [

              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('JOB DETAILS',style: TextStyle(color: appSecondaryColor,fontSize: 18),),
                  ),
                  Spacer(flex: 1,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      onTap: (){

                        Navigator.push(context, MaterialPageRoute(builder: (context)=>JobPackScreen()));
                      },
                      child: Container(
                        width: 100,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Job Pack',style: TextStyle(color: Colors.white,fontSize: 16),),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              getContent(1, 'Job Reference Number', widget.jobDetails.orReferenceNumber),

              getContent(2, 'Contractor', widget.jobDetails.contractorName),

              getContent(3, 'Exchange', widget.jobDetails.exchangeName),

              getContent(4, 'Job Status', widget.jobDetails.jobStatusName),

              getContent(5, 'Location', widget.jobDetails.location),

              getContent(6, 'Postcode', widget.jobDetails.postcode),

              getContent(7, 'Region', widget.jobDetails.regionName),

              getContent(8, 'Comments', widget.jobDetails.whereaboutComments),

              getContent(9, 'Start Date', reformatDate2(widget.jobDetails.startDate)),

              getContent(10, 'End Date', reformatDate2(widget.jobDetails.endDate)),

              getContent(11, 'Gang',widget.jobDetails.gangRef),

            ],
          )),
        ],),

      ));
  }

  Widget getContent(int position,String key , String value){

    return (position%2==0)?Container(
      color: Colors.grey[300],
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Text(key,style: TextStyle(color: Colors.black87,fontSize: 18),),
            Spacer(flex: 1,),
            Text(value==null?'':value,
              style: TextStyle(color: Colors.black87,fontSize: 18),textAlign: TextAlign.center,maxLines: 2,overflow: TextOverflow.ellipsis,)
          ],
        ),
      ),
    ):Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(key,style: TextStyle(color: Colors.black87,fontSize: 18),),
        ),
        Spacer(flex: 1,),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(value,
            style: TextStyle(color: Colors.black87,fontSize: 18),textAlign: TextAlign.center,maxLines: 2,overflow: TextOverflow.ellipsis,),
        )
      ],
    ) ;
  }
}