import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Screen/dashboard/job_list_screen.dart';
import 'package:RouteProve/Screen/dashboard/kit_bag_list.dart';
import 'package:RouteProve/Screen/login_scr.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/material.dart';

import 'offline_queue_list.dart';

class HomeScreen0 extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeScreenState();
  }
}
class HomeScreenState extends State<HomeScreen0>{

  int _selectedIndex = 0;
  String appBarTitle = 'My Jobs';
  static  List<Widget> _widgetOptions = <Widget>[
    JobListScreen(),
    KitBagScreen(),
    OfflineQueueScreen(),
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return getScaffold(
      context,
      Scaffold(
        appBar:AppBar(
          backgroundColor: appThemeColor,
          title: Text(appBarTitle,style: TextStyle(color: appBackgroundColor,),),
          centerTitle: true,
          elevation: 0.0,
          actions: [

            GestureDetector(onTap: (){},child: Icon(Icons.settings,color: appBackgroundColor,),),
            SizedBox(width: 20,),
            GestureDetector(onTap: (){},child: Icon(Icons.share,color: appBackgroundColor,),),
            SizedBox(width: 20,),
            InkWell(onTap: (){
              logoutTapped();
            },child: Icon(Icons.power_settings_new,color: appBackgroundColor,),),
            SizedBox(width: 10,),
          ],
        ),
        body:_widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Jobs'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.all_out),
              title: Text('Kit Bag'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.offline_bolt),
              title: Text('Offline Queue'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: appSecondaryColor,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      switch (index) {
        case 0:
          appBarTitle = 'My Jobs';
          break;
        case 1:
          appBarTitle = 'Kit Bag';
          break;
        case 2:
          appBarTitle = 'Offline Queue';
          break;
      }
    });
  }


  logoutTapped() async {
    await Constants.clearAllUserDefaults();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen()),
            (route) => false);
  }

}