import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Modal/jobs.dart';
import 'package:RouteProve/Screen/dashboard/job_details.dart';
import 'package:RouteProve/Screen/dashboard/work_log_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AllJobsScreen extends StatelessWidget{
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: appThemeColor,
        centerTitle: true,
        title: Text('Job List',style: TextStyle(color: appBackgroundColor),),
      ),
      body: FutureBuilder<List<Jobs>>(
        future: Api().getJobs(),
        builder: (context, snapshot){
          if(snapshot.hasData){
            return Container(
              color: Colors.grey[200],
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: ListView.builder(itemBuilder: (BuildContext context,int count){
                  return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Card(
                        color: Colors.white,
                        elevation: 5.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),

                        ),
                        child: getMainContent(context,snapshot.data[count]),
                      ));
                },itemCount: snapshot.data.length,),
              ),
            );
          }else{
            return Container(
              color: Colors.white,
              child: Center(child: Theme(
                data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
                child: new CircularProgressIndicator(),
              )),
            );
          }
        },
      ),
    );
  }

  Widget getMainContent(BuildContext context,Jobs job){
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        children: [
          InkWell(
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>WorkLogScreen(jobId: job.jobId,)));
            },
            child: Row(
              children: [

                Container(

                  width: 120,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Center(
                    child: Icon(Icons.report,color: Colors.yellow[800],size: 60.0,),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 20, 20,0),
                      child: Text('Exchange: ${job.exchangeName}',style:TextStyle(
                          color: Colors.black,
                          fontSize: 18

                      ) ,),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20,0),
                      child: Text('Job No: ${job.jobId}',style:TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                      ) ,softWrap: true,maxLines: 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20,10),
                      child: Text('Section: ${job.jobRefNumber}',style:TextStyle(
                          color: Colors.black,
                          fontSize: 18

                      ) ,),
                    ),
                  ],
                ),

              ],
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>JobDetailScreen(jobDetails: job,)));
            },
            child: Container(child: Center(child: Text('View Job',style: TextStyle(fontSize: 16,color: appBackgroundColor),),)
              ,height: 45,decoration: BoxDecoration(
                color: appThemeColor,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10.0)),
              ),),
          ),
        ],
      ),
    );
  }
}