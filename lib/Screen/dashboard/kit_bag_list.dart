

import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Modal/KitBagResponse.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../pdf_viewer_screen.dart';

class KitBagScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return KitBagState();
  }
}

class KitBagState extends State<KitBagScreen>{

  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<List<KitBagResponse>>(
      future: Api().getKitBagList(),
      builder: (context, snapshot){
        if(snapshot.hasData){
          return Container(
            color: Colors.grey[200],
            child: ListView.builder(itemBuilder: (BuildContext context,int count){
              return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Card(
                    color: Colors.white,
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),

                    ),
                    child: KitBagCard(kitBagResponse: snapshot.data[count],)
                  ));
            },itemCount: snapshot.data.length,),
          );
        }else{
          return Center(child: Theme(
            data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
            child: new CircularProgressIndicator(),
          ));
        }
      },
    );
  }


  Widget getMainContent(BuildContext context,KitBagResponse kitBagResponse){


    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        children: [
          Row(
            children: [

              Container(

                width: 50,
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Center(
                  child: Icon(Icons.report,color: Colors.yellow[800],size: 30.0,),
                ),
              ),
              Container(
                color: Colors.white,
                width: 200,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 20, 20,0),
                      child: Text('FileName: ${kitBagResponse.fileName}',style:TextStyle(
                          color: Colors.black,
                          fontSize: 18

                      ) ,),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20,10),
                      child: Text('Extension: ${kitBagResponse.extension}',style:TextStyle(
                          color: Colors.black,
                          fontSize: 18

                      ) ,),
                    ),
                  ],
                ),
              ),
              Container(
                width: 100,
                child: FutureBuilder<String>(future: Api().checkPdf(kitBagResponse.url),

                builder: (context,snapshot){

                  if(snapshot.connectionState==ConnectionState.none && snapshot.data==null){
                    return Container();
                  }
                  if(snapshot.hasData){

                    return InkWell(
                      onTap: () async {
                        if(snapshot.data.isNotEmpty){

                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => PDFReaderScreen(filePath: snapshot.data,)));

                        }else{
                          Api().downloadEbook(kitBagResponse.url,showDownloadProgress).then((value) => {
                          setState((){
                          isLoading = false;
                          }),
                          });
                          setState((){
                            isLoading = true;
                          });
                        }
                      },
                      child: Container(

                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(10)
                          ),
                          child:
                          Center(
                            child: Text(!snapshot.data.isEmpty?'open':'Download',style: TextStyle(color: appSecondaryColor,fontSize: 18),),
                          )

                      ),
                    );

                  }
                  return Container();
                },
                ),
              ),
              /*FutureBuilder<String>(
                future: Api().checkPdf(kitBagResponse.url),
                builder:(context, snapshot){

                  if(snapshot.hasData){
                    print(snapshot.data);
                    return InkWell(
                      onTap: () async {
                        if(snapshot.data.isNotEmpty){

                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => PDFReaderScreen(filePath: snapshot.data,)));

                        }else{
                          await Api().downloadEbook(kitBagResponse.url,showDownloadProgress);
                          setState((){});
                        }
                      },
                      child: Container(

                        width: 100,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child:
                            Center(
                              child: Text(!snapshot.data.isEmpty?'open':'Download',style: TextStyle(color: appSecondaryColor,fontSize: 18),),
                            )

                      ),
                    );

                  }else{
                    print('else');
                    return Container(

                      width: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Center(
                        child: Text('',style: TextStyle(color: appSecondaryColor,fontSize: 18),),
                      ),
                    );
                  }
                },
              ),*/

            ],
          ),
          isLoading?LinearProgressIndicator():Container(),
        ],
      ),
    );
  }

  void showDownloadProgress(received, total) {
    print('re: ${received} tot: ${total}');
    print((received / total * 100).toStringAsFixed(0) + "%");
    if (total != -1) {

    }
  }
}

class KitBagCard extends StatefulWidget{

  KitBagResponse kitBagResponse;


  KitBagCard({this.kitBagResponse});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return KitBagCardState();
  }
}
class KitBagCardState extends State<KitBagCard>{


  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return getMainContent(context, widget.kitBagResponse);
  }


  Widget getMainContent(BuildContext context,KitBagResponse kitBagResponse){


    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        children: [
          Row(
            children: [
              Container(

                width: 40,
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Center(
                  child: Icon(Icons.report,color: Colors.yellow[800],size: 30.0,),
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  color: Colors.white,
             //   width: 200,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 20, 20,0),
                        child: Text('FileName: ${kitBagResponse.fileName}',style:TextStyle(
                            color: Colors.black,
                            fontSize: 18

                        ) ,),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 10, 20,10),
                        child: Text('Extension: ${kitBagResponse.extension}',style:TextStyle(
                            color: Colors.black,
                            fontSize: 18

                        ) ,overflow: TextOverflow.ellipsis,),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 0, 0,10),
                        child: Text('${kitBagResponse.url}',style:TextStyle(
                            color: Colors.black,
                            fontSize: 18

                        ) ,overflow: TextOverflow.ellipsis,),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  color: Colors.white,
                 // width: 100,
                  child: FutureBuilder<String>(future: Api().checkPdf(kitBagResponse.url),

                    builder: (context,snapshot){

                      if(snapshot.connectionState==ConnectionState.none && snapshot.data==null){
                        return Container();
                      }
                      if(snapshot.hasData){

                        return InkWell(
                          onTap: () async {
                            if(snapshot.data.isNotEmpty){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (context) => PDFReaderScreen(filePath: snapshot.data,)));

                            }else{
                              Api().downloadEbook(kitBagResponse.url,showDownloadProgress).then((value) => {
                                setState((){
                                  isLoading = false;
                                }),
                              });
                              setState((){
                                isLoading = true;
                              });
                            }
                          },
                          child: Container(

                             // width: 100,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              child:
                              Center(
                                child: Text(!snapshot.data.isEmpty?'open':'Download',style: TextStyle(color: appSecondaryColor,fontSize: 18),),
                              )

                          ),
                        );

                      }
                      return Container();
                    },
                  ),
                ),
              ),

            ],
          ),
          isLoading?Theme(
            data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
            child: new LinearProgressIndicator(),
          ):Container(),
        ],
      ),
    );
  }

  void showDownloadProgress(received, total) {
    print('re: ${received} tot: ${total}');
    print((received / total * 100).toStringAsFixed(0) + "%");
    if (total != -1) {

    }
  }
}
