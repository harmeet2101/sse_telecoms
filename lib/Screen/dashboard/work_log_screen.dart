import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Modal/BlockageClearanceJobs.dart';
import 'package:RouteProve/Screen/Defect/DefectScreen.dart';
import 'package:RouteProve/Screen/FibreInstall/FibreInstallScreen.dart';
import 'package:RouteProve/Screen/blockagClearance/blockage_list_screen.dart';
import 'package:RouteProve/Screen/blockage_clearance_scr.dart';
import 'package:RouteProve/Screen/blockage_locator_scr.dart';
import 'package:RouteProve/Screen/login_scr.dart';
import 'package:RouteProve/Widgets/appBarWidget.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WorkLogScreen extends StatefulWidget{

  int jobId;


  WorkLogScreen({this.jobId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return WorkLogState();
  }
}

class WorkLogState extends State<WorkLogScreen>{

  double screenHeight;
  double screenWidth;
  int noOfBlocClerances =0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return getScaffold(
      context,
      Scaffold(
        appBar: getActionbar(
            context,
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(flex: 1,),
                RawMaterialButton(
                    child: Text("Logout",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 18,
                            color: appBackgroundColor,
                            fontWeight: FontWeight.w600)),
                    onPressed: () {
                      logoutTapped();
                    }),
              ],
            )),
        body: SafeArea(
          child:FutureBuilder<dynamic>(builder:(BuildContext context, AsyncSnapshot<dynamic> snapshot){

            if(snapshot.hasData){

              if(snapshot.data[8] is List<BlockageClearanceJobs>){
                List<BlockageClearanceJobs> ls = snapshot.data[8];
                noOfBlocClerances = ls.length;
              }
              return getMainContent(/*viewportConstraints.maxHeight*/);

            }
            else return Stack(
              children: [
                getMainContent(/*viewportConstraints.maxHeight*/),
                new Opacity(
                  opacity: 0.3,
                  child: const ModalBarrier(
                      dismissible: false, color: Colors.grey),
                ),
                new Center(
                  child: Theme(
                    data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
                    child: new CircularProgressIndicator(),
                  )
                ),
              ],
            );


          } ,future:Future.wait([
            Api().getSpeedLimits(),
            Api().getTmTypes(),
            Api().getBoreFull(),
            Api().getCableTypes(),
            Api().getCableInSitu(),
            Api().getMaterialTypes(),
            Api().getSurfaceTypes(),
            Api().getDefectTypes(),
            Api().getBlockageClearanceJobs(),
          ])),
        ),
      ),
    );
  }

  Widget getMainContent(/*double height*/){
    return SingleChildScrollView(
      child:Padding(
        padding: EdgeInsets.only(left: 20, right: 20, top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            buildCard(
                titile: "Blockage Locate",
                image: "assets/blockage_locate.png",
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              BlockageLocatorScreen(jobId: widget.jobId,)));
                },flag: false,boxBackground: appThemeColor),
            SizedBox(height: 10),
            buildCard(
                titile: "Blockage Clearance",
                image: "assets/blocage_clearance.png",
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              BlockageListScreen()));
                },flag: true,boxBackground: appThemeColor),
            SizedBox(height: 10),
            buildCard(
                titile: "Fibre Installation",
                image: "assets/app_logo.png",
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              FibreInstallScreen(jobId: widget.jobId,)));
                },flag: false,boxBackground: appThemeColor),SizedBox(height: 10),
            buildCard(
                titile: "1024 Defect",
                image: "assets/app_logo.png",
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DefectScreen(jobId: widget.jobId,)));
                },flag: false,boxBackground: appThemeColor)
          ],
        ),
      ),
    );
  }

  buildCard(
      {String titile = "Card",
        void Function() onTap,
        String image = "assets/blockage_locate.png",
        Icon sideIcon = const Icon(
          Icons.arrow_forward_ios,
          color: appSecondaryColor,
          size: 20,
        ),bool flag,Color boxBackground}) {
    return Container(
      height: 80,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 20,
            right: 0,
            bottom: 0,
            child: Card(
              child: FlatButton(
                padding: EdgeInsets.all(0),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 10, 10, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        titile,
                        style: TextStyle(
                            fontSize: 16,
                            color: appSecondaryColor,
                            fontWeight: FontWeight.w600),
                      ),
                      Spacer(flex: 1,),
                      ( flag && noOfBlocClerances>0)?Container(
                        width: 26,
                        height: 26,
                        child: Center(
                          child: Text('${noOfBlocClerances}',
                            style: TextStyle(color: Colors.white,fontSize: 14),),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          shape: BoxShape.circle,
                        ),
                      ):Container(),
                      SizedBox(width: 10,),
                      sideIcon
                    ],
                  ),
                ),
                onPressed: onTap,
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  color: boxBackground, borderRadius: BorderRadius.circular(5)),
              clipBehavior: Clip.hardEdge,
              padding: EdgeInsets.all(5),
              child: Image.asset(image,color: appSecondaryColor,),
            ),
          )
        ],
      ),
    );
  }

  logoutTapped() async {
    await Constants.clearAllUserDefaults();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen()),
            (route) => false);
  }
}
