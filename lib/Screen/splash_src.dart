import 'dart:async';
import 'dart:io';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/Screen/dashboard/home_screen.dart';
import 'package:RouteProve/Screen/login_scr.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    navigareDashBoard();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: appThemeColor,
      body: Container(
        /*decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/splash_background.png"),
            fit: BoxFit.cover,
          ),
        ),*/
        padding: EdgeInsets.only(left: 60, right: 60),
        alignment: Alignment.center,
        child: Image.asset(
          "assets/splash_logo.png",
          color: appBackgroundColor,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  navigareDashBoard() async {
    if (Platform.isAndroid) {
      PermissionStatus permissionCheck = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.storage);

      if (PermissionStatus.granted == permissionCheck) {
        Timer(Duration(seconds: 1), () async {
          await FileHandler.initLocalPath();
          goToNextScreen();
        });
      } else {
        await PermissionHandler().requestPermissions([PermissionGroup.storage]);
        await FileHandler.initLocalPath();
        goToNextScreen();
      }
    } else {
      await FileHandler.initLocalPath();
      goToNextScreen();
    }
  }

  goToNextScreen() async {
    bool isLoggedIn = await Constants.getLoginStatus();
    if (isLoggedIn) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen0()));
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LoginScreen()));
    }
  }
}
