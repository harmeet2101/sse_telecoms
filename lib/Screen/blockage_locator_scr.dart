import 'dart:typed_data';

import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Data/AppInitDataProvider.dart';
import 'package:RouteProve/Data/DBProvider.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/FormEngine/FormController.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:RouteProve/FormEngine/FormPage.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/Modal/BlockageLocateResponse.dart';
import 'package:RouteProve/Modal/DataSet.dart';
import 'package:RouteProve/Modal/PostBlockageLocate.dart';
import 'package:RouteProve/Modal/dataset/work_point.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:uuid/uuid.dart';
class BlockageLocatorScreen extends StatefulWidget {

  int jobId;


  BlockageLocatorScreen({this.jobId});

  @override
  State<StatefulWidget> createState() {
    return BlockageLocatorScreenState();
  }
}

class BlockageLocatorScreenState extends State<BlockageLocatorScreen> {
  double screenHeight;
  double screenWidth;
  BlockageLocation blockageLocation = BlockageLocation();
  String initialDuctValue;
  int currentPage = 0;
  bool isPrivateSwitchSelected = false;
  List<String> images = [
    "assets/add_a_job.png",
    "assets/traffic_management.png",
    "assets/cable_surface_type.png",
    "assets/mark_blockage.png",
    "assets/mark_blockage.png",
    "assets/jointbox.png",
    "assets/jointbox.png",
  ];
  List<ChoiceItem> wp1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return FutureBuilder<List<WorkPoint>>(builder:(BuildContext context, AsyncSnapshot<List<WorkPoint>> snapshot){

      if(snapshot.hasData){

         wp1 = new List<ChoiceItem>();

        for(int i =0;i<snapshot.data.length;i++){

          wp1.add(new ChoiceItem(key: snapshot.data[i].jobWorkPointId,
              value: 'Work Point ${snapshot.data[i].rowNum}-'
                  '${snapshot.data[i].plantItem} (${(snapshot.data[i].categoryName==CategoryName.BURIED)?'BURIED':'JOINTING CHAMBER'})'));
    }

        return FormController(
          formId: widget.jobId.toString(),
          formName: "BlockageLocate",
          formTitle: "Blockage Locate",
          appBarImage: images[currentPage],
          isSubTitleHidden: false,
          isProgressBarHidden: true,
          pages: getFormPages(),
          didFetchSavedAnswers: (data) {
            if (data["markBlockage"] != null) {
              blockageLocation = BlockageLocation.fromJson(data["markBlockage"]);
              setState(() {});
            } else {
              if (data["numberOfBlockage"] != null &&
                  data["sectionLength"] != null) {
                int intValue = int.parse(data["numberOfBlockage"].split("_").last);
                double doubleValue = double.parse(data["sectionLength"]);
                blockageLocation.numberOfBlockages = intValue;
                blockageLocation.sectionLength = doubleValue;
                blockageLocation.blockageAtA = 0.0;
                blockageLocation.blockageAtB = 0.0;
                setState(() {});
              } else if (data["sectionLength"] != null) {
                double doubleValue = double.parse(data["sectionLength"]);
                blockageLocation.sectionLength = doubleValue;
                blockageLocation.blockageAtA = 0.0;
                blockageLocation.blockageAtB = 0.0;
                setState(() {});
              }
            }

            if (data["ductsForJointboxA"] != null) {
              initialDuctValue = data["ductsForJointboxA"];
              setState(() {});
            }
          },
          didAssignValue: (key, value) {
            //print("Key = $key\nValue = $value");
            if (key == "markBlockage") {
              if (value != null) {
                blockageLocation = BlockageLocation.fromJson(value);
                setState(() {});
              }
            }

            if (key == "numberOfBlockage") {
              if (value != null) {
                int intValue = int.parse(value.split("_").last);
                blockageLocation.numberOfBlockages = intValue;
                blockageLocation.blockageAtA = 0.0;
                blockageLocation.blockageAtB = 0.0;
                setState(() {});
              }
            }

            if (key == "sectionLength") {
              if (value != null) {
                double doubleValue = double.parse(value);
                blockageLocation.sectionLength = doubleValue;
                blockageLocation.blockageAtA = 0.0;
                blockageLocation.blockageAtB = 0.0;
                setState(() {});
              }
            }

            if (key == "ductsForJointboxA") {
              if (value != null) {
                initialDuctValue = value;
                setState(() {});
              }
            }
            if (key == "private") {
              if (value != null) {
                isPrivateSwitchSelected = value;
                // setState(() {});
              }
            }
          },
          onSubmit: () {
              submitTapped();
          },
          changeInPageIndex: (int pageIndex) {
            currentPage = pageIndex;
            setState(() {});
          },
        );

      }
      else return Stack(
        children: [
          new FormController(
        formId: widget.jobId.toString(),
        formName: "BlockageLocate",
        formTitle: "Blockage Locate",
        appBarImage: images[currentPage],
        isSubTitleHidden: false,
        isProgressBarHidden: true,
        pages: getFormPages(),
        didFetchSavedAnswers: (data) {
          if (data["markBlockage"] != null) {
            blockageLocation = BlockageLocation.fromJson(data["markBlockage"]);
            setState(() {});
          } else {
            if (data["numberOfBlockage"] != null &&
                data["sectionLength"] != null) {
              int intValue = int.parse(data["numberOfBlockage"].split("_").last);
              double doubleValue = double.parse(data["sectionLength"]);
              blockageLocation.numberOfBlockages = intValue;
              blockageLocation.sectionLength = doubleValue;
              blockageLocation.blockageAtA = 0.0;
              blockageLocation.blockageAtB = 0.0;
              setState(() {});
            } else if (data["sectionLength"] != null) {
              double doubleValue = double.parse(data["sectionLength"]);
              blockageLocation.sectionLength = doubleValue;
              blockageLocation.blockageAtA = 0.0;
              blockageLocation.blockageAtB = 0.0;
              setState(() {});
            }
          }

          if (data["ductsForJointboxA"] != null) {
            initialDuctValue = data["ductsForJointboxA"];
            setState(() {});
          }
        },
        didAssignValue: (key, value) {
          //print("Key = $key\nValue = $value");
          if (key == "markBlockage") {
            if (value != null) {
              blockageLocation = BlockageLocation.fromJson(value);
              setState(() {});
            }
          }

          if (key == "numberOfBlockage") {
            if (value != null) {
              int intValue = int.parse(value.split("_").last);
              blockageLocation.numberOfBlockages = intValue;
              blockageLocation.blockageAtA = 0.0;
              blockageLocation.blockageAtB = 0.0;
              setState(() {});
            }
          }

          if (key == "sectionLength") {
            if (value != null) {
              double doubleValue = double.parse(value);
              blockageLocation.sectionLength = doubleValue;
              blockageLocation.blockageAtA = 0.0;
              blockageLocation.blockageAtB = 0.0;
              setState(() {});
            }
          }

          if (key == "ductsForJointboxA") {
            if (value != null) {
              initialDuctValue = value;
              setState(() {});
            }
          }
          if (key == "private") {
            if (value != null) {
              isPrivateSwitchSelected = value;
              // setState(() {});
            }
          }
        },
        onSubmit: () {

          submitTapped();
        },
        changeInPageIndex: (int pageIndex) {
          currentPage = pageIndex;
          setState(() {});
        },
      ),
          new Opacity(
            opacity: 0.3,
            child: const ModalBarrier(
                dismissible: false, color: Colors.grey),
          ),
          new Center(
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
              child: new CircularProgressIndicator(),
            ),
          ),
        ],
      );


    } ,future:Api().getWorkPoints(widget.jobId));
  }

  List<FormPage> getFormPages() {
    List<FormPage> pages = List<FormPage>();

    pages.add(
      FormPage(
        title: "Find/Add a Job",
        progress: 0.15,
        items: <FormItem>[
/*          FormItem(
              label: "Job Id",
              key: "jobId",
              isRequired: true,
              type: FormViewType.Small_Input,),*/
          FormItem(
              label: "Work Point 1",
              key: "workPointA",
              isRequired: true,
              type: FormViewType.Single_Choice,
              choiceItems: wp1),
          FormItem(
              label: "Work Point 2",
              key: "workPointB",
              isRequired: true,
              type: FormViewType.Single_Choice,
              choiceItems: wp1),
          FormItem(
              label: "Splitter Node/PON no.",
              key: "splitterNode",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.text),
          FormItem(
              label: "Duct Section",
              key: "ductSection",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.text),
          FormItem(
              label: "Section Length (m)",
              key: "sectionLength",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.number),
          FormItem(
            label: "Percentage Bore Full",
            key: "percentageBoreFullId",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getPercetageBoreFill(),
          ),
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Traffic Management",
        progress: 0.30,
        items: <FormItem>[
          FormItem(
            label: "TM Type",
            key: "trafficManagementTypeId",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getTrafficManagmentType(),
          ),
          FormItem(
            label: "TM Comments",
            key: "trafficManagementComment",
            isRequired: true,
            type: FormViewType.Large_Input,
          ),
          FormItem(
            label: "Red Route / Bus Lane",
            key: "redRoute",
            isRequired: true,
            type: FormViewType.Switch,

          ),
          FormItem(
            label: "Private",
            key: "private",
            isRequired: true,
            type: FormViewType.Switch,
          ),
          FormItem(
            label: "Contact Details",
            key: "contactDetails",
            isRequired: true,
            type: FormViewType.Large_Input,
            isHidden: !isPrivateSwitchSelected
          ),
          FormItem(
            label: "Speed Limit",
            key: "speedLimitId",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getSpeedLimitType(),
          ),
          FormItem(
            label: "Click a Photo",
            key: "trafficManagementPhotoFileBytes",
            isRequired: true,
            type: FormViewType.Camera,
            minRequired: 1,
            maxRequired: 1,
          ),
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Cable & Surface Type",
        progress: 0.45,
        items: <FormItem>[
          FormItem(
            label: "Cable Type",
            key: "cableTypeId",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getCableType(),
          ),
          FormItem(
            label: "Number of Cables in situ",
            key: "noCables",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getCableSituType(),
          ),
          FormItem(
            label: "Number of Blockages",
            key: "numberOfBlockage",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: [
              ChoiceItem(key: 1, value: "1"),
              ChoiceItem(key: 2, value: "2"),
            ],
          ),
          FormItem(
            label: blockageLocation.numberOfBlockages == 1
                ? "Surface Type - Blockage"
                : "Surface Type - Blockage A",
            key: "surfaceType1",
            isRequired: true,
            isHidden: blockageLocation.numberOfBlockages == null,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getSurfaceType(),
          ),
          FormItem(
            label: "Surface Type - Blockage B",
            key: "surfaceType2",
            isRequired: true,
            isHidden: blockageLocation.numberOfBlockages != 2,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getSurfaceType(),
          ),
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Mark Blockage",
        progress: 0.60,
        items: <FormItem>[
          FormItem(
            label: "Mark Blockage",
            key: "markBlockage",
            isRequired: true,
            type: FormViewType.Mark_Blockage,
            blockageLocation: blockageLocation.toJson(),
          ),
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Mark Blockage",
        progress: 0.75,
        items: <FormItem>[
          FormItem(
            label: "Mark Blockage",
            key: "blockagePhotos",
            isRequired: true,
            type: FormViewType.Mark_Blockage_Photo,
            blockageLocation: blockageLocation.toJson(),
          ),
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Duct & Blockage Bores",
        progress: 0.87,
        items: <FormItem>[
          FormItem(
            label: "Ducts required for Jointbox A",
            key: "ductsForJointboxA",
            isRequired: true,
            type: FormViewType.JointboxA,
          ),
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Duct & Blockage Bores",
        progress: 1.00,
        items: <FormItem>[
          FormItem(
              label: "Ducts required for Jointbox B",
              key: "ductsForJointboxB",
              isRequired: true,
              type: FormViewType.JointboxB,
              initialDuctValue: initialDuctValue),
          FormItem(
            label: "Comments",
            key: "comments",
            isRequired: true,
            type: FormViewType.Large_Input,
          ),
        ],
      ),
    );

    return pages;
  }

  submitTapped() async {
    bool temp = await DBProvider.db.getAnswerCompletedStatus(widget.jobId.toString(), 'BlockageLocate');
   // bool temp2 = await DBProvider.db.getAnswerUploadStatus('BlockageLocate', 'BlockageLocate');
    print('comp: ${temp}');

    await DBProvider.db.changeAnswerCompletedStatus(widget.jobId.toString(), 'BlockageLocate', true);

    Map<String, dynamic> savedAnswers = await DBProvider.db.getAnswer(widget.jobId.toString(), 'BlockageLocate');
    String authToken = await Constants.getAuthToken();
     savedAnswers.forEach((key, value) {
      print('${key}: ${value}');
    });

    Map<String, dynamic> markBlockage  =  savedAnswers['markBlockage'];
    Map<String, dynamic> blockagePhotos  =  savedAnswers['blockagePhotos'];
    Map<String, dynamic> jointBoxB  =  blockagePhotos['jointBoxB'];
    Map<String, dynamic> jointBoxA  =  blockagePhotos['jointBoxA'];
    List<dynamic> blokageA  =  blockagePhotos['blockageAtA'];
    List<dynamic> blokageB  =  blockagePhotos['blockageAtB'];

    PostBlockageLocate postBlockageLocate = new PostBlockageLocate(

        splitterNode: savedAnswers['splitterNode'],
        ductSection: savedAnswers['ductSection'],
        sectionLength: int.parse(savedAnswers['sectionLength']),
        boreFullId: int.parse(savedAnswers['percentageBoreFullId'].toString().split("_").last),
        trafficManagementId: int.parse(savedAnswers['trafficManagementTypeId'].toString().split("_").last),
        tmComment: savedAnswers['trafficManagementComment'],
        redRoute: savedAnswers['redRoute'],
        private: savedAnswers['private'],
        contactDetails: '12345',
        speedLimitId: int.parse(savedAnswers['speedLimitId'].toString().split("_").last),
        cableTypeId: int.parse(savedAnswers['cableTypeId'].toString().split('_').last),
        cablesInSituId: int.parse(savedAnswers['noCables'].toString().split("_").last),
        surfaceTypeAid: int.parse(savedAnswers['surfaceType1'].toString().split('_').last),
        surfaceTypeBid: int.parse(savedAnswers['surfaceType2'].toString().split('_').last),
        jobWorkPointAid: int.parse(savedAnswers['workPointA'].toString().split('_').last),
        jobWorkPointBid: int.parse(savedAnswers['workPointB'].toString().split('_').last),
        numberOfBlocks: int.parse(savedAnswers['numberOfBlockage'].toString().split("_").last),
        submissionId: Uuid().v4(),
        jobId: widget.jobId,
        ductPattern: '${savedAnswers['ductsForJointboxA']}',
        blockageADistance: double.parse(markBlockage['blockageAtA'].toString()).toInt(),
        blockageALat: double.parse(blokageA[0]['lat'].toString()).toInt(),
        blockageALng: double.parse(blokageA[0]['lon'].toString()).toInt(),
        blockageALocation: blokageA[0]['address'],
        blockageBDistance: double.parse(markBlockage['blockageAtB'].toString()).toInt(),
        blockageBLat: double.parse(blokageB[0]['lat'].toString()).toInt(),
        blockageBLng: double.parse(blokageB[0]['lon'].toString()).toInt(),
        blockageBLocation: blokageB[0]['address'],
        jointBoxALat: double.parse(jointBoxA['photoOfArea']['lat'].toString()).toInt(),
        jointBoxALng: double.parse(jointBoxA['photoOfArea']['lon'].toString()).toInt(),
        jointBoxALocation: jointBoxA['photoOfArea']['address'].toString(),
        jointBoxBLat: double.parse(jointBoxB['photoOfArea']['lat'].toString()).toInt(),
        jointBoxBLng: double.parse(jointBoxB['photoOfArea']['lon'].toString()).toInt(),
        jointBoxBLocation: jointBoxB['photoOfArea']['address'].toString(),
        authToken: authToken

    );


    BlockageLocateResponse  resp = await Api().submitBlockageLocate(postBlockageLocate.toJson());

    if(resp!=null){
      showAlertMessage(
          message: "Submit Successful",
          context: context,
          type: AlertType.Success);
      DBProvider.db.changeAnswerUploadStatus(widget.jobId.toString(), 'BlockageLocate', true);
    }else{
      print('else');
      DBProvider.db.changeAnswerUploadStatus(widget.jobId.toString(), 'BlockageLocate', false);
    }
    uploadBlockPhotos(blokageA,resp,authToken,widget.jobId);
    uploadBlockPhotos(blokageB,resp,authToken,widget.jobId);
    uploadJointBoxPhoto(jointBoxA, resp, authToken, widget.jobId);
    uploadJointBoxPhoto(jointBoxB, resp, authToken, widget.jobId);
  }

  Future<String> getTMPhoto(List<dynamic>photos){

      return FileHandler.readFile('${FileHandler.localPath}/${photos[0]['path']}');

  }

  Future<void> uploadBlockPhotos(List<dynamic>photos,BlockageLocateResponse response,String authToken,int jobId)async{

    for(int i =0;i<photos.length;i++){

      String str = await FileHandler.readFile('${FileHandler.localPath}/${photos[i]['path']}');
      Map<String,dynamic> mp ={"Photo":str,"A55ID":response.a55Id,"AuthToken":authToken,"JobID":jobId};
      await Api().uploadImage(mp);
    }
  }

  Future<void> uploadJointBoxPhoto(Map<String, dynamic> jointBox,BlockageLocateResponse response,String authToken,int jobId)async{


    if(jointBox!=null && jointBox['photoOfArea']!=null){
      String str = await FileHandler.readFile('${FileHandler.localPath}/${jointBox['photoOfArea']['path']}');
      Map<String,dynamic> mp ={"Photo":str,"A55ID":response.a55Id,"AuthToken":authToken,"JobID":jobId};
      await Api().uploadImage(mp);
    }
    if(jointBox!=null && jointBox['photoOfBox']!=null){
      String str = await FileHandler.readFile('${FileHandler.localPath}/${jointBox['photoOfBox']['path']}');
      Map<String,dynamic> mp ={"Photo":str,"A55ID":response.a55Id,"AuthToken":authToken,"JobID":jobId};
      await Api().uploadImage(mp);
    }
  }
}
