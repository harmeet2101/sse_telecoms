import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Modal/UserResponse.dart';
import 'package:RouteProve/Screen/dashboard/home_screen.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:RouteProve/Widgets/progressHud.dart';
import 'package:flutter/material.dart';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/app_string.dart';
import 'package:RouteProve/Widgets/appBarWidget.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  double screenHeight;
  double screenWidth;
  String version = '';
  bool isLoading = false;
  bool isHidePassword = true;
  IconData hidePasswordIcon = Icons.remove_red_eye;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController(text: "");
  TextEditingController passwordController = TextEditingController(text: "");
  FocusNode passwordNode = FocusNode();

  @override
  void initState() {
    super.initState();
    fetchVersionName();
  }

  @override
  void dispose() {
    super.dispose();
  }

  fetchVersionName() async {
    version = await Constants.fetchAppVersion();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return ProgressHUD(
      inAsyncCall: isLoading,
      child: Container(
        color: Colors.grey[200],
        child: Scaffold(
          appBar: getActionbar(
              context,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[],
              )),
          body:LayoutBuilder(builder:
              (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
              child: Container(
                color: Colors.transparent,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Container(width: screenWidth*0.7,height: 200,
                        decoration: BoxDecoration(

                      image: DecorationImage(
                        image: AssetImage("assets/blocloc_sign_in.png"),
                        fit: BoxFit.contain,
                      )),),
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: viewportConstraints.maxHeight,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[

                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                blocloc,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 60,
                                  color: appThemeColor,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                a_blocker_locater,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: appThemeColor,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Text(
                                sign_in,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 22,
                                  color: appThemeColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          loginForm(),
                          Text(
                            "Version " + version,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: appTextColor,
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  loginForm() {
    return Theme(
      data: ThemeData(primaryColor: appThemeColor),
      child: Padding(
        padding: EdgeInsets.only(left: 30, right: 30, bottom: 30),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                  ),
                  child: TextFormField(
                    style: TextStyle(
                      fontSize: 16,
                    ),
                    controller: emailController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Username",
                      prefixIcon: Icon(Icons.person,color: appSecondaryColor,),
                    ),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.emailAddress,
                    onFieldSubmitted: (String value) {
                      FocusScope.of(context).requestFocus(passwordNode);
                    },
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                  ),
                  child: TextFormField(
                    style: TextStyle(
                      fontSize: 16,
                    ),
                    controller: passwordController,
                    focusNode: passwordNode,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Password",
                      prefixIcon: Icon(Icons.lock,color: appSecondaryColor,),
                      suffixIcon: new IconButton(
                          icon: new Icon(hidePasswordIcon),
                          onPressed: () {
                            hidePassword();
                          }),
                    ),
                    textInputAction: TextInputAction.done,
                    obscureText: isHidePassword,
                    onFieldSubmitted: (String value) {
                      FocusScope.of(context).unfocus();
                      loginButtonPressed();
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10, right: 10, bottom: 20),
                child: Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    onTap: () {
                      forgotPassowrdPressed();
                    },
                    child: RichText(
                        text: TextSpan(
                            text: forgot_pass,
                            style: TextStyle(
                                fontSize: 14, color: appSecondaryColor))),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                height: 45,
                width: screenWidth - 60,
                child: RaisedButton(
                  color: appThemeColor,
                  textColor: appBackgroundColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                  ),
                  child: RichText(
                      text: TextSpan(
                          text: login,
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: appBackgroundColor))),
                  onPressed: () {
                    loginButtonPressed();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  hidePassword() {
    if (isHidePassword) {
      isHidePassword = false;
      hidePasswordIcon = Icons.visibility_off;
    } else {
      isHidePassword = true;
      hidePasswordIcon = Icons.remove_red_eye;
    }
    setState(() {});
  }

  loginButtonPressed() async {
    String email = emailController.text.trim();
    String password = passwordController.text.trim();
    if (validateLoginRequest(email, password)) {
      updateLoadingStatus(true);
      var response = await Api().login(email, password);
      if (response != null) {
        if (response!= null) {
          //Update Keychain
          await Constants.setUsername(email ?? "");
          await Constants.setPassword(password ?? "");
          // Update Shared Prefernces
          await Constants.setLoginStatus(true);
          await Constants.setUser(response?? UserResponse());
          await Constants.setUserID(response.userId.toString() ?? "");
          await Constants.setAuthToken(response.authToken?? "");
          await Constants.setJwtToken(response.token ?? "");
          goToHomeScreen();
        } else {
          showAlertMessage(
              message:"Failed to login now, Please try again later.",
              type: AlertType.Failure,
              context: context);
        }
      } else {
        showAlertMessage(
              message: "Failed to login now, Please try again later.",
              type: AlertType.Failure,
              context: context);
      }
      updateLoadingStatus(false);
    } else {
      showAlertMessage(
              message: "Please enter valid username and password.",
              type: AlertType.Failure,
              context: context);

    }
  }

  bool validateLoginRequest(String email, String password) {
    if (email == "" || password == "") {
      return false;
    } else {
      return true;
    }
  }

  goToHomeScreen() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => HomeScreen0()));
  }

  forgotPassowrdPressed() async {
    const url = 'mailto:support@depotnet.co.uk?subject=Forgotten Password';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  updateLoadingStatus(bool loading) {
    setState(() {
      isLoading = loading;
    });
  }
}
