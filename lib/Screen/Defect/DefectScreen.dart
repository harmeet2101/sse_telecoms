import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Data/AppInitDataProvider.dart';
import 'package:RouteProve/Data/DBProvider.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/FormEngine/FormController.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:RouteProve/FormEngine/FormPage.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:RouteProve/Modal/DefectResponse.dart';
import 'package:RouteProve/Modal/PostDefect.dart';
import 'package:RouteProve/Modal/PostDefectRequest.dart';
import 'package:RouteProve/Modal/dataset/work_point.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class DefectScreen extends StatefulWidget{

  int jobId;
  DefectScreen({this.jobId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DefectState();
  }
}

class DefectState extends State<DefectScreen>{

  int currentPage = 0;
  List<String> images = [
    "assets/app_logo.png",
    "assets/app_logo.png",
    "assets/app_logo.png",
  ];
  List<ChoiceItem> wp1;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<List<WorkPoint>>(builder:(BuildContext context, AsyncSnapshot<List<WorkPoint>> snapshot){

      if(snapshot.hasData){

        wp1 = new List<ChoiceItem>();

        for(int i =0;i<snapshot.data.length;i++){

          wp1.add(new ChoiceItem(key: snapshot.data[i].jobWorkPointId,
              value: 'Work Point ${snapshot.data[i].rowNum}-'
                  '${snapshot.data[i].plantItem} (${(snapshot.data[i].categoryName==CategoryName.BURIED)?'BURIED':'JOINTING CHAMBER'})'));
        }

        return FormController(
          formId: widget.jobId.toString(),
          formName: "Defect1024",
          formTitle: "1024 Defect",
          appBarImage: images[currentPage],
          isSubTitleHidden: false,
          isProgressBarHidden: true,
          pages: getFormPages(),
          didFetchSavedAnswers: (data) {


          },
          didAssignValue: (key, value) {

          },
          onSubmit: () {
            getSavedAnswersFromDB();
//        submitTapped();
          },
          changeInPageIndex: (int pageIndex) {
            currentPage = pageIndex;
            setState(() {});
          },
        );

      }
      else return Stack(
        children: [
          new FormController(
        formId: widget.jobId.toString(),
        formName: "Defect1024",
        formTitle: "1024 Defect",
        appBarImage: images[currentPage],
        isSubTitleHidden: false,
        isProgressBarHidden: true,
        pages: getFormPages(),
        didFetchSavedAnswers: (data) {


        },
        didAssignValue: (key, value) {

        },
        onSubmit: () {
          getSavedAnswersFromDB();
//        submitTapped();
        },
        changeInPageIndex: (int pageIndex) {
          currentPage = pageIndex;
          setState(() {});
        },
      ),
          new Opacity(
            opacity: 0.3,
            child: const ModalBarrier(
                dismissible: false, color: Colors.grey),
          ),
          new Center(
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
              child: new CircularProgressIndicator(),
            ),
          ),
        ],
      );


    } ,future:Api().getWorkPoints(widget.jobId));
  }

  List<FormPage> getFormPages() {
    List<FormPage> pages = List<FormPage>();

    pages.add(
      FormPage(
        title: "",
        progress: 0.15,
        items: <FormItem>[
         /* FormItem(
              label: "Job Id",
              key: "jobId",
              isRequired: true,
              type: FormViewType.Single_Choice,
              choiceItems: AppInitDataProvider().getEstimatesType()),*/
          FormItem(
              label: "Work Point",
              key: "workPointA",
              isRequired: true,
              type: FormViewType.Single_Choice,
              choiceItems: wp1),
          FormItem(
            label: "Defect Type",
            key: "defectTypeId",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getDefectType(),
          ),

          FormItem(
            label: "Urgent / Non-Urgent",
            key: "urgent",
            isRequired: true,
            type: FormViewType.Switch,
          ),
          FormItem(
            label: "Build Impacting",
            key: "buildImpacting",
            isRequired: true,
            type: FormViewType.Switch,
          ),
          FormItem(
            label: "Cable Type",
            key: "cableTypeId",
            isRequired: true,
            type: FormViewType.Single_Choice,
            choiceItems: AppInitDataProvider().getCableType(),
          ),
          FormItem(
            label: "Address",
            key: "address_1",
            isRequired: true,
            type: FormViewType.Location,
          ),
          FormItem(
              label: "Section",
              key: "section",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.number),

        ],
      ),
    );

    pages.add(
      FormPage(
        title: "Evidence of Damage",
        progress: 0.30,
        items: <FormItem>[
          FormItem(
              label: '',
              key: 'defectPhoto',
              isRequired: false,
              type: FormViewType.DefectPhotoWidget
          )
        ],
      ),
    );

    pages.add(
      FormPage(
        title: "",
        progress: 1.0,
        items: <FormItem>[
          FormItem(
              label: "What is Damaged",
              key: "whatIsDamaged",
              isRequired: true,
              maxRequired: 4,
              minRequired: 1,
              type: FormViewType.Large_Input,
              dataType: FormFieldDataType.text),
          FormItem(
              label: "How it got Damaged",
              key: "howItGotDamaged",
              isRequired: true,
              maxRequired: 4,
              minRequired: 1,
              type: FormViewType.Large_Input,
              dataType: FormFieldDataType.text),
          FormItem(
              label: "Comments",
              key: "comments",
              isRequired: true,
              maxRequired: 4,
              minRequired: 1,
              type: FormViewType.Large_Input,
              dataType: FormFieldDataType.text),

        ],
      ),
    );

    return pages;
  }

  submitTapped() async {
    showAlertMessage(
        message: "Submit Successful",
        context: context,
        type: AlertType.Success);
    // var response = await Api.api
    //     .submitBlockageLocate("BlockageLocate", "BlockageLocate");
  }

  Future<Map<String, dynamic>> getSavedAnswersFromDB() async {
    Map<String, dynamic> savedAnswers =
    await DBProvider.db.getAnswer(widget.jobId.toString(), 'Defect1024');
    savedAnswers.forEach((key, value) {
      print('${key}: ${value}');
    });
    String authToken = await Constants.getAuthToken();

    PostDefect postDefect = new PostDefect(

      authToken:  authToken,
      jobId: widget.jobId,
      submissionId: Uuid().v4(),
      jobWorkPointId: int.parse(savedAnswers['workPointA'].toString().split('_').last),
      buildImpacting: savedAnswers['buildImpacting'],
      location: savedAnswers['address_1']['address'],
      addComments: savedAnswers['comments'],
      defectTypeId: int.parse(savedAnswers['defectTypeId'].toString().split('_').last),
      defectRef: '1024_defect',
      lat: savedAnswers['address_1']['lat'],
      lng: savedAnswers['address_1']['lon'],
      urgent: savedAnswers['urgent'],
      whatDamaged: savedAnswers['whatIsDamaged'],
      howDamaged: savedAnswers['howItGotDamaged'],

    );
    DefectResponse  resp = await Api().submitDefect(postDefect.toJson());

    print(resp);
    if(resp!=null){
      showAlertMessage(
          message: "Submit Successful",
          context: context,
          type: AlertType.Success);
      DBProvider.db.changeAnswerUploadStatus(widget.jobId.toString(), 'Defect1024', true);
    }else{
      DBProvider.db.changeAnswerCompletedStatus(widget.jobId.toString(), 'Defect1024', true);
      DBProvider.db.changeAnswerUploadStatus(widget.jobId.toString(), 'Defect1024', false);
    }


    try {
      Map<String, dynamic> defectPhotos  =  savedAnswers['defectPhoto'];
      Map<String, dynamic> defect_photo_1  =  defectPhotos['Defect Photo 1'];
      Map<String, dynamic> defect_photo_2  =  defectPhotos['Defect Photo 2'];
      print(defect_photo_1['path']);
      if(defect_photo_1!=null)
      uploadPhotos(defect_photo_1['path'], resp, authToken, widget.jobId);
      if(defect_photo_2!=null)
      uploadPhotos(defect_photo_2['path'], resp, authToken, widget.jobId);
    } on Exception catch (e) {
      // TODO
    }

    return savedAnswers;
  }

  Future<void> uploadPhotos(String path,DefectResponse response,String authToken,int jobId)async{

    String str = await FileHandler.readFile('${FileHandler.localPath}/${path}');
    Map<String,dynamic> mp ={"Photo":str,"defectId":response.defectId,"AuthToken":authToken,"JobID":jobId};
    await Api().uploadImage(mp);
/*    for(int i =0;i<photos.length;i++){

      String str = "/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABCAAD/4QNvaHR0cDov";//await FileHandler.readFile('${FileHandler.localPath}/${photos[i]['path']}');
      Map<String,dynamic> mp ={"Photo":str,"FibreInstallID":response.fibreInstallId,"AuthToken":authToken,"JobID":jobId};
      await Api().uploadImage(mp);
    }*/
  }
}