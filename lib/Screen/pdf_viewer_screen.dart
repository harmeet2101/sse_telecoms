import 'package:RouteProve/Common/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';


class PDFReaderScreen extends StatefulWidget {

  String filePath;
  PDFReaderScreen({@required this.filePath, Key key}) : super(key: key);
  @override
  PDFReaderScreenState createState() => PDFReaderScreenState();
}

class PDFReaderScreenState extends State<PDFReaderScreen> {


  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    if (widget.filePath != null && widget.filePath.isNotEmpty) {
      return PDFViewerScaffold(
        appBar: AppBar(
          title: Text('File Viewer'),
          backgroundColor: appThemeColor,
          centerTitle: true,
        ),
        path: widget.filePath,
      );
    } else{
      return Scaffold(
        body: Center(
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Text(
              'Error fetching ${widget.filePath}, Please try again later.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.red),
            ),
          ),
        ),
      );
    }

      }
    }
