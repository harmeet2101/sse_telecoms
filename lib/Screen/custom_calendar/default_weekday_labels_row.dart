import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CalendarroWeekdayLabelsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(child: Text("Mon", textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
        Expanded(child: Text("Tue", textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
        Expanded(child: Text("Wed", textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
        Expanded(child: Text("Thu", textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
        Expanded(child: Text("Fri", textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
        Expanded(child: Text("Sat", textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
        Expanded(child: Text("Sun", textAlign: TextAlign.center,style: TextStyle(color: Colors.white),)),
      ],
    );
  }
}