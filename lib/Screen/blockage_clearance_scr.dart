import 'package:RouteProve/Common/app_constants.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Data/AppInitDataProvider.dart';
import 'package:RouteProve/Data/DBProvider.dart';
import 'package:RouteProve/Data/FileHandler.dart';
import 'package:RouteProve/Modal/BlockageClearRequest.dart';
import 'package:RouteProve/Modal/BlockageClearanceJobs.dart';
import 'package:RouteProve/Modal/ClearanceJobs.dart';
import 'package:RouteProve/Widgets/alertWidgets.dart';
import 'package:flutter/material.dart';
import 'package:RouteProve/FormEngine/FormController.dart';
import 'package:RouteProve/FormEngine/FormItem.dart';
import 'package:RouteProve/FormEngine/FormPage.dart';
import 'package:RouteProve/FormEngine/FormStructs.dart';
import 'package:uuid/uuid.dart';

class BlockageClearanceScreen extends StatefulWidget {

  ClearanceJobs clearanceJobs;
  BlockageClearanceJobs data;

  BlockageClearanceScreen({this.data});


  @override
  State<StatefulWidget> createState() {
    return BlockageClearanceScreenState();
  }
}

class BlockageClearanceScreenState extends State<BlockageClearanceScreen> {
  double screenHeight;
  double screenWidth;
  int blockagesDug = 0;
  int currentPage = 0;

  @override
  void initState() {
    super.initState();
  //  blockagesDug = widget.clearanceJobs.noBlocs;
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return FormController(
      formId: widget.data.jobId.toString(),///"BlockageClearance",
      formName: "BlockageClearance",
      formTitle: "Blockage Clearance",
      appBarImage: "assets/blocage_clearance.png",
      isSubTitleHidden: currentPage == 0,
      isProgressBarHidden: true,
      pages: getFormPages(),
      didFetchSavedAnswers: (data) {
        String value = data["blockagesDug"];
        int intValue = int.parse(value);
        if (intValue != blockagesDug) {
          blockagesDug = intValue;
          setState(() {});
        }
      },
      didAssignValue: (key, value) {
        print("Key = $key\nValue = $value");
        if (key == "blockagesDug") {
          int intValue = int.parse(value);
          if (intValue != blockagesDug) {
            blockagesDug = intValue;
            setState(() {});
          }
        }
      },
      onSubmit: () {
        getSavedAnswersFromDB();
//        submitTapped();
      },
      changeInPageIndex: (int pageIndex) {
        currentPage = pageIndex;
        setState(() {});
      },
    );
  }

  List<FormPage> getFormPages() {
    List<FormPage> pages = List<FormPage>();

    pages.add(
      FormPage(
        items: <FormItem>[
          FormItem(
              label: "How many Blockages have you dug?",
              key: "blockagesDug",
              isRequired: true,
              type: FormViewType.Small_Input,
              dataType: FormFieldDataType.number),
        ],
      ),
    );

    if (blockagesDug > 0) {
      for (var pageNumber = 1; pageNumber < (blockagesDug + 1); pageNumber++) {
        String distanceMFromBoxTitle = "";
        if (pageNumber == 1) {
          distanceMFromBoxTitle = "Distance from Box A";
        } else {
          if (blockagesDug > pageNumber) {
            distanceMFromBoxTitle = "Distance from previous blockage";
          } else {
            distanceMFromBoxTitle = "Distance from Box B";
          }
        }

        pages.add(
          FormPage(
            title: "Blockage $pageNumber",
            items: <FormItem>[
              FormItem(
                label: "Blockage Outcome",
                key: "blockageOutcome_$pageNumber",
                isRequired: true,
                type: FormViewType.Single_Choice,
                choiceItems: [
                  ChoiceItem(key: 1, value: "Failed"),
                  ChoiceItem(key: 2, value: "Cleared"),
                ],
              ),
              FormItem(
                label: 'Identifier',
                key: "Identifier_$pageNumber",
                isRequired: true,
                type: FormViewType.Small_Input,
                dataType: FormFieldDataType.text,
              ),
              FormItem(
                label: distanceMFromBoxTitle,
                key: "blockagesDug_$pageNumber",
                isRequired: true,
                type: FormViewType.Small_Input,
                dataType: FormFieldDataType.number,
              ),
              FormItem(
                label: "R/S Measures",
                key: "rsMeasuresLabel_$pageNumber",
                isRequired: false,
                type: FormViewType.Title_Label,
              ),
              FormItem(
                label: "Length (mm) of R/S",
                key: "rsLength_$pageNumber",
                isRequired: true,
                type: FormViewType.Small_Input,
                dataType: FormFieldDataType.decimal,
              ),
              FormItem(
                label: "Width (mm) of R/S",
                key: "rsWidth_$pageNumber",
                isRequired: true,
                type: FormViewType.Small_Input,
                dataType: FormFieldDataType.decimal,
              ),
              FormItem(
                label: "Interim?",
                key: "interim_$pageNumber",
                isRequired: true,
                type: FormViewType.Switch,
              ),
              FormItem(
                label: "Surface Type",
                key: "surfaceType_$pageNumber",
                isRequired: true,
                type: FormViewType.Single_Choice,
                choiceItems: AppInitDataProvider().getSurfaceType(),
              ),
              FormItem(
                label: "Material Type",
                key: "materialType_$pageNumber",
                isRequired: true,
                type: FormViewType.Single_Choice,
                choiceItems: AppInitDataProvider().getMaterialType(),
              ),
              FormItem(
                label: "Address",
                key: "address_$pageNumber",
                isRequired: true,
                type: FormViewType.Location,
              ),
              FormItem(
                label: "Comments",
                key: "comments_$pageNumber",
                isRequired: true,
                type: FormViewType.Large_Input,
              ),
              FormItem(
                label: "You can take up to 20 photos, 1 being mandatory",
                key: "photos_$pageNumber",
                isRequired: true,
                type: FormViewType.Camera,
                minRequired: 1,
                maxRequired: 20,
              ),
            ],
          ),
        );
      }
    }

    return pages;
  }

  submitTapped() async {
    showAlertMessage(
        message: "Submit Successful",
        context: context,
        type: AlertType.Success);
    // var response = await Api.api
    //     .submitBlockageLocate("BlockageClearance", "BlockageClearance");
  }


  Future<Map<String, dynamic>> getSavedAnswersFromDB() async {
    Map<String, dynamic> savedAnswers =
    await DBProvider.db.getAnswer(widget.data.jobId.toString(), 'BlockageClearance');
   /* savedAnswers.forEach((key, value) {
      print('${key}: ${value}');
    });*/
    List<Block> blockageClears = new List();
    String authToken = await Constants.getAuthToken();

    for(int i =0;i<widget.data.numberOfBlockages;i++){

      blockageClears.add(Block(


        comments:i==0?savedAnswers['comments_1']:savedAnswers['comments_2'],
        distance:i==0?int.parse(savedAnswers['blockagesDug_1']):int.parse(savedAnswers['blockagesDug_2']),
        surfaceTypeId: i==0?int.parse(savedAnswers['surfaceType_1'].toString().split('_').last):
        int.parse(savedAnswers['surfaceType_2'].toString().split('_').last),
        materialTypeId: i==0?int.parse(savedAnswers['materialType_1'].toString().split('_').last):
        int.parse(savedAnswers['materialType_1'].toString().split('_').last),
        rsLength: i==0?int.parse(savedAnswers['rsLength_1']):int.parse(savedAnswers['rsLength_2']),
        rsWidth: i==0?int.parse(savedAnswers['rsWidth_1']):int.parse(savedAnswers['rsWidth_2']),
        blockageOutcome: i==0?(savedAnswers['blockageOutcome_1'].toString().split('_').first):
        (savedAnswers['blockageOutcome_2'].toString().split('_').first),
        interim: i==0?savedAnswers['interim_1']:savedAnswers['interim_2'],
        identifier: i==0?savedAnswers['Identifier_1']:savedAnswers['Identifier_2'],
        a55BlockageId: 0,
        blockageLat:i==0?savedAnswers['address_1']['lat']:savedAnswers['address_2']['lon'],
        blockageLng: i==0?savedAnswers['address_1']['lon']:savedAnswers['address_2']['lon']
      ));

    }
    BlockageClearRequest request = new BlockageClearRequest(

      blocks: blockageClears,
      submissionId: Uuid().v4(),
      authToken: authToken,
      a55Id: widget.data.a55Id,
      jobId: widget.data.jobId,
    );

    var resp = await Api().submitBlockageClearance(request.toJson());
    if(resp){

      DBProvider.db.changeAnswerUploadStatus(widget.data.jobId.toString(), 'BlockageClearance', true);

      showAlertMessage(
          message: "Submit Successful",
          context: context,
          type: AlertType.Success);
    }else{
      DBProvider.db.changeAnswerCompletedStatus(widget.data.jobId.toString(), 'BlockageClearance', true);
      DBProvider.db.changeAnswerUploadStatus(widget.data.jobId.toString(), 'BlockageClearance', false);
    }
    return savedAnswers;
  }

}
