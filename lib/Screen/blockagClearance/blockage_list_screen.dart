import 'package:RouteProve/Common/app_color.dart';
import 'package:RouteProve/Common/helper.dart';
import 'package:RouteProve/Data/Api.dart';
import 'package:RouteProve/Modal/BlockageClearanceJobs.dart';
import 'package:RouteProve/Modal/ClearanceJobs.dart';
import 'package:RouteProve/Screen/blockage_clearance_scr.dart';
import 'package:RouteProve/Widgets/appBarWidget.dart';
import 'package:RouteProve/Widgets/scaffoldWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BlockageListScreen extends StatefulWidget{


  BlockageListScreen();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return BlockageListState();
  }
}

class BlockageListState extends State<BlockageListScreen>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return getScaffold(context, Scaffold(

      appBar: getActionbar(
          context,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              RawMaterialButton(
                  child: Text("Blockage Clearance",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 16,
                          color: appBackgroundColor,
                          fontWeight: FontWeight.w600)),
                  onPressed: () {}),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    GestureDetector(
                      child: Icon(Icons.settings,
                        color: appBackgroundColor,),
                      onTap: (){

                      },),
                    SizedBox(width: 10,),
                    GestureDetector(
                      child: Icon(Icons.share,
                        color: appBackgroundColor,),
                      onTap: (){

                      },),
                  ],
                ),
              ),
            ],
          ),
          image: "assets/home_screen_logo.png"),

      body: FutureBuilder<List<BlockageClearanceJobs>>(
        builder: (BuildContext buildContext, AsyncSnapshot<List<BlockageClearanceJobs>> snapshot){
          if(snapshot.hasData){

            if(snapshot.data.length==0){
              return Container(
                child: Center(child: Text('Empty List'),),
              );
            }else
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 10),
              child: ListView.builder(itemBuilder: (BuildContext,count){

                return Card(
                    color: Colors.white,
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),

                    ),
                    child: InkWell(

                      onTap: (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    BlockageClearanceScreen(data: snapshot.data[count],)));
                      },
                      child: Container(
                        height: 120,
                        color: Colors.white,
                        child: Row(
                          children: [

                            Container(
                              color: Colors.white,
                              width: 120,
                              height: 120,
                              child: snapshot.data[count].numberOfBlockages>1?Stack(
                               children: [
                                 Align(alignment: Alignment.center,
                                     child: Image.asset("assets/red_cross_icon.png",width: 40,height: 40,)),
                                 Positioned(child: Image.asset("assets/red_cross_icon.png",width: 40,height: 40,),
                                 bottom: 20,right: 20,
                                 )
                               ],
                              ):Center(
                                child: Icon(Icons.cancel,color: Colors.red,size: 60.0,),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 20, 20,0),
                                  child: Text('Section:${snapshot.data[count].ductSection}',style:TextStyle(
                                      color: Colors.black,
                                      fontSize: 16

                                  ) ,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 10, 20,0),
                                  child: Text('A55 ID: ${snapshot.data[count].a55Id}',style:TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                  ) ,softWrap: true,maxLines: 1),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(20, 10, 20,10),
                                  child: Text('Planned Date: ${reformatDate(snapshot.data[count].dateLocated)}',style:TextStyle(
                                      color: Colors.black,
                                      fontSize: 16

                                  ) ,),
                                ),
                              ],
                            ),

                          ],
                        ),
                      ),

                    ));

              },itemCount: snapshot.data.length,),
            );
          }else{
            return Center(child:Theme(
              data: Theme.of(context).copyWith(accentColor: appSecondaryColor),
              child: new CircularProgressIndicator(),
            ));
          }
        },future: Api().getBlockageClearanceJobs(),),

      ),
    );
  }
}