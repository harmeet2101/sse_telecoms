import 'package:RouteProve/Common/app_color.dart';
import 'package:flutter/material.dart';

enum AlertType { Success, Failure, Info }

showAlertMessage({String message, AlertType type, BuildContext context}) {
  String title = "";
  Color defaultColor = Colors.green;

  switch (type) {
    case AlertType.Success:
      title = "Success";
      defaultColor = Colors.green;
      break;
    case AlertType.Failure:
      title = "Error";
      defaultColor = Colors.red;
      break;
    case AlertType.Info:
      title = "Info";
      defaultColor = appBackgroundColor;
      break;
  }

  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        titlePadding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        contentPadding: EdgeInsets.fromLTRB(15, 15, 15, 10),
        actionsPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: appTextColor, fontSize: 18, fontWeight: FontWeight.w600),
        ),
        content: Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: appSecondaryTextColor,
              fontSize: 14,
              fontWeight: FontWeight.w400),
        ),
        actions: <Widget>[
          Container(
            child: RaisedButton(
              color: defaultColor,
              textColor: appBackgroundColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              child: RichText(
                  text: TextSpan(
                      text: "Ok",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: appBackgroundColor))),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          )
        ],
      );
    },
  );
}

showActionDialog(
    {@required String message,
    @required AlertType type,
    @required BuildContext context,
    @required void Function() okAction,
    @required void Function() cancelAction}) {
  String title = "";
  Color defaultColor = Colors.green;

  switch (type) {
    case AlertType.Success:
      title = "Success";
      defaultColor = Colors.green;
      break;
    case AlertType.Failure:
      title = "Error";
      defaultColor = Colors.red;
      break;
    case AlertType.Info:
      title = "Info";
      defaultColor = appThemeColor;
      break;
  }

  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        titlePadding: EdgeInsets.fromLTRB(15, 15, 15, 0),
        contentPadding: EdgeInsets.fromLTRB(15, 15, 15, 10),
        actionsPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        title: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: appTextColor, fontSize: 18, fontWeight: FontWeight.w600),
        ),
        content: Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: appSecondaryTextColor,
              fontSize: 14,
              fontWeight: FontWeight.w400),
        ),
        actions: <Widget>[
          Container(
            child: RaisedButton(
              color: appBackgroundColor,
              textColor: Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              child: RichText(
                  text: TextSpan(
                      text: "Cancel",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.red))),
              onPressed: () {
                Navigator.of(context).pop();
                if (cancelAction != null){
                  cancelAction();
                }
              },
            ),
          ),
          Container(
            child: RaisedButton(
              color: defaultColor,
              textColor: appBackgroundColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              child: RichText(
                  text: TextSpan(
                      text: "Ok",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: appBackgroundColor))),
              onPressed: () {
                Navigator.of(context).pop();
                if (okAction != null){
                  okAction();
                }
              },
            ),
          )
        ],
      );
    },
  );
}
