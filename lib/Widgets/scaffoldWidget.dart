import 'package:RouteProve/Common/app_color.dart';
import 'package:flutter/material.dart';

getScaffold(BuildContext context, Widget child) {
  return GestureDetector(
    onTap: () {
      FocusScopeNode currentFocus = FocusScope.of(context);
      if (!currentFocus.hasPrimaryFocus) {
        currentFocus.unfocus();
      }
    },
    child: Container(
        decoration: BoxDecoration(
            color: appBackgroundColor,
            image: DecorationImage(
              image: AssetImage("assets/app_background.png"),
              fit: BoxFit.cover,
            )),
        child: child),
  );
}
