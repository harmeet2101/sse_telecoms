import 'package:RouteProve/Common/app_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

getActionbar(BuildContext context, Row row,
    {String image = "assets/app_logo.png"}) {
  double screenWidth = MediaQuery.of(context).size.width;
  return PreferredSize(
    preferredSize: Size(screenWidth, 140),
    child: Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).padding.top,
          width: screenWidth,
          color: appThemeColor,
        ),
        Container(
          height: 135,
          child: Stack(
            children: <Widget>[
              Container(
                height: 100,
                width: screenWidth,
                color: appThemeColor,
                padding: EdgeInsets.fromLTRB(10, 20, 10, 35),
                alignment: Alignment.center,
                child: Container(
                  child: Navigator.of(context).canPop()
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                                icon: Icon(Icons.arrow_back_ios),
                                color: appBackgroundColor,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                }),
                            SizedBox(width: 10),
                            Expanded(child: row)
                          ],
                        )
                      : row,
                ),
              ),
              Positioned(
                top: 65,
                left: screenWidth / 2 - 40,
                child: Container(
                  height: 60,
                  width: 60,
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: appSecondaryColor,
                      boxShadow: [
                        BoxShadow(
                          color: appSecondaryTextColor,
                          blurRadius: 2.0,
                          offset: Offset(0.0, 1.0),
                        ),
                      ]),
                  clipBehavior: Clip.hardEdge,
                  child: Image.asset(
                    image,
                    height: 50,
                    width: 50,
                    fit: BoxFit.contain,
                    color: appBackgroundColor,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );
}
