import 'package:RouteProve/Common/app_color.dart';
import 'package:flutter/material.dart';

class ProgressHUD extends StatelessWidget {
  final bool inAsyncCall;
  final double opacity;
  final Color color;
  final Offset offset;
  final bool dismissible;
  final Widget child;

  ProgressHUD({
    Key key,
    @required this.inAsyncCall,
    this.opacity = 0.3,
    this.color = appThemeColor,
    this.offset,
    this.dismissible = false,
    @required this.child,
  })  : assert(child != null),
        assert(inAsyncCall != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetList = [];
    widgetList.add(child);
    if (inAsyncCall) {
      Widget layOutProgressIndicator;
      if (offset == null)
        layOutProgressIndicator = Center(
          child: getLoadingWidget(context),
        );
      else {
        layOutProgressIndicator = Positioned(
          child: getLoadingWidget(context),
          left: offset.dx,
          top: offset.dy,
        );
      }
      final modal = [
        new Opacity(
          child: new ModalBarrier(dismissible: dismissible, color: color),
          opacity: opacity,
        ),
        layOutProgressIndicator
      ];
      widgetList += modal;
    }
    return new Stack(
      children: widgetList,
    );
  }

  getLoadingWidget(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Theme(
      data: ThemeData(accentColor: appThemeColor),
      child: Container(
        width: screenWidth * 0.35,
        height: screenWidth * 0.30,
        padding: EdgeInsets.all(screenWidth * 0.02),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: appBackgroundColor,
            borderRadius: BorderRadius.circular(screenWidth * 0.02)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CircularProgressIndicator(),
            FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  "Loading...",
                  style: TextStyle(
                      inherit: false,
                      color: appThemeColor,
                      fontWeight: FontWeight.w600),
                ))
          ],
        ),
      ),
    );
  }
}
